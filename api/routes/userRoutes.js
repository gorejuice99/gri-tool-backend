(function() {
'use strict';

    module.exports = function(app) {
        
        var userCtrl = require('../controllers/userCtrl'),
            config = require('../../config'),
            jwt = require('jsonwebtoken');
        
        app.post('/api/validate_user', function(req, res, next) {
            
            userCtrl.verify_user(req, res);
        });

        app.get('/api/getUser', function(req, res, next) {
            
            jwt.verify(req.headers.authorization, config.secret, function(err, decoded) {
                if(err) res.status(401).send("invalid token");
                
                userCtrl.getUsers(req, res);
            });
        });

        app.get('/api/getUserPerId', function(req, res, next) {
            
            jwt.verify(req.headers.authorization, config.secret, function(err, decoded) {
                if(err) res.status(401).send("invalid token");
                
                userCtrl.getUserPerId(req, res);
            });
        });

        app.post('/api/addUser', function(req, res, next) {
            jwt.verify(req.headers.authorization, config.secret, function(err, decoded) {
                if(err) res.status(401).send("invalid token");

                userCtrl.addUser(req, res);
            });
        });
    };

})();