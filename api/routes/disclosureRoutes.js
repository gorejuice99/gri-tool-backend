(function() {
   'use strict';

	module.exports = function(app) {
		var disclosureCtrl = require('../controllers/disclosureCtrl'),
				config = require('../../config'),
				jwt = require('jsonwebtoken');

		app.post('/api/getDisclosureBySeriesNo', function(req, res, next) {
			jwt.verify(req.headers.authorization, config.secret, function(err, decoded) {
				if(err) res.status(401).send('Invalid token');

				disclosureCtrl.getDisclosure(req, res);
			});
		});	
		
		app.get('/api/getUserDisclosure', function(req, res, next) {
			jwt.verify(req.headers.authorization, config.secret, function(err, decode) {
				if(err) res.status(401).send('Invalid token');

				disclosureCtrl.getUserDisclosure(req, res);
			});
		});

		app.post('/api/saveDisclosureByUser', function(req, res, next) {
			jwt.verify(req.headers.authorization, config.secret, function(err, decoded) {
				if(err) res.status(401).send('Invalid Token');

				disclosureCtrl.saveDisclosureByUser(req, res);
			});
		});
	};
})();