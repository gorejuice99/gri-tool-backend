(function() {
    'use strict';

    module.exports = function(app) {

        var organizationCtrl = require('../controllers/organizationCtrl'),
            jwt = require('jsonwebtoken'),
            config = require('../../config');

        app.get('/api/getOrganization', function(req, res, next) {
            jwt.verify(req.headers.authorization, config.secret, function(err, decoded) {
                if(err) res.status(401).send('Invalid token');

                organizationCtrl.getAllOrganization(req, res);
            });
        });

        app.post('/api/addOrganization', function(req, res, next) {
            jwt.verify(req.headers.authorization, config.secret, function(err, decoded) {
                if(err) res.status(401).send('Invalid token');
            
                organizationCtrl.addOrganization(req, res);
            });
        });
    };
})();