'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var OrganizationSchema = new Schema({
    organization: {
        type: String
    }
});

mongoose.model('organizations', OrganizationSchema);

