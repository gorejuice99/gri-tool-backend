'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema


var disclosureSchema = new Schema({
    
    disclosureNumber: {
        type: String
    },

    guidance: {
        type: String
    },

    number: {
        type: Number
    },

    recommendations: {
        type: Array
    },

    additionalRequirements: {
        type: Array
    },

    reportingRequirements: {
        type: Array
    },

    series: {
        type: Number
    },

    subNumber: {
        type: Number
    },

    title: {
        type: String
    }
});

mongoose.model('disclosures', disclosureSchema);

