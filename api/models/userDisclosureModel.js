'use strict';

/**
 * module dependencies
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var userDisclosureSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
    },

    genDisclosure : [{
        title : {
            type: String,
        },

        disclosureNumber: {
            type: String
        },

        status: {
            type: Number
        }, 

        series: {
            type: Number
        }
    }],

    ecoDisclosure : [{
        title : {
            type: String,
        },

        disclosureNumber: {
            type: String
        },

        status: {
            type: Number
        }, 

        series: {
            type: Number
        }
    }],

    envDisclosure : [{
        title : {
            type: String,
        },

        disclosureNumber: {
            type: String
        },

        status: {
            type: Number
        }, 

        series: {
            type: Number
        }
    }],    

    socDisclosure : [{
        title : {
            type: String,
        },

        disclosureNumber: {
            type: String
        },

        status: {
            type: Number
        }, 

        series: {
            type: Number
        }
    }],    
});


mongoose.model('userDisclosures', userDisclosureSchema);