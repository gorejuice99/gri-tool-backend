'use strict';

/**
 * module dependencies
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * user schema
 */

var userSchema = new Schema({
   
    firstName: {
        type: String
    },

    lastName: {
        type: String
    },

    username: {
        type: String,
        required: true
    },

    password: {
        type: String,
        required: true
    },

    status: {
        type: Number,
    },

    userType: {
        type: Number
    },

    organization: {
        type: Schema.Types.ObjectId,
    },
});

mongoose.model('users', userSchema);
