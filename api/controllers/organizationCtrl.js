'use strict';

var mongoose = require('mongoose'),
    model = mongoose.model('organizations');

module.exports = {

    getAllOrganization: function(req, res) {
        model.find({}, function(err, result) {
            if(err) res.status(500).send(err);
            
            res.status(200).send({
                error: 0,
                data: result
            });
        });
    },   

    addOrganization: function(req, res) {
        var instance_model = new model();
        instance_model.organization = req.body.orgName;

        instance_model.save(function(err, result) {
            if(err) res.status(500).send(err);

            res.status(200).send({
                error: 0,
                msg: "Successfully saved!"
            });
        });

    },

    
}