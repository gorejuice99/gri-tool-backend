'use strict';

var mongoose = require('mongoose'),
    model = mongoose.model('disclosures'),
    userModel = mongoose.model('users'),
    userDisclosureModel = mongoose.model('userDisclosures');

module.exports = {

  getDisclosure: function(req, res) {
    model.find({
      series: req.body.seriesNo
    }, function(err, result) {
      if(err) res.status(500).send(err);

      res.status(200).send(result);
    });
  },

  getUserDisclosure: function(req, res) {
    userDisclosureModel.find({
      userId: mongoose.Types.ObjectId(req.query.userId)
    }, function(err, result) {
      if(err) res.status(500).send(err);

      res.status(200).send(result);
    });
  },

  saveDisclosureByUser: function(req, res) {
    userModel.find({
      _id: req.body.userId
    }, function(err, result) {
      if(result.length != 0) {
        
        // req.body.formData.forEach((key, element) => {
        
        let formData = req.body.formData;
        let series200 = [], series300 = [], series400 = [];

        for(var x = 0; x < formData.length; x++) {
          if(formData[x].series == 200) {
            series200.push(module.exports.pushUserDisclosure(formData[x]));
          } else if(formData[x].series == 300) {
            series300.push(module.exports.pushUserDisclosure(formData[x]));
          } else if(formData[x].series == 400) {
            series400.push(module.exports.pushUserDisclosure(formData[x]));
          }
        } 

        var instance_model = new userDisclosureModel();
        instance_model.userId = result[0]._id;
        instance_model.ecoDisclosure = series200;
        instance_model.envDisclosure = series300;
        instance_model.socDisclosure = series400;

        instance_model.save(function(err, result){
          if(err) res.status(500).send(err);

          res.status(200).send(result);
        });
      }
    });
  
  },

  pushUserDisclosure: function(formData) {
    let series = {};
    
    return series = {
      title: formData.title,
      disclosureNumber: formData.disclosureNumber,
      status: 0,
      series: formData.series 
    };
  }
}