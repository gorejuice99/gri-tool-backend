'use strict';

var mongoose = require('mongoose'),
    model = mongoose.model('users'),
    bcrypt = require('bcrypt'),
    jwt = require('jsonwebtoken'),
    config = require('../../config'),
    async = require('async');

module.exports = {
    
    verify_user: function(req, res) {

        model.find({'username': req.body.username}, function(err, result) {
            if(err) res.status(500).send(err);

            if(result.length != 0) {
                console.log(result[0]);
                bcrypt.compare(req.body.password, result[0].password, function(err, passTrue) {
                    if(err) res.status(500).send(err);

                    if(passTrue) {
                        console.log('test');
                        var sign_data = {
                            _id: result[0]._id,
                            username: result[0].username,
                            password: result[0].password
                        }
                        
                        var token = jwt.sign(sign_data, config.secret, {
                            expiresIn: 86400 // expires in 24 hours
                        });

                        res.status(200).send({
                            token: token,
                            data: result[0], 
                            error: 0
                        });

                    } else {
                        res.status(200).send({
                            error: 1,
                            msdg: "your password is incorrect"
                        })
                    }
                });
            } else {
                res.status(200).send({
                    error: 1,
                    msg: "Username doesn't exist"
                })
            }
        });
    },

   

    getUsers: function(req, res) {
        let userType = Number(req.query.usertype);
        model.aggregate([
          {
            $lookup:
            {
                from: "organizations",
                localField: "organization",
                foreignField: "_id",
                as: "organization_docs"
            }
          },
          {
            $match: { "userType": userType }
          }
        ]).exec(function(err, result) {
            if(err) res.status(500).send(err);

            res.status(200).send({
                data: result,
                error: 0
            });
        });
    },

    addUser: function(req, res) {

        model.find({
            'username': req.body.username, 
            'organization': mongoose.Types.ObjectId(req.body.organization)}, 
            function(err, result) {
                if(err) res.status(500).send(err);
                
                if(result.length == 0) {
                    module.exports.hashPassword(req.body.password).then(function(result) {
                        let instance_model = new model();
                        instance_model.username = req.body.username;
                        instance_model.password = result.hash;
                        instance_model.status = 0;
                        instance_model.firstName = req.body.firstName;
                        instance_model.lastName = req.body.lastName;
                        instance_model.userType = req.body.userType;
                        instance_model.organization = mongoose.Types.ObjectId(req.body.organization);

                        instance_model.save(function(err, record) {
                            if(err) res.status(500).send(err);
                            console.log(record);
                            res.status(200).send({
                                error: 0
                            });
                        });

                    }).catch(function(err) {
                        res.status(500).send(err);
                    });
                } else {
                    res.status(200).send({
                        error: 1,
                        msg: "Username already been taken"
                    })
                }
        });
    },

    hashPassword(plainPwd) {
        return new Promise((resolve, reject) => {
            bcrypt.hash(plainPwd, 10, function(err, hash) {
                if(err) reject(err);

                resolve({
                    hash: hash
                });
            });
        });
    },

    getUserPerId(req, res) {
      
      model.aggregate([
        {
          $lookup:
          {
              from: "organizations",
              localField: "organization",
              foreignField: "_id",
              as: "organization_docs"
          }
        },
        {
          $match: { _id: mongoose.Types.ObjectId(req.query.id) }
        }
      ]).exec(function(err, result) {
          if(err) res.status(500).send(err);

          res.status(200).send({
            data: result,
          });
      });

    },

    createSocialDisclosure() {
        return [
            {
              "additionalRequirements" : [ {
                "description" : "When compiling the information specified in Disclosure 401-1, the reporting organization shall use the total employee numbers at the end of the reporting period to calculate the rates of new employee hires and employee turnover."
              } ],
              "disclosureNumber" : "401-1",
              "guidance" : "<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 401-1</strong></p>\n<p>An organization can use the following age groups:</p>\n<ul>\n<li>\n<p>Under 30 years old;</p>\n</li>\n<li>\n<p>30-50 years old;</p>\n</li>\n<li>\n<p>Over 50 years old.</p>\n</li>\n</ul>\n<p><strong>Background</strong></p>\n<p>The number, age, gender, and region of an organization&rsquo;s new employee hires can indicate its strategy and ability to attract diverse, qualified employees. This information can signify the organization&rsquo;s efforts to implement inclusive recruitment practices based on age and gender. It can also signify the optimal use of available labor and talent in different regions.</p>\n<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>A high rate of employee turnover can indicate levels of uncertainty and dissatisfaction among employees. It can also signal a fundamental change in the structure of an organization&rsquo;s core operations. An uneven pattern of turnover by age or gender can indicate incompatibility or potential inequity in the workplace. Turnover results in changes to the human and intellectual capital of the organization and can impact productivity. Turnover has direct cost implications either in terms of reduced payroll or greater expenses for the recruitment of employees.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 1,
              "recommendations" : [ {
                "description" : "When compiling the information specified in Disclosure 401-1, the reporting organization should use data from Disclosure 102-7 in GRI 102: General Disclosures to identify the total number of employees."
              } ],
              "reportingRequirements" : [ {
                "description" : "Total number and rate of new employee hires during the reporting period, by age group, gender and region."
              }, {
                "description" : "Total number and rate of employee turnover during the reporting period, by age group, gender and region."
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "New employee hires and employee turnover"
            },
            {
              "additionalRequirements" : [ {
                "description" : "When compiling the information specified in Disclosure 401-2, the reporting organization shall exclude in-kind benefits such as provision of sports or child day care facilities, free meals during working time, and similar general employee welfare programs."
              } ],
              "disclosureNumber" : "401-2",
              "guidance" : "<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Data reported under this disclosure provide a measure of an organization&rsquo;s investment in human resources and the minimum <span style=\"text-decoration: underline;\">benefits</span> it offers its full- time employees. The quality of benefits for full-time employees is a key factor in retaining employees.</p>\n</div>\n</div>\n</div>",
              "number" : 1,
              "reportingRequirements" : [ {
                "description" : "Benefits which are standard for full-time employees of the organization but are not provided to temporary or part-time employees, by significant locations of operation. These include, as a minimum:",
                "subRequirements" : [ {
                  "description" : "life insurance;"
                }, {
                  "description" : "health care;"
                }, {
                  "description" : "disability and invalidity coverage;"
                }, {
                  "description" : "parental leave;"
                }, {
                  "description" : "retirement provision;"
                }, {
                  "description" : "stock ownership;"
                }, {
                  "description" : "others."
                } ]
              }, {
                "description" : "The definition used for ‘significant locations of operation’."
              } ],
              "series" : "400",
              "subNumber" : 2,
              "title" : "Benefits provided to full-time employees that are not provided to temporary or part-time employees"
            },
            {
              "disclosureNumber" : "401-3",
              "guidance" : "<div class=\"page\" title=\"Page 9\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 401-3</strong></p>\n<p>Employees entitled to parental leave means those employees that are covered by organizational policies, agreements or contracts that contain parental leave entitlements.</p>\n<p>To determine who returned to work after parental leave ended and were still employed 12 months later, an organization can consult records from the prior reporting periods.</p>\n<div class=\"page\" title=\"Page 9\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Many countries have introduced legislation to provide parental leave. The aim of the legislation is to allow employees to take leave and return to work in the same or a comparable position.</p>\n</div>\n<div class=\"column\">\n<p>The application of legislation varies according to interpretation by government, employers and employees. Many women are discouraged from taking leave and returning to work by employer practices that affect their employment security, remuneration and career path. Many men are not encouraged to take the leave to which they are entitled.</p>\n<p>Equitable gender choice for maternity and paternity leave, and other leave entitlements, can lead to the greater recruitment and retention of qualified employees. It can also boost employee morale and productivity. Men&rsquo;s uptake of paternity leave options can indicate the degree to which an organization encourages fathers to take such leave. Men taking advantage of leave entitlements positively impacts women to take such leave without prejudicing their career path.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 1,
              "recommendations" : [ {
                "description" : "When compiling the information specified in Disclosure 401-3, the reporting organization should use the following formulas to calculate the return to work and retention rates: Return to work rate = (Total number of employees that did return to work after parental leave / Total number of employees due to return to work after taking parental leave) x 100; Retention rate = (Total number of employees retained 12 months after returning to work following a period of parental leave / Total number of employees returning from parental leave in the prior reporting period(s)) x 100"
              } ],
              "reportingRequirements" : [ {
                "description" : "Total number of employees that were entitled to parental leave, by gender."
              }, {
                "description" : "Total number of employees that took parental leave, by gender."
              }, {
                "description" : "Total number of employees that returned to work in the reporting period after parental leave ended, by gender."
              }, {
                "description" : "Total number of employees that returned to work after parental leave ended that were still employed 12 months after their return to work, by gender."
              }, {
                "description" : "Return to work and retention rates of employees that took parental leave, by gender."
              } ],
              "series" : "400",
              "subNumber" : 3,
              "title" : "Parental leave"
            },
            {
              "disclosureNumber" : "402-1",
              "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 402-1</strong></p>\n<p>Minimum notice periods can be found in corporate policies and standard employment contracts. Different policy statements can exist at a regional level.</p>\n<p>An organization can identify the collective bargaining agreements referred to in Disclosure 102-41 of GRI 102: General Disclosures, and review the notice period clauses within these documents.</p>\n<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Organizations are expected to provide reasonable notice of significant operational changes to employees and their representatives, as well as to appropriate government authorities. Minimum notice periods are a measure of an organization&rsquo;s ability to maintain employee satisfaction and motivation while implementing significant changes to operations.</p>\n<p>This disclosure provides insight into an organization&rsquo;s practice of ensuring timely discussion of significant operational changes, and engaging with its employees and their representatives to negotiate and implement these changes, which can have positive or negative implications for workers.</p>\n</div>\n<div class=\"column\">\n<p>This disclosure also allows an assessment of an organization&rsquo;s consultation practices in relation to expectations expressed in relevant international norms.</p>\n<p>The essence of consultation is that management takes the views of workers into account when making specific decisions. Therefore, it is important that consultation takes place before a decision is made. Meaningful consultation includes the timely provision of all information needed to make an informed decision to workers or their representatives. Genuine consultation involves dialogue; opinion surveys and questionnaires are not considered consultation.</p>\n<p>Timely and meaningful consultation allows the affected parties to understand the impacts of the changes, such as possible loss of employment. It also gives an opportunity for them to work collectively to avoid or mitigate negative impacts as much as possible (see references 11 and 12 in the References section). Consultative practices that result in good industrial relations help to provide positive working environments, reduce turnover, and minimize operational disruptions.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 2,
              "reportingRequirements" : [ {
                "description" : "Minimum number of weeks’ notice typically provided to employees and their representatives prior to the implementation of significant operational changes that could substantially affect them."
              }, {
                "description" : "For organizations with collective bargaining agreements, report whether the notice period and provisions for consultation and negotiation are specified in collective agreements."
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Minimum notice periods regarding operational changes"
            },
            {
              "disclosureNumber" : "403-1",
              "guidance" : "<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 403-1</strong></p>\n<p>This disclosure covers formal health and safety committees that help monitor, collect feedback<br /> and advise on occupational safety programs. These committees can exist at the facility level, or at the multi-facility, regional, group or organizational levels.</p>\n<p>A formal committee is a committee whose existence and function are integrated in an organization&rsquo;s organizational and authority structure, and that operate according to certain agreed, written rules.</p>\n<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 403-1-b</strong></p>\n<p>This disclosure covers workers performing work that is under the direct control of the reporting organization, as well as workers performing work at a site controlled by the organization, even if the work itself is not controlled by the organization.</p>\n<p>This disclosure requires reporting the percentage of workers represented by formal joint management- worker health and safety committees. It does not require reporting the percentage of workers who are members of these committees.</p>\n<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Respect for the rights of workers and their participation in health and safety decisions are key. This includes the right of workers to:</p>\n<ul>\n<li>\n<p>know fully about the hazard of their work;</p>\n</li>\n<li>\n<p>receive all necessary education and training to perform work safely;</p>\n</li>\n<li>\n<p>refuse unsafe work without fear of reprisal;</p>\n</li>\n<li>\n<p>participate fully in the establishment and implementation of occupational health and safety policies, procedures, investigations and risk assessments.</p>\n</li>\n</ul>\n<p style=\"display: inline !important;\">A health and safety committee with joint representation can facilitate a positive health and safety culture. The use of committees is one way to involve workers in driving the improvement of occupational health and safety</p>\n<p>in the workplace. Participation can be by means of properly and independently elected worker health and safety representatives, and through worker members of joint management-worker health and safety committees.</p>\n<p>This disclosure provides one measure of the extent to which workers whose work, or workplace, is controlled by an organization are actively involved in health and safety.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 3,
              "recommendations" : [ {
                "description" : "When compiling the information specified in Disclosure 403-1-b, the reporting organization should explain how the percentage was calculated, including any assumptions made, such as which workers were included in the calculation."
              } ],
              "reportingRequirements" : [ {
                "description" : "The level at which each formal joint management-worker health and safety committee typically operates within the organization."
              }, {
                "description" : "Percentage of workers whose work, or workplace, is controlled by the organization, that are represented by formal joint management-worker health and safety committees."
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Workers representation in formal joint management–worker health and safety committees"
            },
            {
              "additionalRequirements" : [ {
                "2" : {
                  "subRequirements" : [ {
                    "description" : "whether ‘days’ means ‘calendar days’ or ‘scheduled work days’;"
                  } ]
                },
                "description" : "When compiling the information specified in Disclosure 403-2, the reporting organization shall:",
                "subRequirements" : [ {
                  "description" : "indicate whether minor (first-aid level) injuries are included or excluded in the injury rate (IR);"
                }, {
                  "description" : "include fatalities in the injury rate (IR);"
                }, {
                  "description" : "in calculating ‘lost days’, indicate:",
                  "subRequirements" : [ {
                    "description" : "whether ‘days’ means ‘calendar days’ or ‘scheduled work days’;"
                  }, {
                    "description" : "at what point the ‘lost days’ count begins (for example, the day after the accident or three days after the accident)."
                  } ]
                } ]
              } ],
              "disclosureNumber" : "403-2",
              "guidance" : "<div class=\"page\" title=\"Page 9\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 403-2</strong></p>\n<p>See also definitions of &lsquo;<span style=\"text-decoration: underline;\">absentee</span>&rsquo; and &lsquo;<span style=\"text-decoration: underline;\">occupational disease</span>&rsquo; in the GRI Standards Glossary.</p>\n<p><strong>Guidance for Disclosure 403-2-c</strong></p>\n<p>An organization is expected to identify the system used to track and report on health and safety incidents and performance, and to ensure that this system covers all significant operations and geographic locations. In some cases, multiple systems can be used across<br /> the organization.</p>\n<div class=\"page\" title=\"Page 9\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for clauses 2.3.3 and 2.3.4</strong></p>\n<p>The ILO Code of Practice was developed for the reporting, recording, and notification<br /> of workplace accidents.</p>\n<p><strong>Background</strong></p>\n<p>Low injury and absentee rates are generally linked to positive trends in morale and productivity. This disclosure shows whether health and safety management practices are resulting in fewer occupational health and safety incidents. Evaluating trends and patterns can also indicate potential workplace inequity.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 3,
              "recommendations" : [ {
                "description" : "When compiling the information specified in Disclosure 403-2, the reporting organization should:",
                "subRecommendations" : [ {
                  "description" : "report the occupational disease rate (ODR), lost day rate (LDR), and absentee rate (AR) for all workers (excluding employees) whose work, or workplace, is controlled by the organization, with a breakdown by:",
                  "subRecommendations" : [ {
                    "description" : "region;"
                  }, {
                    "description" : "gender;"
                  } ]
                } ]
              }, {
                "description" : "explain how the information in Disclosure 403-2-b was calculated, including any assumptions made, such as which workers were included in the calculation;"
              }, {
                "description" : "in situations where national law follows the ILO Code of Practice on Recording and Notification of Occupational Accidents and Diseases (Code of Practice), state that fact and that practice follows the law;"
              }, {
                "description" : "in situations where national law does not follow the ILO Code of Practice, indicate which system of rules it applies and their relationship to the ILO Code of Practice."
              } ],
              "reportingRequirements" : [ {
                "description" : "Types of injury, injury rate (IR), occupational disease rate (ODR), lost day rate (LDR), absentee rate (AR), and work-related fatalities, for all employees, with a breakdown by:",
                "subRequirements" : [ {
                  "description" : "region;"
                }, {
                  "description" : "gender."
                } ]
              }, {
                "description" : "Types of injury, injury rate (IR), and work-related fatalities, for all workers (excluding employees) whose work, or workplace, is controlled by the organization, with a breakdown by:",
                "subRequirements" : [ {
                  "description" : "region;"
                }, {
                  "description" : "gender."
                } ]
              }, {
                "description" : "The system of rules applied in recording and reporting accident statistics."
              } ],
              "series" : "400",
              "subNumber" : 2,
              "title" : "Types of injury and rates of injury, occupational diseases, lost days, and absenteeism, and number of work-related fatalities"
            },
            {
              "disclosureNumber" : "403-3",
              "guidance" : "<div class=\"page\" title=\"Page 10\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>This disclosure has specific relevance for organizations working in countries with a high risk or incidence of communicable diseases, and those in professions that have a high incidence of specific diseases. Preventing serious diseases contributes to health, satisfaction, and low turnover rate.</p>\n</div>\n</div>\n</div>",
              "number" : 3,
              "reportingRequirements" : [ {
                "description" : "Whether there are workers whose work, or workplace, is controlled by the organization, involved in occupational activities who have a high incidence or high risk of specific diseases."
              } ],
              "series" : "400",
              "subNumber" : 3,
              "title" : "Workers with high incidence or high risk of diseases related to their occupation"
            },
            {
              "disclosureNumber" : "403-4",
              "guidance" : "<div class=\"page\" title=\"Page 11\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 403-4</strong></p>\n<p>Agreements at the local level typically address topics that can include:</p>\n<ul>\n<li>\n<p>personal protective equipment;</p>\n</li>\n<li>\n<p>joint management-worker health and safety committees;</p>\n</li>\n<li>\n<p>participation of worker representatives in health and safety inspections, audits, and accident investigations;</p>\n</li>\n<li>\n<p>training and education;</p>\n</li>\n<li>\n<p>complaints mechanisms;</p>\n</li>\n<li>\n<p>the right to refuse unsafe work;</p>\n</li>\n<li>\n<p>periodic inspections.</p>\n</li>\n</ul>\nAgreements at the global level typically address topics that can include:<br />\n<ul>\n<li>\n<p>compliance with the ILO;</p>\n</li>\n<li>\n<p>arrangements or structures for resolving problems;</p>\n</li>\n<li>\n<p>commitments regarding target performance standards, or levels of practice to apply.</p>\n</li>\n</ul>\n<div class=\"page\" title=\"Page 11\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Formal agreements can promote the acceptance of responsibilities by both parties and the development of a positive health and safety culture. This disclosure reveals the extent to which workers are actively involved in formal, labor-management agreements that determine health and safety management arrangements.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 3,
              "reportingRequirements" : [ {
                "description" : "Whether formal agreements (either local or global) with trade unions cover health and safety."
              }, {
                "description" : "If so, the extent, as a percentage, to which various health and safety topics are covered by these agreements."
              } ],
              "series" : "400",
              "subNumber" : 4,
              "title" : "Health and safety topics covered in formal agreements with trade unions"
            },
            {
              "disclosureNumber" : "404-1",
              "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 404-1</strong></p>\n<p>This disclosure provides insight into the scale of an organization&rsquo;s investment in training, and the degree to which the investment is made across the entire employee base.</p>\n<p>In the context of this Standard, &lsquo;training&rsquo; refers to:</p>\n<ul>\n<li>\n<p>all types of vocational training and instruction;</p>\n</li>\n<li>\n<p>paid educational leave provided by an organization for its employees;</p>\n</li>\n<li>\n<p>training or education pursued externally and paid for in whole or in part by an organization;</p>\n</li>\n<li>\n<p>training on specific topics.</p>\n</li>\n</ul>\nTraining does not include on-site coaching by supervisors.</div>\n<div class=\"column\">&nbsp;</div>\n<div class=\"column\">\n<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>To calculate the information in Disclosure 404-1, the reporting organization can use the following formulas:</p>\n</div>\n</div>\n<div class=\"section\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Average training hours per employee</strong> = Total number of training hours provided to employees / Total number of employees</p>\n<p><strong>Average training hours per female</strong> = Total number of training hours provided to female employees / Total number of female employees</p>\n<div class=\"page\" title=\"Page 7\">\n<div class=\"section\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Average training hours per male</strong> = Total number of training hours provided to male employees / Total number of male employees</p>\n<p><strong>Average training hours per employee category</strong> = Total number of training hours provided to each category of employees / Total number of employees in category</p>\n<p>&nbsp;</p>\n<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>A number of calculations can be undertaken to report on employee categories. These calculations are specific to each organization.</p>\n</div>\n</div>\n</div>\n<p>&nbsp;</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 4,
              "recommendations" : [ {
                "description" : "When compiling the information specified in Disclosure 404-1, the reporting organization should:",
                "subRecommendations" : [ {
                  "description" : "express employee numbers as either head count or Full Time Equivalent (FTE), and disclose and apply the approach consistently in the period, and between periods;"
                }, {
                  "description" : "use data from Disclosure 102-7 in GRI 102: General Disclosures to identify the total number of employees;"
                }, {
                  "description" : "draw from the information used for Disclosure 405-1 in GRI 405: Diversity and Equal Opportunity to identify the total number of employees by employee category. ￼404-1 ￼￼￼￼"
                } ]
              } ],
              "reportingRequirements" : [ {
                "description" : "Average hours of training that the organization’s employees have undertaken during the reporting period, by:",
                "subRequirements" : [ {
                  "description" : "gender;"
                }, {
                  "description" : "employee category."
                } ]
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Average hours of training per year per employee"
            },
            {
              "disclosureNumber" : "404-2",
              "guidance" : "<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 404-2</strong></p>\n<p>Employee training programs that aim to upgrade skills can include:</p>\n<ul>\n<li>\n<p>internal training courses;</p>\n</li>\n<li>\n<p>funding support for external training or education;</p>\n</li>\n<li>\n<p>the provision of sabbatical periods with guaranteed return to employment.</p>\n</li>\n</ul>\n<p>Transition assistance programs provided to support employees who are retiring or who have been terminated can include:</p>\n<ul>\n<li>\n<p>pre-retirement planning for intended retirees;</p>\n</li>\n<li>\n<p>retraining for those intending to continue working;</p>\n</li>\n<li>\n<p>severance pay, which can take into account employee age and years of service;</p>\n</li>\n<li>\n<p>job placement services;</p>\n</li>\n<li>\n<p>assistance (such as training, counselling) on transitioning to a non-working life.</p>\n</li>\n</ul>\n<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Programs for upgrading employee skills allow an organization to plan skills acquisition that equips employees to meet strategic targets in a changing work environment. More skilled employees enhance the organization&rsquo;s human capital and contribute to employee satisfaction, which correlates strongly with improved performance. For those facing retirement, confidence and quality of work relations is improved by the knowledge that they are supported in their transition from work to retirement.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 4,
              "reportingRequirements" : [ {
                "description" : "Type and scope of programs implemented and assistance provided to upgrade employee skills."
              }, {
                "description" : "Transition assistance programs provided to facilitate continued employability and the management of career endings resulting from retirement or termination of employment."
              } ],
              "series" : "400",
              "subNumber" : 2,
              "title" : "Programs for upgrading employee skills and transition assistance programs"
            },
            {
              "disclosureNumber" : "404-3",
              "guidance" : "<div class=\"page\" title=\"Page 9\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>This disclosure measures the extent to which an organization regularly appraises employee performance. This aids the personal development of individual employees. It also contributes to skills management and to the development of human capital within the organization. This disclosure also demonstrates the extent to which this system is applied throughout the organization, and whether there is inequity of access to these opportunities.</p>\n<div class=\"page\" title=\"Page 9\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Regular performance and career development reviews can also enhance employee satisfaction, which correlates with improved organizational performance. This disclosure helps demonstrate how an organization works to monitor and maintain the skill sets of its employees. When reported in conjunction with Disclosure 404-2, the disclosure also helps to illustrate how the organization approaches skills enhancement.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 4,
              "recommendations" : [ {
                "description" : "When compiling the information specified in Disclosure 404-3, the reporting organization should:",
                "subRecommendations" : [ {
                  "description" : "use data from Disclosure 102-7 in GRI 102: General Disclosures to identify the total number of employees;"
                }, {
                  "description" : "draw from the information used for Disclosure 405-1 in GRI 405: Diversity and Equal Opportunity to identify the total number of employees by employee category."
                } ]
              } ],
              "reportingRequirements" : [ {
                "description" : "Percentage of total employees by gender and by employee category who received a regular performance and career development review during the reporting period."
              } ],
              "series" : "400",
              "subNumber" : 3,
              "title" : "Percentage of employees receiving regular performance and career development reviews"
            },
            {
              "disclosureNumber" : "405-1",
              "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 405-1</strong></p>\n<p>Examples of governance bodies that exist within an organization can be the board of directors, management committee, or a similar body for a non-corporate organization.</p>\n<p>An organization can identify any other indicators of diversity used in its own monitoring and recording that are relevant for reporting.</p>\n<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>This disclosure provides a quantitative measure of diversity within an organization and can be used in conjunction with sectoral or regional benchmarks. Comparisons between broad employee diversity and management team diversity offer information on equal opportunity. Information reported in this disclosure also helps in assessing which issues can be of particular relevance to certain segments of the governance bodies or employees.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 5,
              "recommendations" : [ {
                "description" : "When compiling the information specified in Disclosure 405-1, the reporting organization should use data from Disclosure 102-7 in GRI 102: General Disclosures to identify the total number of employees."
              } ],
              "reportingRequirements" : [ {
                "description" : "Percentage of individuals within the organization’s governance bodies in each of the following diversity categories:",
                "subRequirements" : [ {
                  "description" : "Gender;"
                }, {
                  "description" : "Age group: under 30 years old, 30-50 years old, over 50 years old;"
                }, {
                  "description" : "Other indicators of diversity where relevant (such as minority or vulnerable groups)."
                } ]
              }, {
                "description" : "Percentage of employees per employee category in each of the following diversity categories:",
                "subRequirements" : [ {
                  "description" : "Gender;"
                }, {
                  "description" : "Age group: under 30 years old, 30-50 years old, over 50 years old;"
                }, {
                  "description" : "Other indicators of diversity where relevant (such as minority or vulnerable groups)."
                } ]
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Diversity of governance bodies and employees"
            },
            {
              "disclosureNumber" : "405-2",
              "guidance" : "<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 405-2</strong></p>\n<p>The reporting organization can draw from the information used for Disclosure 405-1 to identify the total number of employees in each employee category by gender.</p>\n<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>An organization can take an active role in reviewing its operations and decisions, in order to promote diversity, eliminate gender bias, and support equal opportunity. These principles apply equally to recruitment, opportunities for advancement, and remuneration policies. Equality of remuneration is also an important factor in retaining qualified employees.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 5,
              "recommendations" : [ {
                "description" : "When compiling the information specified in Disclosure 405-2, the reporting organization should base remuneration on the average pay of each gender grouping within each employee category."
              } ],
              "reportingRequirements" : [ {
                "description" : "Ratio of the basic salary and remuneration of women to men for each employee category, by significant locations of operation."
              }, {
                "description" : "The definition used for ‘significant locations of operation’."
              } ],
              "series" : "400",
              "subNumber" : 2,
              "title" : "Ratio of basic salary and remuneration of women to men"
            },
            {
              "additionalRequirements" : [ {
                "description" : "When compiling the information specified in Disclosure 406-1, the reporting organization shall include incidents of discrimination on grounds of race, color, sex, religion, political opinion, national extraction, or social origin as defined by the ILO, or other relevant forms of discrimination involving internal and/or external stakeholders across operations in the reporting period."
              } ],
              "disclosureNumber" : "406-1",
              "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 406-1</strong></p>\n<p>In the context of this disclosure, an &lsquo;incident&rsquo; refers to a legal action or complaint registered with the reporting organization or competent authorities through a formal process, or an instance of non-compliance identified</p>\n<p>by the organization through established procedures. Established procedures to identify instances of non- compliance can include management system audits, formal monitoring programs, or <span style=\"text-decoration: underline;\">grievance mechanisms</span>.</p>\n<p>An incident is no longer subject to action if it is resolved, the case is completed, or no further action is required by the organization. For example, an incident for which no further action is required can include cases that were withdrawn or where the underlying circumstances that led to the incident no longer exist.</p>\n<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>According to ILO instruments, discrimination can occur on the grounds of race, color, sex, religion, political opinion, national extraction, and social origin. Discrimination can also occur based on factors such as age, disability, migrant status, HIV and AIDS, gender, sexual orientation, genetic predisposition, and lifestyles, among others.1</p>\n<p>The presence and effective implementation of policies to avoid discrimination are a basic expectation of socially responsible conduct.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 6,
              "reportingRequirements" : [ {
                "description" : "Total number of incidents of discrimination during the reporting period."
              }, {
                "description" : "Status of the incidents and actions taken with reference to the following:",
                "subRequirements" : [ {
                  "description" : "Incident reviewed by the organization;"
                }, {
                  "description" : "Remediation plans being implemented;"
                }, {
                  "description" : "Remediation plans that have been implemented, with results reviewed through routine internal management review processes;"
                }, {
                  "description" : "Incident no longer subject to action."
                } ]
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Incidents of discrimination and corrective actions taken"
            },
            {
              "disclosureNumber" : "407-1",
              "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 407-1</strong></p>\n<p>The process for identifying operations and suppliers,<br /> as specified in Disclosure 407-1, can reflect the reporting organization&rsquo;s approach to risk assessment on this issue. It can also draw from recognized international data sources, such as the various outcomes of the ILO Supervisory bodies and the recommendations of the ILO Committee of Freedom of Association (see reference 4 in the References section).</p>\n<p>When reporting the measures taken, the organization can refer to the ILO &lsquo;Tripartite Declaration of Principles Concerning Multinational Enterprises and Social Policy&rsquo; and Organisation for Economic Co-operation and Development (OECD) OECD Guidelines for Multinational Enterprises for further guidance.</p>\n<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>This disclosure concerns an organization&rsquo;s due diligence with respect to any adverse impacts its activities have had on the human rights of workers to form or join trade unions and to bargain collectively. This can include policies and processes with respect to the organization's business relationships, including its suppliers. It can also include the due diligence process to identify operations and suppliers where these rights are at risk.</p>\n</div>\n<div class=\"column\">\n<p>It also aims to reveal actions that have been taken to support these rights across an organization&rsquo;s range of operations. This disclosure does not require the organization to express a specific opinion on the quality of national legal systems.</p>\n<p>Collective agreements can be at the level of the organization; at the industry level, in countries where that is the practice; or at both. Collective agreements can cover specific groups of workers; for example, those performing a specific activity or working at<br /> a specific location.</p>\n<p>An organization is expected to respect the rights of workers to exercise freedom of association and collective bargaining. It is also expected to not benefit from or contribute to such violations through its business relationships (e.g., suppliers).</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 7,
              "reportingRequirements" : [ {
                "description" : "Operations and suppliers in which workers’ rights to exercise freedom of association or collective bargaining may be violated or at significant risk either in terms of:",
                "subRequirements" : [ {
                  "description" : "type of operation (such as manufacturing plant) and supplier;"
                }, {
                  "description" : "countries or geographic areas with operations and suppliers considered at risk."
                } ]
              }, {
                "description" : "Measures taken by the organization in the reporting period intended to support rights to exercise freedom of association and collective bargaining."
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Operations and suppliers in which the right to freedom of association and collective bargaining may be at risk"
            },
            {
              "disclosureNumber" : "408-1",
              "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 408-1</strong></p>\n<p>The process for identifying operations and suppliers, as specified in Disclosure 408-1, can reflect the reporting organization&rsquo;s approach to risk assessment on this issue. It can also draw from recognized international data sources, such as the ILO Information and reports on the application of Conventions and Recommendations (see reference 1 in the References section).</p>\n<p>When reporting the measures taken, the organization can refer to the ILO &lsquo;Tripartite Declaration of Principles Concerning Multinational Enterprises and Social Policy&rsquo; and Organisation for Economic Co-operation and Development (OECD) OECD Guidelines for Multinational Enterprises for further guidance.</p>\n<p>In the context of the GRI Standards, a &lsquo;young worker&rsquo; is defined as a person above the applicable minimum working age and younger than 18 years of age. Note that Disclosure 408-1 does not require quantitative reporting on child labor or the number of young workers. Rather, it asks for reporting on the operations and suppliers considered to have significant risk for incidents of child labor or young workers exposed to hazardous work.</p>\n<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Child labor is subject to ILO Conventions 138 &lsquo;Minimum Age Convention&rsquo; (ILO Convention 138) and 182 &lsquo;Worst Forms of Child Labour Convention&rsquo; (ILO Convention 182).</p>\n<p>&lsquo;Child labor&rsquo; refers to an abuse, which is not to be confused with &lsquo;children working&rsquo; or with &lsquo;young persons working&rsquo;, which may not be abuses as stipulated in ILO Convention 138.</p>\n<p>The minimum age for working differs by country. ILO Convention 138 specifies a minimum age of 15 years or the age of completion of compulsory schooling (whichever is higher). However, there is an exception for certain countries where economies and educational facilities are insufficiently developed and a minimum age of 14 years might apply. These countries of exception are specified by the ILO in response to special application by the country concerned, and in consultation with representative organizations of employers and workers.</p>\n<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>ILO Convention 138 stipulates that &lsquo;national laws or regulations may permit the employment or work of persons 13 to 15 years of age on light work which is (a) not likely to be harmful to their health or development; and (b) not such as to prejudice their attendance at school, their participation in vocational orientation or training programmes approved by the competent authority or their capacity to benefit from the instruction received&rsquo;.</p>\n<p>While child labor takes many different forms, a priority is to eliminate without delay the worst forms of child labor as defined by Article 3 of ILO Convention 182. This includes all forms of slavery or practices similar to slavery (such as sale, trafficking, forced or compulsory labor, serfdom, recruitment for armed conflict); the use, procuring or offering of a child for prostitution or illicit activities and any work that is likely to harm the health, safety or morals of children. ILO Convention 182 is intended to set priorities for states; however, organizations are expected not to use this convention to justify forms of child labor.</p>\n</div>\n<div class=\"column\">\n<p>Child labor results in under-skilled and unhealthy workers for tomorrow and perpetuates poverty across generations, thus impeding sustainable development. The abolition of child labor is therefore necessary for both economic and human development.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 8,
              "reportingRequirements" : [ {
                "description" : "Operations and suppliers considered to have significant risk for incidents of:",
                "subRequirements" : [ {
                  "description" : "child labor;"
                }, {
                  "description" : "young workers exposed to hazardous work."
                } ]
              }, {
                "description" : "Operations and suppliers considered to have significant risk for incidents of child labor either in terms of:",
                "subRequirements" : [ {
                  "description" : "type of operation (such as manufacturing plant) and supplier;"
                }, {
                  "description" : "countries or geographic areas with operations and suppliers considered at risk."
                } ]
              }, {
                "description" : "Measures taken by the organization in the reporting period intended to contribute to the effective abolition of child labor."
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Operations and suppliers at significant risk for incidents of child labor"
            },
            {
              "disclosureNumber" : "409-1",
              "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 409-1</strong></p>\n<p>The process for identifying operations and suppliers, as specified in Disclosure 409-1, can reflect the reporting organization&rsquo;s approach to risk assessment on this issue. It can also draw from recognized international data sources, such as the ILO Information and reports on the application of Conventions and Recommendations (see reference 1 in the References section).</p>\n<p>When reporting the measures taken, the organization can refer to the ILO &lsquo;Tripartite Declaration of Principles Concerning Multinational Enterprises and Social Policy&rsquo; and Organisation for Economic Co-operation and Development (OECD) OECD Guidelines for Multinational Enterprises for further guidance.</p>\n<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Forced or compulsory labor exists globally in a variety of forms. The most extreme examples are slave labor and bonded labor, but debts can also be used as a means of maintaining workers in a state of forced labor. Indicators of forced labor can also include withholding identity papers, requiring compulsory deposits, and compelling workers, under threat of firing, to work extra hours to which they have not previously agreed.</p>\n<p>Eliminating forced labor remains an important challenge. Forced labor is not only a serious violation of a fundamental human right, it also perpetuates poverty and is a hindrance to economic and human development.5</p>\n<p>The presence and effective implementation of policies for eliminating all forms of forced or compulsory labor are a basic expectation of socially responsible conduct. Organizations with multinational operations are required by law in some countries to provide information on their efforts to eradicate forced labor in their supply chains.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 9,
              "reportingRequirements" : [ {
                "description" : "Operations and suppliers considered to have significant risk for incidents of forced or compulsory labor either in terms of:",
                "subRequirements" : [ {
                  "description" : "type of operation (such as manufacturing plant) and supplier;"
                }, {
                  "description" : "countries or geographic areas with operations and suppliers considered at risk."
                } ]
              }, {
                "description" : "Measures taken by the organization in the reporting period intended to contribute to the elimination of all forms of forced or compulsory labor."
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Operations and suppliers at significant risk for incidents of forced or compulsory labor"
            },
            {
              "disclosureNumber" : "410-1",
              "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 410-1</strong></p>\n<p>The training can refer either to training dedicated to the subject of human rights or to a human rights module within a general training program. Training can cover issues such as the use of force, inhuman or degrading treatment or discrimination, or identification and registering.</p>\n<p><strong>Background</strong></p>\n<p>The use of security personnel can play an essential role in allowing an organization to operate in a safe and productive manner, and can contribute to the security of local communities and populations.</p>\n<p>However, as set out in the International Code of Conduct for Private Security Service Providers, the use of security personnel can also have potentially negative impacts on local populations and on the upholding of human rights and the rule of law.</p>\n</div>\n<div class=\"column\">\n<p>According to the UN Human Rights Office of the High Commissioner, &lsquo;human rights education constitutes an essential contribution to the long-term prevention of human rights abuses and represents an important investment in the endeavor to achieve a just society in which all human rights of all persons are valued and respected.&rsquo;1</p>\n<p>Training security personnel in human rights can therefore help to ensure their appropriate conduct towards third parties, particularly regarding the use of force. This disclosure indicates the proportion of the security force that can reasonably be assumed to be aware of an organization&rsquo;s expectations of human rights performance. Information provided under this disclosure can demonstrate the extent to which management systems pertaining to human rights are implemented.</p>\n</div>\n</div>\n</div>",
              "number" : 10,
              "recommendations" : [ {
                "description" : "When compiling the information specified in Disclosure 410-1-a, the reporting organization should:",
                "subRecommendations" : [ {
                  "description" : "calculate the percentage using the total number of security personnel, whether they are employees of the organization or employees of third-party organizations;"
                }, {
                  "description" : "state whether employees of third-party organizations are also included in the calculation. ￼￼￼￼"
                } ]
              } ],
              "reportingRequirements" : [ {
                "description" : "Percentage of security personnel who have received formal training in the organization’s human rights policies or specific procedures and their application to security."
              }, {
                "description" : "Whether training requirements also apply to third-party organizations providing security personnel."
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Security personnel trained in human rights policies or procedures"
            },
            {
              "disclosureNumber" : "411-1",
              "guidance" : "<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 411-1</strong></p>\n<p>In the context of this disclosure, an &lsquo;incident&rsquo; refers to a legal action or complaint registered with the reporting organization or competent authorities through a formal process, or an instance of non-compliance identified by the organization through established procedures. Established procedures to identify instances of non- compliance can include management system audits, formal monitoring programs, or grievance mechanisms.</p>\n</div>\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>The number of recorded incidents involving the rights of indigenous peoples provides information about the implementation of an organization&rsquo;s policies relating to indigenous peoples. This information helps to indicate the state of relations with stakeholder communities. This is particularly important in regions where indigenous peoples reside, or have interests near operations of the organization.</p>\n</div>\n</div>\n</div>",
              "number" : 11,
              "recommendations" : [ {
                "description" : "When compiling the information specified in Disclosure 411-1, the reporting organization should include incidents involving the rights of indigenous peoples among:",
                "subRecommendations" : [ {
                  "description" : "workers performing the organization's activities;"
                }, {
                  "description" : "communities likely to be impacted by existing or planned activities of the organization."
                } ]
              } ],
              "reportingRequirements" : [ {
                "description" : "Total number of identified incidents of violations involving the rights of indigenous peoples during the reporting period."
              }, {
                "description" : "Status of the incidents and actions taken with reference to the following:",
                "subRequirements" : [ {
                  "description" : "Incident reviewed by the organization;"
                }, {
                  "description" : "Remediation plans being implemented;"
                }, {
                  "description" : "Remediation plans that have been implemented, with results reviewed through routine internal management review processes;"
                }, {
                  "description" : "Incident no longer subject to action."
                } ]
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Incidents of violations involving rights of indigenous peoples"
            },
            {
              "disclosureNumber" : "412-1",
              "guidance" : "<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Information reported for this disclosure can show the extent to which an organization considers human rights when making decisions on its locations of operations. It can also provide information to assess the organization&rsquo;s potential to be associated with,<br /> or to be considered complicit in, human rights abuse.</p>\n</div>\n</div>\n</div>",
              "number" : 12,
              "reportingRequirements" : [ {
                "description" : "Total number and percentage of operations that have been subject to human rights reviews or human rights impact assessments, by country."
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Operations that have been subject to human rights reviews or impact assessments"
            },
            {
              "disclosureNumber" : "412-1",
              "guidance" : "<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Information reported for this disclosure can show the extent to which an organization considers human rights when making decisions on its locations of operations. It can also provide information to assess the organization&rsquo;s potential to be associated with,<br /> or to be considered complicit in, human rights abuse.</p>\n</div>\n</div>\n</div>",
              "number" : 12,
              "reportingRequirements" : [ {
                "description" : "Total number and percentage of operations that have been subject to human rights reviews or human rights impact assessments, by country."
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Operations that have been subject to human rights reviews or impact assessments"
            },
            {
              "disclosureNumber" : "412-2",
              "guidance" : "<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 412-2</strong></p>\n<p>The disclosure covers employee training on human rights policies or procedures concerning aspects of human rights that are relevant to operations, including the applicability of the human rights policies or procedures to the employees&rsquo; work.</p>\n<p>The training can refer either to training dedicated to the subject of human rights or to a human rights module within a general training program.</p>\n<p>Reporting the total number of hours of employee training is covered by GRI 404: Training and Education.</p>\n</div>\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Information generated from this disclosure offers insight into an organization&rsquo;s capacity to implement its human rights policies and procedures.</p>\n<p>Human rights are well-established in international standards and laws, and this has obligated organizations to implement specialized training that equips employees to address human rights in the course of their regular work. The total number of employees trained and the amount of training they receive both contribute to an assessment of an organization&rsquo;s depth of knowledge about human rights.</p>\n</div>\n</div>\n</div>",
              "number" : 12,
              "recommendations" : [ {
                "description" : "When compiling the information specified in Disclosure 412-2, the reporting organization should use data from Disclosure 102-7 in GRI 102: General Disclosures to identify the total number of employees."
              } ],
              "reportingRequirements" : [ {
                "description" : "Total number of hours in the reporting period devoted to training on human rights policies or procedures concerning aspects of human rights that are relevant to operations."
              }, {
                "description" : "Percentage of employees trained during the reporting period in human rights policies or procedures concerning aspects of human rights that are relevant to operations."
              } ],
              "series" : "400",
              "subNumber" : 2,
              "title" : "Employee training on human rights policies or procedures"
            },
            {
              "disclosureNumber" : "412-3",
              "guidance" : "<div class=\"page\" title=\"Page 9\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 412-3</strong></p>\n<p>Human rights screening refers to a formal or documented process that applies a set of human rights performance criteria as one of the factors to determine whether to proceed with a business relationship.</p>\n<p>Significant agreements and contracts can be determined by the level of approval required within an organization for the investment. Other criteria can also be used to determine significance if they can be consistently applied to all agreements.</p>\n<p>If multiple significant investment agreements are undertaken and contracts signed with the same partner, the total number of agreements reflects the total number of separate projects undertaken or entities created.</p>\n<div class=\"page\" title=\"Page 9\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>This disclosure is one measure of the extent to which human rights considerations are integrated into an organization&rsquo;s economic decisions. This is particularly relevant for organizations that operate within, or are partners in ventures in regions where the protection of human rights is of significant concern.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 12,
              "recommendations" : [ {
                "description" : "When compiling the information specified in Disclosure 412-3, the reporting organization should:",
                "subRecommendations" : [ {
                  "description" : "include the total number of significant investment agreements and contracts finalized during the reporting period that either moved the organization into a position of ownership in another entity, or initiated a capital investment project that was material to financial accounts;include the total number of significant investment agreements and contracts finalized during the reporting period that either moved the organization into a position of ownership in another entity, or initiated a capital investment project that was material to financial accounts;"
                }, {
                  "description" : "include only the agreements and contracts that are significant in terms of size or strategic importance."
                } ]
              } ],
              "reportingRequirements" : [ {
                "description" : "Total number and percentage of significant investment agreements and contracts that include human rights clauses or that underwent human rights screening."
              }, {
                "description" : "The definition used for ‘significant investment agreements’."
              } ],
              "series" : "400",
              "subNumber" : 3,
              "title" : "Significant investment agreements and contracts that include human rights clauses or that underwent human rights screening"
            },
            {
              "disclosureNumber" : "413-1",
              "guidance" : "<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>A key element in managing impacts on people in local communities is assessment and planning in order to understand the actual and potential impacts, and strong engagement with local communities to understand their expectations and needs. There are many elements that can be incorporated into local community engagement, impact assessments, and development programs. This disclosure seeks to identify which elements have been consistently applied, organization-wide.</p>\n<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Where possible, organizations are expected to anticipate and avoid negative impacts on local communities. Where this is not possible, or where residual impacts remain, organizations are expected to manage those impacts appropriately, including grievances, and to compensate local communities for negative impacts.</p>\n<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Establishing a timely and effective stakeholder identification and engagement process is important to help organizations understand the vulnerability of local communities and how these might be affected by the organization&rsquo;s activities. A stakeholder engagement process both in early planning stages as well as during operations, can help establish<br /> lines of communication between an organization&rsquo;s various departments (planning, finance, environment, production, etc.) and key stakeholder interest groups in the community. This enables an organization to consider the views of community stakeholders in its decisions, and to address its potential impacts on local communities in a timely manner.</p>\n<p>Organizations can utilize a number of useful tools to engage communities, including social and human rights impact assessments, which include a diverse set of approaches for proper identification of stakeholders and community characteristics. These can be based on issues such as ethnic background, indigenous descent, gender, age, migrant status, socioeconomic status, literacy levels, disabilities, income level, infrastructure availability or specific human health vulnerabilities which may exist within stakeholder communities.</p>\n</div>\n<div class=\"column\">\n<p>An organization is expected to consider the differentiated nature of local communities and to<br /> take specific action to identify and engage vulnerable groups. This might require adopting differentiated measures to allow the effective participation of vulnerable groups, such as making information available in alternate languages or format for those who are<br /> not literate or who do not have access to printed materials. Where necessary, organizations are expected to establish additional or separate processes so that negative impacts on vulnerable or disadvantaged groups are avoided, minimized, mitigated or compensated.1</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 13,
              "recommendations" : [ {
                "description" : "When compiling the information specified in Disclosure 413-1, the reporting organization should use data from Disclosure 102-7 in GRI 102: General Disclosures to identify the total number of operations."
              } ],
              "reportingRequirements" : [ {
                "description" : "Percentage of operations with implemented local community engagement, impact assessments, and/or development programs, including the use of:",
                "subRequirements" : [ {
                  "description" : "social impact assessments, including gender impact assessments, based on participatory processes;"
                }, {
                  "description" : "environmental impact assessments and ongoing monitoring;"
                }, {
                  "description" : "public disclosure of results of environmental and social impact assessments;"
                }, {
                  "description" : "local community development programs based on local communities’ needs;"
                }, {
                  "description" : "stakeholder engagement plans based on stakeholder mapping;"
                }, {
                  "description" : "broad based local community consultation committees and processes that include vulnerable groups;"
                }, {
                  "description" : "works councils, occupational health and safety committees and other worker representation bodies to deal with impacts;"
                }, {
                  "description" : "formal local community grievance processes."
                } ]
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Operations with local community engagement, impact assessments, and development programs"
            },
            {
              "disclosureNumber" : "413-2",
              "guidance" : "<div class=\"page\" title=\"Page 10\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 413-2</strong></p>\n<p>Internal sources of information about actual and potential negative impacts of operations on local communities can include:</p>\n<ul>\n<li>\n<p>actual performance data;</p>\n</li>\n<li>\n<p>internal investment plans and associated risk assessments;</p>\n</li>\n<li>\n<p>all data collected with topic-specific disclosures as they relate to individual communities. For example: GRI 203: Indirect Economic Impacts, GRI 301: Materials, GRI 302: Energy, GRI 303: Water, GRI 304: Biodiversity, GRI 305: Emissions, GRI 306: Effluents and Waste, GRI 403: Occupational Health and Safety, GRI 408: Child Labor, GRI 409: Forced or Compulsory Labor, GRI 410: Security Practices, GRI 411: Rights of Indigenous Peoples, and GRI 416: Customer Health and Safety.</p>\n</li>\n</ul>\n</div>\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>This disclosure is focused on significant actual and potential negative impacts related to an organization&rsquo;s operations and not on community investments or donations, which are addressed by GRI 201: Economic Performance.</p>\n<p>This disclosure informs stakeholders about an organization&rsquo;s awareness of its negative impacts on local communities. It also enables the organization<br /> to better prioritize and improve its organization-wide attention to local communities.</p>\n</div>\n</div>\n</div>",
              "number" : 13,
              "recommendations" : [ {
                "description" : "When compiling the information specified in Disclosure 413-2, the reporting organization should:",
                "subRecommendations" : [ {
                  "description" : "report the vulnerability and risk to local communities from potential negative impacts due to factors including:",
                  "subRecommendations" : [ {
                    "description" : "the degree of physical or economic isolation of the local community;"
                  }, {
                    "description" : "the level of socioeconomic development, including the degree of gender equality within the community;"
                  }, {
                    "description" : "the state of socioeconomic infrastructure, including health and education infrastructure;"
                  }, {
                    "description" : "the proximity to operations;"
                  }, {
                    "description" : "the level of social organization;"
                  }, {
                    "description" : "the strength and quality of the governance of local and national institutions around local communities;"
                  } ]
                }, {
                  "description" : "report the exposure of the local community to its operations due to higher than average use of shared resources or impact on shared resources, including:",
                  "subRecommendations" : [ {
                    "description" : "the use of hazardous substances that impact the environment and human health in general, and specifically impact reproductive health;"
                  }, {
                    "description" : "the volume and type of pollution released;"
                  }, {
                    "description" : "the status as major employer in the local community;"
                  }, {
                    "description" : "land conversion and resettlement;"
                  }, {
                    "description" : "natural resource consumption;"
                  } ]
                }, {
                  "description" : "for each of the significant actual and potential negative economic, social, cultural, and/or environmental impacts on local communities and their rights, describe:",
                  "subRecommendations" : [ {
                    "description" : "the intensity or severity of the impact;"
                  }, {
                    "description" : "the likely duration of the impact;"
                  }, {
                    "description" : "the reversibility of the impact;"
                  }, {
                    "description" : "the scale of the impact."
                  } ]
                } ]
              } ],
              "reportingRequirements" : [ {
                "description" : "Operations with significant actual and potential negative impacts on local communities, including:",
                "subRequirements" : [ {
                  "description" : "the location of the operations;the location of the operations;"
                }, {
                  "description" : "the significant actual and potential negative impacts of operations."
                } ]
              } ],
              "series" : "400",
              "subNumber" : 2,
              "title" : "Operations with significant actual and potential negative impacts on local communities"
            },
            {
              "disclosureNumber" : "414-1",
              "guidance" : "<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 414-1</strong></p>\n<p>Social criteria can include the topics in the 400 series (Social topics).</p>\n<p><strong>Background</strong></p>\n<p>This disclosure informs stakeholders about the percentage of suppliers selected or contracted subject to due diligence processes for social impacts.</p>\n</div>\n<div class=\"column\">\n<p>An organization is expected to initiate due diligence as early as possible in the development of a new relationship with a supplier.</p>\n<p>Impacts may be prevented or mitigated at the stage of structuring contracts or other agreements, as well as via ongoing collaboration with suppliers.</p>\n</div>\n</div>\n</div>",
              "number" : 14,
              "reportingRequirements" : [ {
                "description" : "Percentage of new suppliers that were screened using social criteria."
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "New suppliers that were screened using social criteria"
            },
            {
              "disclosureNumber" : "414-2",
              "guidance" : "<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 414-2</strong></p>\n<p>Negative impacts include those that are either caused or contributed to by an organization, or that are directly linked to its activities, products, or services by its relationship with a supplier.</p>\n<p>Assessments for social impacts can include the topics in the 400 series (Social topics).</p>\n<p>Assessments can be made against agreed performance expectations that are set and communicated to the suppliers prior to the assessment.</p>\n<p>Assessments can be informed by audits, contractual reviews, two-way engagement, and complaint and grievance mechanisms.</p>\n<p>Improvements can include changing an organization&rsquo;s procurement practices, the adjustment of performance expectations, capacity building, training, and changes to processes.</p>\n<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>This disclosure informs stakeholders about an organization&rsquo;s awareness of significant actual and potential negative social impacts in the supply chain.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 14,
              "recommendations" : [ {
                "description" : "When compiling the information specified in Disclosure 414-2, the reporting organization should, where it provides appropriate context on significant impacts, provide a breakdown of the information by:",
                "subRecommendations" : [ {
                  "description" : "the location of the supplier;"
                }, {
                  "description" : "the significant actual and potential negative social impact."
                } ]
              } ],
              "reportingRequirements" : [ {
                "description" : "Number of suppliers assessed for social impacts."
              }, {
                "description" : "Number of suppliers identified as having significant actual and potential negative social impacts."
              }, {
                "description" : "Significant actual and potential negative social impacts identified in the supply chain."
              }, {
                "description" : "Percentage of suppliers identified as having significant actual and potential negative social impacts with which improvements were agreed upon as a result of assessment."
              }, {
                "description" : "Percentage of suppliers identified as having significant actual and potential negative social impacts with which relationships were terminated as a result of assessment, and why."
              } ],
              "series" : "400",
              "subNumber" : 2,
              "title" : "Negative social impacts in the supply chain and actions taken"
            },
            {
              "additionalRequirements" : [ {
                "description" : "When compiling the information specified in Disclosure 415-1, the reporting organization shall calculate financial political contributions in compliance with national accounting rules, where these exist."
              } ],
              "disclosureNumber" : "415-1",
              "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>The purpose of this disclosure is to identify an organization&rsquo;s support for political causes.</p>\n<p>This disclosure can provide an indication of the extent to which an organization&rsquo;s political contributions are in line with its stated policies, goals, or other<br /> public positions.</p>\n<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Direct or indirect contributions to political causes can also present corruption risks, because they can be used to exert undue influence on the political process. Many countries have legislation that limits the amount an organization can spend on political parties and candidates for campaigning purposes. If an organization channels contributions indirectly through intermediaries, such as lobbyists or organizations linked to political causes, it can improperly circumvent such legislation.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
              "number" : 15,
              "reportingRequirements" : [ {
                "description" : "Total monetary value of financial and in-kind political contributions made directly and indirectly by the organization by country and recipient/beneficiary."
              }, {
                "description" : "If applicable, how the monetary value of in-kind contributions was estimated."
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Political contributions"
            },
            {
              "disclosureNumber" : "416-1",
              "guidance" : "<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 416-1</strong></p>\n<p>This measure helps to identify the existence and range of systematic efforts to address health and safety across the life cycle of a product or service. In reporting the information in Disclosure 416-1, the reporting organization can also describe the criteria used for<br /> the assessment.</p>\n</div>\n</div>\n</div>",
              "number" : 16,
              "reportingRequirements" : [ {
                "description" : "Percentage of significant product and service categories for which health and safety impacts are assessed for improvement."
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Assessment of the health and safety impacts of product and service categories"
            },
            {
              "additionalRequirements" : [ {
                "description" : "When compiling the information specified in Disclosure 416-2, the reporting organization shall:",
                "subRequirements" : [ {
                  "description" : "exclude incidents of non-compliance in which the organization was determined not to be at fault;"
                }, {
                  "description" : "exclude incidents of non-compliance related to labeling. Incidents related to labeling are reported in Disclosure 417-2 of GRI 417: Marketing and Labeling;"
                }, {
                  "description" : "if applicable, identify any incidents of non-compliance that relate to events in periods prior to the reporting period."
                } ]
              } ],
              "disclosureNumber" : "416-2",
              "guidance" : "<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 416-2</strong></p>\n<p>The incidents of non-compliance that occur within the reporting period can relate to incidents formally resolved during the reporting period, whether they occurred in periods prior to the reporting period or not.</p>\n<p><strong>Background</strong></p>\n<p>Protection of health and safety is a recognized goal of many national and international regulations. Customers expect products and services to perform</p>\n</div>\n<div class=\"column\">\n<p>their intended functions satisfactorily, and not pose a risk to health and safety. Customers have a right to non-hazardous products. Where their health and safety is affected, customers also have the right to seek redress.</p>\n<p>This disclosure addresses the life cycle of the product or service once it is available for use, and therefore subject to regulations and voluntary codes concerning the health and safety of products and services.</p>\n</div>\n</div>\n</div>",
              "number" : 16,
              "reportingRequirements" : [ {
                "description" : "Total number of incidents of non-compliance with regulations and/or voluntary codes concerning the health and safety impacts of products and services within the reporting period, by:",
                "subRequirements" : [ {
                  "description" : "incidents of non-compliance with regulations resulting in a fine or penalty;"
                }, {
                  "description" : "incidents of non-compliance with regulations resulting in a warning;"
                }, {
                  "description" : "incidents of non-compliance with voluntary codes."
                } ]
              }, {
                "description" : "If the organization has not identified any non-compliance with regulations and/or voluntary codes, a brief statement of this fact is sufficient."
              } ],
              "series" : "400",
              "subNumber" : 2,
              "title" : "Incidents of non-compliance concerning the health and safety impacts of products and services"
            },
            {
              "disclosureNumber" : "417-1",
              "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Customers and end users need accessible and adequate information about the positive and negative environmental and social impacts of products and services. This can include information on the safe use of a product or service, the disposal of the product, or the sourcing of its components. Access to this information helps customers to make informed purchasing choices.</p>\n</div>\n</div>\n</div>",
              "number" : 17,
              "reportingRequirements" : [ {
                "description" : "Whether each of the following types of information is required by the organization’s procedures for product and service information and labeling:",
                "subRequirements" : [ {
                  "description" : "The sourcing of components of the product or service;"
                }, {
                  "description" : "Content, particularly with regard to substances that might produce an environmental or social impact;"
                }, {
                  "description" : "Safe use of the product or service;"
                }, {
                  "description" : "Disposal of the product and environmental or social impacts;"
                }, {
                  "description" : "Other (explain)."
                } ]
              }, {
                "description" : "Percentage of significant product or service categories covered by and assessed for compliance with such procedures."
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Requirements for product and service information and labeling"
            },
            {
              "disclosureNumber" : "417-2",
              "guidance" : "<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 417-2</strong></p>\n<p>The incidents of non-compliance that occur within the reporting period can relate to incidents formally resolved during the reporting period, whether they occurred in periods prior to the reporting period or not.</p>\n<p><strong>Background</strong></p>\n<p>Providing appropriate information and labeling with respect to economic, environmental, and social impacts can be linked to compliance with certain types of regulations, laws, and codes.</p>\n<p>It is, for example, linked to compliance with regulations, national laws, and the Organisation for Economic Co-operation and Development (OECD) OECD Guidelines for Multinational Enterprises. It is also potentially linked to compliance with strategies for brand and market differentiation.</p>\n</div>\n<div class=\"column\">\n<p>The display and provision of information and labeling for products and services are subject to<br /> many regulations and laws. Non-compliance can indicate either inadequate internal management systems and procedures or ineffective implementation. The trends revealed by this disclosure can indicate improvements or a deterioration in the effectiveness of internal controls.</p>\n</div>\n</div>\n</div>",
              "number" : 17,
              "reportingRequirements" : [ {
                "description" : "Total number of incidents of non-compliance with regulations and/or voluntary codes concerning product and service information and labeling, by:",
                "subRequirements" : [ {
                  "description" : "incidents of non-compliance with regulations resulting in a fine or penalty;"
                }, {
                  "description" : "incidents of non-compliance with regulations resulting in a warning;"
                }, {
                  "description" : "incidents of non-compliance with voluntary codes."
                } ]
              }, {
                "description" : "If the organization has not identified any non-compliance with regulations and/or voluntary codes, a brief statement of this fact is sufficient."
              } ],
              "series" : "400",
              "subNumber" : 2,
              "title" : "Incidents of non-compliance concerning product and service information and labeling"
            },
            {
              "disclosureNumber" : "417-3",
              "guidance" : "<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 417-3</strong></p>\n<p>The incidents of non-compliance that occur within the reporting period can relate to incidents formally resolved during the reporting period, whether they occurred in periods prior to the reporting period or not.</p>\n<p><strong>Background</strong></p>\n<p>Marketing is an important method of communication between organizations and customers, and is subject to many regulations, laws, and voluntary codes, such as the International Chamber of Commerce (ICC)&rsquo;s Consolidated Code of Advertising and Marketing Communication Practice.</p>\n</div>\n<div class=\"column\">\n<p>An organization is expected to use fair and responsible practices in its business and dealings with customers. Fair and responsible marketing requires the organization to communicate transparently about the economic, environmental, and social impacts of its brands, products, and services. Fair and responsible marketing also avoids any deceptive, untruthful, or discriminatory claims, and does not take advantage<br /> of a customers&rsquo; lack of knowledge or choices.</p>\n</div>\n</div>\n</div>",
              "number" : 17,
              "reportingRequirements" : [ {
                "description" : "Total number of incidents of non-compliance with regulations and/or voluntary codes concerning marketing communications, including advertising, promotion, and sponsorship, by:",
                "subRequirements" : [ {
                  "description" : "incidents of non-compliance with regulations resulting in a fine or penalty;"
                }, {
                  "description" : "incidents of non-compliance with regulations resulting in a warning;"
                }, {
                  "description" : "incidents of non-compliance with voluntary codes."
                } ]
              }, {
                "description" : "If the organization has not identified any non-compliance with regulations and/or voluntary codes, a brief statement of this fact is sufficient."
              } ],
              "series" : "400",
              "subNumber" : 3,
              "title" : "Incidents of non-compliance concerning marketing communications"
            },
            {
              "disclosureNumber" : "418-1",
              "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Protection of customer privacy is a generally recognized goal in national regulations and organizational policies. As set out in the Organisation for Economic Co-operation and Development (OECD) OECD Guidelines for Multinational Enterprises, organizations<br /> are expected to &lsquo;respect consumer privacy and take reasonable measures to ensure the security of personal data that they collect, store, process or disseminate&rsquo;.</p>\n<p>To protect customer privacy, an organization is expected to limit its collection of personal data, to collect data by lawful means, and to be transparent about how data are gathered, used, and secured. The organization is also expected to not disclose or use personal customer information for any purposes other than those agreed upon, and to communicate any changes in data protection policies or measures to customers directly.</p>\n</div>\n<div class=\"column\">\n<p>This disclosure provides an evaluation of the success of management systems and procedures relating to customer privacy protection.</p>\n</div>\n</div>\n</div>",
              "number" : 18,
              "reportingRequirements" : [ {
                "description" : "Total number of substantiated complaints received concerning breaches of customer privacy, categorized by:",
                "subRequirements" : [ {
                  "description" : "complaints received from outside parties and substantiated by the organization;"
                }, {
                  "description" : "complaints from regulatory bodies."
                } ]
              }, {
                "description" : "Total number of identified leaks, thefts, or losses of customer data."
              }, {
                "description" : "If the organization has not identified any substantiated complaints, a brief statement of this fact is sufficient."
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Substantiated complaints concerning breaches of customer privacy and losses of customer data"
            },
            {
              "disclosureNumber" : "419-1",
              "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 419-1</strong></p>\n<p>Relevant information for this disclosure can include data as reported with GRI 416: Customer Health and Safety and GRI 417: Marketing and Labeling.</p>\n</div>\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Non-compliance within an organization can indicate the ability of management to ensure that operations conform to certain performance parameters. In some circumstances, non-compliance can lead to remediation obligations or other costly liabilities. The strength of an organization&rsquo;s compliance record can also affect its ability to expand operations or gain permits.</p>\n</div>\n</div>\n</div>",
              "number" : 19,
              "recommendations" : [ {
                "description" : "When compiling the information specified in Disclosure 419-1, the reporting organization should include administrative and judicial sanctions for failure to comply with laws and/or regulations in the social and economic area, including:",
                "subRecommendations" : [ {
                  "description" : "international declarations, conventions, and treaties;"
                }, {
                  "description" : "national, sub-national, regional, and local regulations;"
                }, {
                  "description" : "cases brought against the organization through the use of international dispute mechanisms or national dispute mechanisms supervised by government authorities."
                } ]
              } ],
              "reportingRequirements" : [ {
                "description" : "Significant fines and non-monetary sanctions for non-compliance with laws and/or regulations in the social and economic area in terms of:",
                "subRequirements" : [ {
                  "description" : "total monetary value of significant fines;"
                }, {
                  "description" : "total number of non-monetary sanctions;"
                }, {
                  "description" : "cases brought through dispute resolution mechanisms."
                } ]
              }, {
                "description" : "If the organization has not identified any non-compliance with laws and/or regulations, a brief statement of this fact is sufficient."
              }, {
                "description" : "The context against which significant fines and non-monetary sanctions were incurred."
              } ],
              "series" : "400",
              "subNumber" : 1,
              "title" : "Non-compliance with laws and regulations in the social and economic area"
            },
            
          ];
    },

    createGeneralDisclosure() {
        return [
            {
                "disclosureNumber" : "102-1",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Name of the organization."
                } ],
                "series" : "100",
                "subNumber" : 1,
                "title" : "Name of the organization"
              },
              {
                "disclosureNumber" : "102-2",
                "guidance" : "",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-2-b, the reporting organization should also explain whether it sells products or services that are the subject of stakeholder questions or public debate."
                } ],
                "reportingRequirements" : [ {
                  "description" : "A description of the organization’s activities."
                }, {
                  "description" : "Primary brands, products, and services, including an explanation of any products or services that are banned in certain markets."
                } ],
                "series" : "100",
                "subNumber" : 2,
                "title" : "Activities, brands, products, and services"
              },
              {
                "disclosureNumber" : "102-3",
                "guidance" : "<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Headquarters refers to an organization&rsquo;s administrative center, from which it is controlled or directed.</p>\n</div>\n</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Location of the organization’s headquarters."
                } ],
                "series" : "100",
                "subNumber" : 3,
                "title" : "Location of headquarters"
              },
              {
                "disclosureNumber" : "102-4",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Number of countries where the organization operates, and the names of countries where it has significant operations and/or that are relevant to the topics covered in the report."
                } ],
                "series" : "100",
                "subNumber" : 4,
                "title" : "Location of operations"
              },
              {
                "disclosureNumber" : "102-5",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Nature of ownership and legal form."
                } ],
                "series" : "100",
                "subNumber" : 5,
                "title" : "Ownership and legal form"
              },
              {
                "disclosureNumber" : "102-6",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Markets served, including:",
                  "subRequirements" : [ {
                    "description" : "geographic locations where products and services are offered;"
                  }, {
                    "description" : "sectors served;"
                  }, {
                    "description" : "types of customers and beneficiaries."
                  } ]
                } ],
                "series" : "100",
                "subNumber" : 6,
                "title" : "Markets served"
              },
              {
                "disclosureNumber" : "102-7",
                "guidance" : "",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-7, the reporting organization should provide the following additional information:",
                  "subRecommendations" : [ {
                    "description" : "Total assets;"
                  }, {
                    "description" : "Beneficial ownership, including the identity and percentage of ownership of the largest shareholders;"
                  }, {
                    "description" : "Breakdowns of:",
                    "subRecommendations" : [ {
                      "description" : "net sales or net revenues by countries or regions that make up five percent or more of total revenues;"
                    }, {
                      "description" : "costs by countries or regions that make up five percent or more of total costs;"
                    }, {
                      "description" : "total number of employees by country or region."
                    } ]
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Scale of the organization, including:",
                  "subRequirements" : [ {
                    "description" : "total number of employees;"
                  }, {
                    "description" : "total number of operations;"
                  }, {
                    "description" : "net sales (for private sector organizations) or net revenues (for public sector organizations);"
                  }, {
                    "description" : "total capitalization (for private sector organizations) broken down in terms of debt and equity;"
                  }, {
                    "description" : "quantity of products or services provided."
                  } ]
                } ],
                "series" : "100",
                "subNumber" : 7,
                "title" : "Scale of the organization"
              },
              {
                "disclosureNumber" : "102-8",
                "guidance" : "<div class=\"page\" title=\"Page 10\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 102-8-d</strong></p>\n<p>The organization&rsquo;s activities are reported in Disclosure 102-2-a.</p>\n<p><strong>Background</strong></p>\n<div class=\"page\" title=\"Page 10\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Breaking down these data by gender enables an understanding of gender representation across an organization, and of the optimal use of available labor and talent.</p>\n<p>See references 6, 7, 10 and 12 in the References section.</p>\n</div>\n</div>\n</div>\n<p>The number of employees and workers involved in an organization&rsquo;s activities provides insight into the scale of impacts created by labor issues.</p>\n</div>\n</div>\n</div>",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-8, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "express employee numbers as either head count or Full Time Equivalent (FTE), with the chosen approach stated and applied consistently;"
                  }, {
                    "description" : "identify the contract type and full-time and part-time status of employees based on the definitions under the national laws of the country where they are based;"
                  }, {
                    "description" : "use numbers as at the end of the reporting period, unless there has been a material change during the reporting period;"
                  }, {
                    "description" : "combine country statistics to calculate global statistics, and disregard differences in legal definitions. Although what constitutes a type of contract and employment type varies between countries, the global figure should still reflect the relationships under law."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Total number of employees by employment contract (permanent and temporary), by gender."
                }, {
                  "description" : "Total number of employees by employment contract (permanent and temporary), by region."
                }, {
                  "description" : "Total number of employees by employment type (full-time and part-time), by gender."
                }, {
                  "description" : "Whether a significant portion of the organization’s activities are performed by workers who are not employees. If applicable, a description of the nature and scale of work performed by workers who are not employees."
                }, {
                  "description" : "Any significant variations in the numbers reported in Disclosures 102-8-a, 102-8-b, and 102-8-c (such as seasonal variations in the tourism or agricultural industries)."
                }, {
                  "description" : "An explanation of how the data have been compiled, including any assumptions made."
                } ],
                "series" : "100",
                "subNumber" : 8,
                "title" : "Information on employees and other workers"
              },
              {
                "disclosureNumber" : "102-9",
                "guidance" : "<div class=\"page\" title=\"Page 11\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Examples of elements that can be covered in the description include:</p>\n<ul>\n<li>\n<p>the types of suppliers engaged;</p>\n</li>\n<li>\n<p>the total number of suppliers engaged by an organization and the estimated number of suppliers throughout the supply chain;</p>\n</li>\n<li>\n<p>the geographic location of suppliers;</p>\n</li>\n<li>\n<p>the estimated monetary value of payments made to suppliers;</p>\n</li>\n<li>\n<p>the supply chain&rsquo;s sector-specific characteristics, such as how labor intensive it is.</p>\n</li>\n</ul>\n<div class=\"page\" title=\"Page 11\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>This disclosure sets the overall context for understanding an organization&rsquo;s supply chain.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "A description of the organization’s supply chain, including its main elements as they relate to the organization’s activities, primary brands, products, and services."
                } ],
                "series" : "100",
                "subNumber" : 9,
                "title" : "Supply chain"
              },
              {
                "disclosureNumber" : "102-10",
                "guidance" : "<div class=\"page\" title=\"Page 12\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>This disclosure covers significant changes during the <span style=\"text-decoration: underline;\">reporting period</span>.</p>\n<p>Significant changes to the supply chain are those that can cause or contribute to significant economic, environmental, and social <span style=\"text-decoration: underline;\">impacts</span>.</p>\n<div class=\"page\" title=\"Page 12\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Examples of significant changes can include:</p>\n<ul>\n<li>\n<p>moving parts of the supply chain from one country to another;</p>\n</li>\n<li>\n<p>changing the structure of the supply chain, such as the outsourcing of a significant part of an organization&rsquo;s activities.</p>\n</li>\n</ul>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Significant changes to the organization’s size, structure, ownership, or supply chain, including:",
                  "subRequirements" : [ {
                    "description" : "Changes in the location of, or changes in, operations, including facility openings, closings, and expansions;"
                  }, {
                    "description" : "Changes in the share capital structure and other capital formation, maintenance, and alteration operations (for private sector organizations);"
                  }, {
                    "description" : "Changes in the location of suppliers, the structure of the supply chain, or relationships with suppliers, including selection and termination."
                  } ]
                } ],
                "series" : "100",
                "subNumber" : 10,
                "title" : "Significant changes to the organization and its supply chain"
              },
              {
                "disclosureNumber" : "102-11",
                "guidance" : "<div class=\"page\" title=\"Page 12\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Disclosure 102-11 can include an organization&rsquo;s approach to risk management in operational planning, or when developing and introducing new <span style=\"text-decoration: underline;\">products</span>.</p>\n<p><strong>Background</strong></p>\n<p>The precautionary approach was introduced by the United Nations in Principle 15 of &lsquo;The Rio Declaration on Environment and Development&rsquo;. It states: &lsquo;In order to protect the environment, the precautionary approach shall be widely applied by States according to their capabilities. Where there are threats of serious or irreversible damage, lack of full scientific certainty shall not be used as a reason for postponing cost-effective measures to prevent environmental degradation.&rsquo; Applying the Precautionary Principle can help an organization to reduce or to avoid negative impacts on the environment. See reference 13 in the References section.</p>\n</div>\n</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Whether and how the organization applies the Precautionary Principle or approach."
                } ],
                "series" : "100",
                "subNumber" : 11,
                "title" : "Precautionary Principle or approach"
              },
              {
                "disclosureNumber" : "102-12",
                "guidance" : "",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-12, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "include the date of adoption, the countries or operations where applied, and the range of stakeholders involved in the development and governance of these initiatives;"
                  }, {
                    "description" : "differentiate between non-binding, voluntary initiatives and those with which the organization has an obligation to comply."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "A list of externally-developed economic, environmental and social charters, principles, or other initiatives to which the organization subscribes, or which it endorses."
                } ],
                "series" : "100",
                "subNumber" : 12,
                "title" : "External initiatives"
              },
              {
                "disclosureNumber" : "102-13",
                "guidance" : "",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-13, the reporting organization should include memberships maintained at the organizational level in associations or organizations in which it holds a position on the governance body, participates in projects or committees, provides substantive funding beyond routine membership dues, or views its membership as strategic."
                } ],
                "reportingRequirements" : [ {
                  "description" : "A list of the main memberships of industry or other associations, and national or international advocacy organizations."
                } ],
                "series" : "100",
                "subNumber" : 13,
                "title" : "Membership of associations"
              },
              {
                "disclosureNumber" : "102-14",
                "guidance" : "<div class=\"page\" title=\"Page 14\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>See references 14, 15 and 16 in the References section.</p>\n</div>\n</div>\n</div>",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-14, the reporting organization should include:",
                  "subRecommendations" : [ {
                    "description" : "the overall vision and strategy for the short-term, medium-term, and long-term, with respect to managing the significant economic, environmental, and social impacts that the organization causes, contributes to, or that are directly linked to its activities, products or services as a result of relationships with others (such as suppliers and persons or organizations in local communities);"
                  }, {
                    "description" : "strategic priorities and key topics for the short and medium-term with respect to sustainability, including observance of internationally-recognized standards and how such standards relate to long-term organizational strategy and success;"
                  }, {
                    "description" : "broader trends (such as macroeconomic or political) affecting the organization and influencing its sustainability priorities;"
                  }, {
                    "description" : "key events, achievements, and failures during the reporting period;"
                  }, {
                    "description" : "views on performance with respect to targets;"
                  }, {
                    "description" : "outlook on the organization’s main challenges and targets for the next year and goals for the coming 3–5 years;"
                  }, {
                    "description" : "other items pertaining to the organization’s strategic approach."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "A statement from the most senior decision-maker of the organization (such as CEO, chair, or equivalent senior position) about the relevance of sustainability to the organization and its strategy for addressing sustainability."
                } ],
                "series" : "100",
                "subNumber" : 14,
                "title" : "Statement from senior decision-maker"
              },
              {
                "disclosureNumber" : "102-15",
                "guidance" : "",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-15, the reporting organization should include:",
                  "subRecommendations" : [ {
                    "description" : "a description of its significant economic, environmental and social impacts, and associated challenges and opportunities. This includes the effects on stakeholders and their rights as defined by national laws and relevant internationally-recognized standards;"
                  }, {
                    "description" : "the range of reasonable expectations and interests of the organization’s stakeholders;"
                  }, {
                    "description" : "an explanation of the approach to prioritizing these challenges and opportunities;"
                  }, {
                    "description" : "key conclusions about progress in addressing these topics and related performance in the reporting period, including an assessment of reasons for underperformance or overperformance;"
                  }, {
                    "description" : "a description of the main processes in place to address performance, and relevant changes;"
                  }, {
                    "description" : "the impact of sustainability trends, risks, and opportunities on the long-term prospects and financial performance of the organization;"
                  }, {
                    "description" : "information relevant to financial stakeholders or that could become so in the future;"
                  }, {
                    "description" : "a description of the most important risks and opportunities for the organization arising from sustainability trends;"
                  }, {
                    "description" : "prioritization of key economic, environmental, and social topics as risks and opportunities according to their relevance for long-term organizational strategy, competitive position, qualitative, and, if possible, quantitative financial value drivers;"
                  }, {
                    "description" : "table(s) summarizing targets, performance against targets, and lessons learned for the current reporting period;"
                  }, {
                    "description" : "table(s) summarizing targets for the next reporting period and medium-term objectives and goals (i.e., 3–5 years) related to key risks and opportunities;"
                  }, {
                    "description" : "a description of governance mechanisms in place specifically to manage these risks and opportunities, and identification of other related risks and opportunities."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "A description of key impacts, risks, and opportunities."
                } ],
                "series" : "100",
                "subNumber" : 15,
                "title" : "Key impacts, risks, and opportunities"
              },
              {
                "disclosureNumber" : "102-16",
                "guidance" : "<div class=\"page\" title=\"Page 16\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Values, principles, standards and norms of behavior can include codes of conduct and ethics. The highest governance body&rsquo;s and senior executives&rsquo; roles in the development, approval, and updating of value statements is reported under Disclosure 102-26.</p>\n</div>\n</div>\n</div>",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-16, the reporting organization should provide additional information about its values, principles, standards, and norms of behavior, including:",
                  "subRecommendations" : [ {
                    "description" : "how they were developed and approved;"
                  }, {
                    "description" : "whether training on them is given regularly to all and new governance body members, workers performing the organization’s activities, and business partners;"
                  }, {
                    "description" : "whether they need to be read and signed regularly by all and new governance body members, workers performing the organization’s activities, and business partners;"
                  }, {
                    "description" : "whether any executive-level positions maintain responsibility for them;"
                  }, {
                    "description" : "whether they are available in different languages to reach all governance body members, workers performing the organization’s activities, business partners, and other stakeholders."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "A description of the organization’s values, principles, standards, and norms of behavior."
                } ],
                "series" : "100",
                "subNumber" : 16,
                "title" : "Values, principles, standards, and norms of behavior"
              },
              {
                "disclosureNumber" : "102-17",
                "guidance" : "<div class=\"page\" title=\"Page 17\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Examples of elements that can be described include:</p>\n<ul>\n<li>\n<p>Who is assigned the overall responsibility for the mechanisms to seek advice about and report on behavior;</p>\n</li>\n<li>\n<p>Whether any mechanisms are independent of the organization;</p>\n</li>\n<li>\n<p>Whether and how <span style=\"text-decoration: underline;\">workers</span> performing the organization&rsquo;s activities, business partners, and other <span style=\"text-decoration: underline;\">stakeholders</span> are informed of the mechanisms;</p>\n</li>\n<li>\n<p>Whether training on them is given to workers performing the organization&rsquo;s activities and business partners;</p>\n</li>\n<li>\n<p>The availability and accessibility of the mechanisms to workers performing the organization&rsquo;s activities and business partners, such as the total number of hours per day, days per week, and availability in different languages;</p>\n</li>\n<li>\n<p>Whether requests for advice and concerns are treated confidentially;</p>\n</li>\n<li>\n<p>Whether the mechanisms can be used anonymously;</p>\n</li>\n<li>\n<p>The total number of requests for advice received, their type, and the percentage that were answered during the <span style=\"text-decoration: underline;\">reporting period</span>;</p>\n</li>\n<li>\n<p>The total number of concerns reported, the type of misconduct reported, and the percentage of concerns that were addressed, resolved, or found to be unsubstantiated during the reporting period;</p>\n</li>\n<li>\n<p>Whether the organization has a non-retaliation policy;</p>\n</li>\n<li>\n<p>The process through which concerns are investigated;</p>\n</li>\n<li>\n<p>The level of satisfaction of those who used the mechanisms.</p>\n</li>\n</ul>\n</div>\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>An organization can provide means for stakeholders to seek advice about ethical and lawful behavior, and organizational integrity, or to report concerns about these matters. These means can include escalating issues through line management, whistleblowing mechanisms, and hotlines.</p>\n</div>\n</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "A description of internal and external mechanisms for:",
                  "subRequirements" : [ {
                    "description" : "seeking advice about ethical and lawful behavior, and organizational integrity;"
                  }, {
                    "description" : "reporting concerns about unethical or unlawful behavior, and organizational integrity."
                  } ]
                } ],
                "series" : "100",
                "subNumber" : 17,
                "title" : "Mechanisms for advice and concerns about ethics"
              },
              {
                "disclosureNumber" : "102-18",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Governance structure of the organization, including committees of the highest governance body."
                }, {
                  "description" : "Committees responsible for decision-making on economic, environmental, and social topics."
                } ],
                "series" : "100",
                "subNumber" : 18,
                "title" : "Governance structure"
              },
              {
                "disclosureNumber" : "102-19",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Process for delegating authority for economic, environmental, and social topics from the highest governance body to senior executives and other employees."
                }, {
                  "description" : ""
                } ],
                "series" : "100",
                "subNumber" : 19,
                "title" : "Delegating authority"
              },
              {
                "disclosureNumber" : "102-20",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Whether the organization has appointed an executive-level position or positions with responsibility for economic, environmental, and social topics."
                }, {
                  "description" : "Whether post holders report directly to the highest governance body."
                } ],
                "series" : "100",
                "subNumber" : 20,
                "title" : "Executive-level responsibility for economic, environmental, and social topics"
              },
              {
                "disclosureNumber" : "102-21",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Processes for consultation between stakeholders and the highest governance body on economic, environmental, and social topics."
                }, {
                  "description" : "If consultation is delegated, describe to whom it is delegated and how the resulting feedback is provided to the highest governance body."
                } ],
                "series" : "100",
                "subNumber" : 21,
                "title" : "Consulting stakeholders on economic, environmental, and social topics"
              },
              {
                "disclosureNumber" : "102-22",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Composition of the highest governance body and its committees by:",
                  "subRequirements" : [ {
                    "description" : "executive or non-executive;"
                  }, {
                    "description" : "independence;"
                  }, {
                    "description" : "tenure on the governance body;"
                  }, {
                    "description" : "number of each individual’s other significant positions and commitments, and the nature of the commitments;"
                  }, {
                    "description" : "gender;"
                  }, {
                    "description" : "membership of under-represented social groups;"
                  }, {
                    "description" : "competencies relating to economic, environmental, and social topics;"
                  }, {
                    "description" : "stakeholder representation."
                  } ]
                } ],
                "series" : "100",
                "subNumber" : 22,
                "title" : "Composition of the highest governance body and its committees"
              },
              {
                "disclosureNumber" : "102-23",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Whether the chair of the highest governance body is also an executive officer in the organization."
                }, {
                  "description" : "If the chair is also an executive officer, describe his or her function within the organization’s management and the reasons for this arrangement."
                } ],
                "series" : "100",
                "subNumber" : 23,
                "title" : "Chair of the highest governance body"
              },
              {
                "disclosureNumber" : "102-24",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Nomination and selection processes for the highest governance body and its committees."
                }, {
                  "description" : "Criteria used for nominating and selecting highest governance body members, including whether and how:",
                  "subRequirements" : [ {
                    "description" : "stakeholders (including shareholders) are involved;"
                  }, {
                    "description" : "diversity is considered;"
                  }, {
                    "description" : "independence is considered;"
                  }, {
                    "description" : "expertise and experience relating to economic, environmental, and social topics are considered."
                  } ]
                } ],
                "series" : "100",
                "subNumber" : 24,
                "title" : "Nominating and selecting the highest governance body"
              },
              {
                "disclosureNumber" : "102-25",
                "guidance" : "<div class=\"page\" title=\"Page 21\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>See reference 11 in the References section.</p>\n</div>\n</div>\n</div>",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-25, the reporting organization should align the definition of controlling shareholder to the definition used for the purpose of the organization’s consolidated financial statements or equivalent documents."
                } ],
                "reportingRequirements" : [ {
                  "description" : "Processes for the highest governance body to ensure conflicts of interest are avoided and managed."
                }, {
                  "description" : "Whether conflicts of interest are disclosed to stakeholders, including, as a minimum:",
                  "subRequirements" : [ {
                    "description" : "Cross-board membership;"
                  }, {
                    "description" : "Cross-shareholding with suppliers and other stakeholders;"
                  }, {
                    "description" : "Existence of controlling shareholder;"
                  }, {
                    "description" : "Related party disclosures."
                  } ]
                } ],
                "series" : "100",
                "subNumber" : 25,
                "title" : "Conflicts of interest"
              },
              {
                "disclosureNumber" : "102-26",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Highest governance body’s and senior executives’ roles in the development, approval, and updating of the organization’s purpose, value or mission statements, strategies, policies, and goals related to economic, environmental, and social topics."
                } ],
                "series" : "100",
                "subNumber" : 26,
                "title" : "Role of highest governance body in setting purpose, values, and strategy"
              },
              {
                "disclosureNumber" : "102-27",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Measures taken to develop and enhance the highest governance body’s collective knowledge of economic, environmental, and social topics."
                } ],
                "series" : "100",
                "subNumber" : 27,
                "title" : "Collective knowledge of highest governance body"
              },
              {
                "disclosureNumber" : "102-28",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Processes for evaluating the highest governance body’s performance with respect to governance of economic, environmental, and social topics."
                }, {
                  "description" : "Whether such evaluation is independent or not, and its frequency."
                }, {
                  "description" : "Whether such evaluation is a self-assessment."
                }, {
                  "description" : "Actions taken in response to evaluation of the highest governance body’s performance with respect to governance of economic, environmental, and social topics, including, as a minimum, changes in membership and organizational practice."
                } ],
                "series" : "100",
                "subNumber" : 28,
                "title" : "Evaluating the highest governance body’s performance"
              },
              {
                "disclosureNumber" : "102-29",
                "guidance" : "<div class=\"page\" title=\"Page 22\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>See references 11, 14, 15 and 16 in the References section.</p>\n</div>\n</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Highest governance body’s role in identifying and managing economic, environmental, and social topics and their impacts, risks, and opportunities – including its role in the implementation of due diligence processes."
                }, {
                  "description" : "Whether stakeholder consultation is used to support the highest governance body’s identification and management of economic, environmental, and social topics and their impacts, risks, and opportunities."
                } ],
                "series" : "100",
                "subNumber" : 29,
                "title" : "Identifying and managing economic, environmental, and social impacts"
              },
              {
                "disclosureNumber" : "102-30",
                "guidance" : "<div class=\"page\" title=\"Page 22\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>See references 11, 14, 15 and 16 in the References section.</p>\n</div>\n</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Highest governance body’s role in reviewing the effectiveness of the organization’s risk management processes for economic, environmental, and social topics."
                } ],
                "series" : "100",
                "subNumber" : 30,
                "title" : "Effectiveness of risk management processes"
              },
              {
                "disclosureNumber" : "102-31",
                "guidance" : "<div class=\"page\" title=\"Page 23\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>See references 11, 14, 15 and 16 in the References section.</p>\n</div>\n</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Frequency of the highest governance body’s review of economic, environmental, and social topics and their impacts, risks, and opportunities."
                } ],
                "series" : "100",
                "subNumber" : 31,
                "title" : "Review of economic, environmental, and social topics"
              },
              {
                "disclosureNumber" : "102-32",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "The highest committee or position that formally reviews and approves the organization’s sustainability report and ensures that all material topics are covered."
                } ],
                "series" : "100",
                "subNumber" : 32,
                "title" : "Highest governance body’s role in sustainability reporting"
              },
              {
                "disclosureNumber" : "102-33",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Process for communicating critical concerns to the highest governance body."
                } ],
                "series" : "100",
                "subNumber" : 33,
                "title" : "Communicating critical concerns"
              },
              {
                "disclosureNumber" : "102-34",
                "guidance" : "<div class=\"page\" title=\"Page 24\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>When the exact nature of concerns is sensitive due to regulatory or legal restrictions, this disclosure can be limited to the information that the reporting organization is able to provide without jeopardizing confidentiality. For more information on reasons for omission, see GRI 101: Foundation.</p>\n</div>\n</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Total number and nature of critical concerns that were communicated to the highest governance body."
                }, {
                  "description" : "Mechanism(s) used to address and resolve critical concerns."
                } ],
                "series" : "100",
                "subNumber" : 34,
                "title" : "Nature and total number of critical concerns"
              },
              {
                "disclosureNumber" : "102-35",
                "guidance" : "",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-35, the reporting organization should, if performance-related pay is used, describe:",
                  "subRecommendations" : [ {
                    "description" : "how remuneration and incentive-related pay for senior executives are designed to reward longer-term performance;"
                  }, {
                    "description" : "how performance criteria in the remuneration policies relate to the highest governance body’s and senior executives’ objectives for economic, environmental, and social topics for the reporting period and the period ahead."
                  } ]
                }, {
                  "description" : "When compiling the information specified in Disclosure 102-35, the reporting organization should, if termination payments are used, explain whether:",
                  "subRecommendations" : [ {
                    "description" : "notice periods for governance body members and senior executives are different from those for other employees;"
                  }, {
                    "description" : "termination payments for governance body members and senior executives are different from those for other employees;"
                  }, {
                    "description" : "any payments other than those related to the notice period are paid to departing governance body members and senior executives;"
                  }, {
                    "description" : "any mitigation clauses are included in the termination arrangements."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Remuneration policies for the highest governance body and senior executives for the following types of remuneration:",
                  "subRequirements" : [ {
                    "description" : "Fixed pay and variable pay, including performance-based pay, equity-based pay, bonuses, and deferred or vested shares;"
                  }, {
                    "description" : "Sign-on bonuses or recruitment incentive payments;"
                  }, {
                    "description" : "Termination payments;"
                  }, {
                    "description" : "Clawbacks;"
                  }, {
                    "description" : "Retirement benefits, including the difference between benefit schemes and contribution rates for the highest governance body, senior executives, and all other employees."
                  } ]
                }, {
                  "description" : "How performance criteria in the remuneration policies relate to the highest governance body’s and senior executives’ objectives for economic, environmental, and social topics."
                } ],
                "series" : "100",
                "subNumber" : 35,
                "title" : "Remuneration policies"
              },
              {
                "disclosureNumber" : "102-36",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Process for determining remuneration."
                }, {
                  "description" : "Whether remuneration consultants are involved in determining remuneration and whether they are independent of management."
                }, {
                  "description" : "Any other relationships that the remuneration consultants have with the organization."
                } ],
                "series" : "100",
                "subNumber" : 36,
                "title" : "Process for determining remuneration"
              },
              {
                "disclosureNumber" : "102-37",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "How stakeholders’ views are sought and taken into account regarding remuneration."
                }, {
                  "description" : "If applicable, the results of votes on remuneration policies and proposals"
                } ],
                "series" : "100",
                "subNumber" : 37,
                "title" : "Stakeholders’ involvement in remuneration"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-38, the reporting organization shall, for each country of significant operations:",
                  "subRequirements" : [ {
                    "description" : "identify the highest-paid individual for the reporting period, as defined by total compensation;"
                  }, {
                    "description" : "calculate the median annual total compensation for all employees, except the highest- paid individual;"
                  }, {
                    "description" : "calculate the ratio of the annual total compensation of the highest-paid individual to the median annual total compensation for all employees."
                  } ]
                } ],
                "disclosureNumber" : "102-38",
                "guidance" : "",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-38, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "for each country of significant operations, define and report the composition of the annual total compensation for the highest-paid individual and for all employees as follows:",
                    "subRecommendations" : [ {
                      "description" : "List types of compensation included in the calculation;"
                    }, {
                      "description" : "Specify whether full-time and part-time employees are included in this calculation;"
                    }, {
                      "description" : "Specify whether full-time equivalent pay rates are used for each part-time employee in this calculation;"
                    }, {
                      "description" : "Specify which operations or countries are included, if the organization chooses to not consolidate this ratio for the entire organization;"
                    } ]
                  }, {
                    "description" : "depending on the organization’s remuneration policies and availability of data, consider the following components for the calculation:",
                    "subRecommendations" : [ {
                      "description" : "Base salary: guaranteed, short-term, and non-variable cash compensation;"
                    }, {
                      "description" : "Cash compensation: sum of base salary + cash allowances + bonuses + commissions + cash pro t-sharing + other forms of variable cash payments;"
                    }, {
                      "description" : "Direct compensation: sum of total cash compensation + total fair value of all annual long-term incentives, such as stock option awards, restricted stock shares or units, performance stock shares or units, phantom stock shares, stock appreciation rights, and long-term cash awards."
                    } ]
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Ratio of the annual total compensation for the organization’s highest-paid individual in each country of significant operations to the median annual total compensation for all employees (excluding the highest-paid individual) in the same country."
                } ],
                "series" : "100",
                "subNumber" : 38,
                "title" : "Annual total compensation ratio"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-39, the reporting organization shall, for each country of significant operations:",
                  "subRequirements" : [ {
                    "description" : "identify the highest-paid individual for the reporting period, as defined by total compensation;"
                  }, {
                    "description" : "calculate the percentage increase in the highest-paid individual’s compensation from prior period to the reporting period;"
                  }, {
                    "description" : "calculate median annual total compensation for all employees except the highest-paid individual;"
                  }, {
                    "description" : "calculate the percentage increase of the median annual total compensation from the previous reporting period to the current reporting period;"
                  }, {
                    "description" : "calculate the ratio of the annual total compensation percentage increase of the highest-paid individual to the median annual total compensation percentage increase for all employees."
                  } ]
                } ],
                "disclosureNumber" : "102-39",
                "guidance" : "",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-39, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "for each country of significant operations, define and report the composition of the annual total compensation for the highest-paid individual and for all employees as follows:",
                    "subRecommendations" : [ {
                      "description" : "List types of compensation included in the calculation;"
                    }, {
                      "description" : "Specify whether full-time and part-time employees are included in this calculation;"
                    }, {
                      "description" : "Specify whether full-time equivalent pay rates are used for each part-time employee in this calculation;"
                    }, {
                      "description" : "Specify which operations or countries are included, if the organization chooses to not consolidate this ratio for the entire organization;"
                    } ]
                  }, {
                    "description" : "depending on the organization’s remuneration policies and availability of data, consider the following components for the calculation:",
                    "subRecommendations" : [ {
                      "description" : "Base salary: guaranteed, short-term, and non-variable cash compensation;"
                    }, {
                      "description" : "Cash compensation: sum of base salary + cash allowances + bonuses + commissions + cash profit-sharing + other forms of variable cash payments;"
                    }, {
                      "description" : "Direct compensation: sum of total cash compensation + total fair value of all annual long-term incentives, such as stock option awards, restricted stock shares or units, performance stock shares or units, phantom stock shares, stock appreciation rights, and long-term cash awards."
                    } ]
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Ratio of the percentage increase in annual total compensation for the organization’s highest-paid individual in each country of significant operations to the median percentage increase in annual total compensation for all employees (excluding the highest-paid individual) in the same country."
                } ],
                "series" : "100",
                "subNumber" : 39,
                "title" : "Percentage increase in annual total compensation ratio"
              },
              {
                "disclosureNumber" : "102-40",
                "guidance" : "<div class=\"page\" title=\"Page 29\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Examples of stakeholder groups are:</p>\n<ul>\n<li>\n<p>civil society</p>\n</li>\n<li>\n<p>customers</p>\n</li>\n<li>\n<p>employees and workers who are not employees</p>\n</li>\n<li>\n<p>trade unions</p>\n</li>\n<li>\n<p>local communities</p>\n</li>\n<li>\n<p>shareholders and providers of capital</p>\n</li>\n<li>\n<p>suppliers</p>\n</li>\n</ul>\n</div>\n</div>\n<div class=\"layoutArea\">&nbsp;</div>\n<div class=\"layoutArea\">&nbsp;</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "A list of stakeholder groups engaged by the organization."
                } ],
                "series" : "100",
                "subNumber" : 40,
                "title" : "List of stakeholder groups"
              },
              {
                "disclosureNumber" : "102-41",
                "guidance" : "<div class=\"page\" title=\"Page 30\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Guidance for Disclosure 102-41</p>\n<p>This disclosure asks for the percentage of employees covered by collective bargaining agreements. It does not ask for the percentage of employees belonging to trade unions.</p>\n<p>Collective bargaining refers to all negotiations which take place between one or more employers or employers' organizations, on the one hand, and one or more workers' organizations (trade unions), on the other, for determining working conditions and terms of employment or for regulating relations between employers and workers.1 Therefore, a collective bargaining agreement represents a form of joint decision-making concerning the organization&rsquo;s operations.</p>\n<div class=\"page\" title=\"Page 30\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>By definition, collective bargaining agreements are obligations (often legally binding) that the organization has undertaken. The organization is expected to understand the coverage of the agreement (the workers to whom it is obligated to apply the terms of the agreement).</p>\n<p>Collective agreements can be made at various levels and for categories and groups of workers. Collective agreements can be at the level of the organization; at the industry level, in countries where that is the practice; or at both. Collective agreements can cover specific groups of workers; for example, those performing a specific activity or working at a specific location.</p>\n<p>See references 1, 2, 3, 4, 5, 8 and 9 in the References section.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-41, the reporting organization should use data from Disclosure 102-7 as the basis for calculating the percentage."
                } ],
                "reportingRequirements" : [ {
                  "description" : "Percentage of total employees covered by collective bargaining agreements."
                } ],
                "series" : "100",
                "subNumber" : 41,
                "title" : "Collective bargaining agreements"
              },
              {
                "disclosureNumber" : "102-42",
                "guidance" : "",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-42, the reporting organization should describe the process for:",
                  "subRecommendations" : [ {
                    "description" : "defining its stakeholder groups;"
                  }, {
                    "description" : "determining the groups with which to engage and not to engage."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "The basis for identifying and selecting stakeholders with whom to engage."
                } ],
                "series" : "100",
                "subNumber" : 42,
                "title" : "Identifying and selecting stakeholders"
              },
              {
                "disclosureNumber" : "102-43",
                "guidance" : "<div class=\"page\" title=\"Page 31\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Methods of stakeholder engagement can include surveys (such as <span style=\"text-decoration: underline;\">supplier</span>, customer, or <span style=\"text-decoration: underline;\">worker</span> surveys), focus groups, community panels, corporate advisory panels, written communication, management or union structures, <span style=\"text-decoration: underline;\">collective bargaining</span> agreements, and other mechanisms.</p>\n</div>\n<div class=\"column\">\n<p>For many organizations, customers are a relevant stakeholder group. As well as measuring an organization&rsquo;s sensitivity to its customers&rsquo; needs and preferences, customer satisfaction or dissatisfaction can give insight into the degree to which the organization considers the needs of stakeholders.</p>\n</div>\n</div>\n</div>",
                "number" : 2,
                "series" : "100",
                "subNumber" : 43,
                "title" : "Approach to stakeholder engagement"
              },
              {
                "disclosureNumber" : "102-44",
                "guidance" : "<div class=\"page\" title=\"Page 32\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>As part of the key topics and concerns raised by stakeholders, this disclosure can include the results or key conclusions of customer surveys (based on statistically relevant sample sizes) conducted in the <span style=\"text-decoration: underline;\">reporting period</span>.</p>\n<div class=\"page\" title=\"Page 32\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>These surveys can indicate customer satisfaction and dissatisfaction relating to:</p>\n<ul>\n<li>\n<p>the organization as a whole</p>\n</li>\n<li>\n<p>a major <span style=\"text-decoration: underline;\">product or service category</span></p>\n</li>\n<li>\n<p>significant locations of operation</p>\n</li>\n</ul>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Key topics and concerns that have been raised through stakeholder engagement, including:",
                  "subRequirements" : [ {
                    "description" : "how the organization has responded to those key topics and concerns, including through its reporting;"
                  }, {
                    "description" : "the stakeholder groups that raised each of the key topics and concerns."
                  } ]
                } ],
                "series" : "100",
                "subNumber" : 44,
                "title" : "Key topics and concerns raised"
              },
              {
                "disclosureNumber" : "102-45",
                "guidance" : "<div class=\"page\" title=\"Page 33\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>An organization can report Disclosure 102-45 by referencing the information in publicly available consolidated financial statements or equivalent documents.</p>\n</div>\n</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "A list of all entities included in the organization’s consolidated financial statements or equivalent documents."
                }, {
                  "description" : "Whether any entity included in the organization’s consolidated financial statements or equivalent documents is not covered by the report."
                } ],
                "series" : "100",
                "subNumber" : 45,
                "title" : "Entities included in the consolidated financial statements"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-46, the reporting organization shall include an explanation of how the Materiality principle was applied to identify material topics, including any assumptions made."
                } ],
                "disclosureNumber" : "102-46",
                "guidance" : "<div class=\"page\" title=\"Page 34\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>The four Reporting Principles for defining report content are: Stakeholder Inclusiveness, Sustainability Context, Materiality, and Completeness. Together, these Principles help an organization decide which content to include in the report by considering the organization&rsquo;s activities, <span style=\"text-decoration: underline;\">impacts</span>, and the substantive expectations and interests of its <span style=\"text-decoration: underline;\">stakeholders</span>.</p>\n<p>This disclosure asks for an explanation of how the organization has defined its report content and topic Boundaries, and how these four Principles have been implemented. This explanation also requires a specific description of how the Materiality principle has been applied, including how material topics were identified based on the two dimensions of the principle.</p>\n</div>\n<div class=\"column\">\n<p>This explanation can also include:</p>\n<ul>\n<li>\n<p>the steps taken to identify relevant topics (i.e., those that potentially merit inclusion in the report);</p>\n</li>\n<li>\n<p>how the relative priority of material topics was determined.</p>\n</li>\n</ul>\n<p>For more information on the Reporting Principles for defining report content, see GRI 101: Foundation.</p>\n<p>The description of the topic Boundary for each material topic is reported under Disclosure 103-1 in GRI 103: Management Approach.</p>\n</div>\n</div>\n</div>",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-46, the reporting organization should include an explanation of:",
                  "subRecommendations" : [ {
                    "description" : "the steps taken to define the content of the report and to define the topic Boundaries;"
                  }, {
                    "description" : "at which steps in the process each of the Reporting Principles for defining report content was applied;"
                  }, {
                    "description" : "assumptions and subjective judgements made in the process;"
                  }, {
                    "description" : "challenges the organization encountered when applying the Reporting Principles for defining report content."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "An explanation of the process for defining the report content and the topic Boundaries."
                }, {
                  "description" : "An explanation of how the organization has implemented the Reporting Principles for defining report content."
                } ],
                "series" : "100",
                "subNumber" : 46,
                "title" : "Defining report content and topic Boundaries"
              },
              {
                "disclosureNumber" : "102-47",
                "guidance" : "<div class=\"page\" title=\"Page 35\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Material topics are those that an organization has prioritized for inclusion in the report. This prioritization exercise is carried out using the Stakeholder Inclusiveness and the Materiality principles. The Materiality principle identifies material topics based on the following two dimensions:</p>\n<ul>\n<li>\n<p>The significance of the organization&rsquo;s economic, environmental, and social <span style=\"text-decoration: underline;\">impacts</span>;</p>\n</li>\n<li>\n<p>Their substantive influence on the assessments and decisions of <span style=\"text-decoration: underline;\">stakeholders</span>.</p>\n</li>\n</ul>\n<div class=\"page\" title=\"Page 35\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>For more information on the Stakeholder Inclusiveness and Materiality principles, see GRI 101: Foundation.</p>\n<p>The explanation of why each topic is material is reported under Disclosure 103-1 in GRI 103: Management Approach.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "A list of the material topics identified in the process for defining report content."
                } ],
                "series" : "100",
                "subNumber" : 47,
                "title" : "List of material topics"
              },
              {
                "disclosureNumber" : "102-48",
                "guidance" : "<div class=\"page\" title=\"Page 35\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Restatements can result from:</p>\n<ul>\n<li>\n<p>mergers or acquisitions</p>\n</li>\n<li>\n<p>change of <span style=\"text-decoration: underline;\">base years</span> or periods</p>\n</li>\n<li>\n<p>nature of business</p>\n</li>\n<li>\n<p>measurement methods</p>\n</li>\n</ul>\n</div>\n</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "The effect of any restatements of information given in previous reports, and the reasons for such restatements."
                } ],
                "series" : "100",
                "subNumber" : 48,
                "title" : "Restatements of information"
              },
              {
                "disclosureNumber" : "102-49",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Significant changes from previous reporting periods in the list of material topics and topic Boundaries."
                } ],
                "series" : "100",
                "subNumber" : 49,
                "title" : "Changes in reporting"
              },
              {
                "disclosureNumber" : "102-50",
                "guidance" : "<div class=\"page\" title=\"Page 36\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>The reporting period can be, for example, the fiscal or calendar year.</p>\n</div>\n</div>\n</div>",
                "number" : 2,
                "series" : "100",
                "subNumber" : 50,
                "title" : "Reporting period"
              },
              {
                "disclosureNumber" : "102-51",
                "guidance" : "<div class=\"page\" title=\"Page 36\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<div class=\"page\" title=\"Page 36\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>If this is the first report prepared by the reporting organization, the response to this disclosure can state this.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "If applicable, the date of the most recent previous report."
                } ],
                "series" : "100",
                "subNumber" : 51,
                "title" : "Date of most recent report"
              },
              {
                "disclosureNumber" : "102-52",
                "guidance" : "<div class=\"page\" title=\"Page 37\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>The reporting cycle can be, for example, annual or biennial.</p>\n</div>\n</div>\n</div>",
                "number" : 2,
                "series" : "100",
                "subNumber" : 52,
                "title" : "Reporting cycle"
              },
              {
                "disclosureNumber" : "102-53",
                "guidance" : "",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "The contact point for questions regarding the report or its contents."
                } ],
                "series" : "100",
                "subNumber" : 53,
                "title" : "Contact point for questions regarding the report"
              },
              {
                "disclosureNumber" : "102-54",
                "guidance" : "<div class=\"page\" title=\"Page 37\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>An organization preparing a report in accordance with the GRI Standards can choose one of two options (Core or Comprehensive), depending on the degree to which the GRI Standards have been applied. For each option, there is a corresponding claim, or statement of use, that the organization is required to include in the report. These claims have set wording.</p>\n</div>\n<div class=\"column\">\n<p>For more information on making claims related to the use of the GRI Standards, see Section 3 in GRI 101: Foundation.</p>\n</div>\n</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "The claim made by the organization, if it has prepared a report in accordance with the GRI Standards, either:",
                  "subRequirements" : [ {
                    "description" : "‘This report has been prepared in accordance with the GRI Standards: Core option’;"
                  }, {
                    "description" : "‘This report has been prepared in accordance with the GRI Standards: Comprehensive option’."
                  } ]
                } ],
                "series" : "100",
                "subNumber" : 54,
                "title" : "Claims of reporting in accordance with the GRI Standards"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When reporting the GRI content index as specified in Disclosure 102-55, the reporting organization shall:",
                  "subRequirements" : [ {
                    "description" : "include the words ‘GRI Content Index’ in the title;"
                  }, {
                    "description" : "present the complete GRI content index in one location;"
                  }, {
                    "description" : "include in the report a link or reference to the GRI content index, if it is not provided in the report itself;"
                  }, {
                    "description" : "for each GRI Standard used, include the title and publication year (e.g., GRI 102: General Disclosures 2016);"
                  }, {
                    "description" : "include any additional material topics reported on which are not covered by the GRI Standards, including page number(s) or URL(s) where the information can be found."
                  } ]
                } ],
                "disclosureNumber" : "102-55",
                "guidance" : "<div class=\"page\" title=\"Page 38\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>The content index required by this disclosure is a navigation tool that specifies which GRI Standards have been used, which disclosures have been made, and where these disclosures can be found in the report or other location. It enables stakeholders to gain a quick overview of the report and facilitates easy navigation across reports. Any organization making a claim that its report has been prepared in accordance with the GRI Standards is required to include a GRI content index in its report or provide a link to where the content index can be found. See Table 1 in GRI 101: Foundation for more information.</p>\n<p>The disclosure number refers to the unique numeric identifier for each disclosure in the GRI Standards (e.g., 102-53).</p>\n</div>\n<div class=\"column\">\n<p>The page numbers (when the report is PDF-based) or URLs (when the report is web-based) referenced in the content index are expected to be specific enough to direct stakeholders to the information for the disclosures made. If a disclosure is spread over multiple pages or URLs, the content index references all pages and URLs where the information can be found. See &lsquo;Reporting required disclosures using references&rsquo; in GRI 101: Foundation for more information.</p>\n<p>References to webpages and documents other than the report, such as the annual financial report or a policy document, can be included in the content index as long as they have a specific page number or a direct URL to the webpage.</p>\n<p>Material topics that are not covered by the GRI Standards but are included in the report are also required to be in the content index. See 'Reporting on material topics' in GRI 101: Foundation for more information on how to report on these topics, and Table 1 of this Standard for an example of how to include these topics in the context index.</p>\n<p>While in principle it is up to the reporting organization to add direct answers to the content index, too much text can diminish the clarity and navigation of the index.</p>\n<div class=\"page\" title=\"Page 39\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Additional content can also be included in the content index, for example to show the connection with other reporting standards or frameworks. Such additions can be made to add clarity for stakeholders, as long as they do not compromise the readability of the content index.</p>\n<div class=\"page\" title=\"Page 39\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>See clause 3.2 in GRI 101: Foundation, which specifies the required information to provide when giving reasons for omission.</p>\n<div class=\"page\" title=\"Page 39\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>The organization can use Table 1 of this Standard as one possible format to prepare the GRI content index.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 102-55, the reporting organization should include in the GRI content index the title of each disclosure made (e.g., Name of the organization), in addition to the number (e.g., 102-1)."
                } ],
                "reportingRequirements" : [ {
                  "description" : "The GRI content index, which specifies each of the GRI Standards used and lists all disclosures included in the report."
                }, {
                  "description" : "For each disclosure, the content index shall include:",
                  "subRequirements" : [ {
                    "description" : "the number of the disclosure (for disclosures covered by the GRI Standards);"
                  }, {
                    "description" : "the page number(s) or URL(s) where the information can be found, either within the report or in other published materials;"
                  }, {
                    "description" : "if applicable, and where permitted, the reason(s) for omission when a required disclosure cannot be made."
                  } ]
                } ],
                "series" : "100",
                "subNumber" : 55,
                "title" : "GRI content index"
              },
              {
                "disclosureNumber" : "102-56",
                "guidance" : "<div class=\"page\" title=\"Page 41\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>An organization can use a variety of approaches to enhance the credibility of its report.</p>\n<p>The use of external assurance for sustainability reports is advised in addition to any internal resources, but it is not required in order to make a claim that a report has been prepared in accordance with the GRI Standards.</p>\n<p>The GRI Standards use the term &lsquo;external assurance&rsquo; to refer to activities designed to result in published conclusions on the quality of the report and the information (whether it be qualitative or quantitative) contained within it. External assurance can also refer to activities designed to result in published conclusions about systems or processes (such as the process for defining report content, including the application of the Materiality principle or the stakeholder engagement process). This is different from activities designed to assess or validate the quality or level of performance of an organization, such as issuing performance certifications or compliance assessments.</p>\n<p>In addition to external assurance, an organization can have systems of internal controls in place. These internal systems are also important to the overall integrity and credibility of a report.</p>\n<p>In some jurisdictions, corporate governance codes can require directors to inquire, and then, if satisfied, to confirm in the annual report the adequacy of an organization&rsquo;s internal controls. Generally, management is responsible for designing and implementing these internal controls. The confirmation in the annual report might relate only to such internal controls that are necessary for financial reporting purposes, and do not necessarily extend to those controls that would be required to address the reliability of information in the sustainability report.</p>\n</div>\n<div class=\"column\">\n<p>An organization can also establish and maintain an internal audit function as part of its processes for risk management and for managing and reporting information.</p>\n<p>An organization can also convene a <span style=\"text-decoration: underline;\">stakeholder</span> panel to review its overall approach to sustainability reporting or to provide advice on the content of its sustainability report.</p>\n<p><strong>Guidance for Disclosure 102-56</strong></p>\n<p>An organization can use a variety of approaches to seek external assurance, such as the use of professional assurance providers, or other external groups or persons. Regardless of the specific approach, it is expected that external assurance is conducted by competent groups or persons who follow professional standards for assurance, or who apply systematic, documented, and evidence-based processes (&lsquo;assurance providers&rsquo;).</p>\n<p>Overall, for external assurance of reports that have used the GRI Standards, it is expected that the assurance providers:</p>\n<ul>\n<li>\n<p>are independent from the organization and therefore able to reach and publish an objective and impartial opinion or conclusions about the report;</p>\n</li>\n<li>\n<p>are demonstrably competent in both the subject matter and assurance practices;</p>\n</li>\n<li>\n<p>apply quality control procedures to the assurance engagement;</p>\n</li>\n<li>\n<p>conduct the engagement in a manner that is systematic, documented, evidence-based, and characterized by defined procedures;</p>\n</li>\n<li>\n<p>assess whether the report provides a reasonable and balanced presentation of performance &ndash; considering the veracity of data in the report<br /> as well as the overall selection of content;</p>\n</li>\n<li>\n<p>assess the extent to which the report preparer has applied the GRI Standards in the course of reaching its conclusions;</p>\n</li>\n<li>\n<p>issue a written report that is publicly available and includes: an opinion or set of conclusions;<br /> a description of the responsibilities of the report preparer and the assurance provider; and a summary of the work performed, which explains the nature of the assurance conveyed by the assurance report.</p>\n</li>\n</ul>\nThe language used in external assurance reports, statements, or opinions can be technical and is not always accessible. Thus, it is expected that information for this disclosure is included in broadly-accessible language.</div>\n</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "A description of the organization’s policy and current practice with regard to seeking external assurance for the report."
                }, {
                  "description" : "If the report has been externally assured:",
                  "subRequirements" : [ {
                    "description" : "A reference to the external assurance report, statements, or opinions. If not included in the assurance report accompanying the sustainability report, a description of what has and what has not been assured and on what basis, including the assurance standards used, the level of assurance obtained, and any limitations of the assurance process;"
                  }, {
                    "description" : "The relationship between the organization and the assurance provider;"
                  }, {
                    "description" : "Whether and how the highest governance body or senior executives are involved in seeking external assurance for the organization’s sustainability report."
                  } ]
                } ],
                "series" : "100",
                "subNumber" : 56,
                "title" : "External assurance"
              }
        ];
    },

    createEconomicDisclosure(req, res) {
          return [
            {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 201-1, the reporting organization shall, if applicable, compile the EVG&D from data in the organization’s audited financial or profit and loss (P&L) statement, or its internally audited management accounts."
                } ],
                "disclosureNumber" : "201-1",
                "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Information on the creation and distribution of economic value provides a basic indication of how an organization has created wealth for stakeholders. Several components of the economic value generated and distributed (EVG&amp;D) also provide an economic profile of an organization, which can be useful for normalizing other performance figures.</p>\n<p>If presented in country-level detail, EVG&amp;D can provide a useful picture of the direct monetary value added to local economies.</p>\n<p><em>Revenues</em></p>\n<p>An organization can calculate revenues as net sales plus revenues from financial investments and sales of assets.</p>\n<p>Net sales can be calculated as gross sales from products and services minus returns, discounts, and allowances.</p>\n<p>Revenues from financial investments can include cash received as:</p>\n<ul>\n<li>interest on financial loans;</li>\n<li>dividends from shareholdings;</li>\n<li>royalties;</li>\n<li>direct income generated from assets, such as property rental.</li>\n</ul>\n<p>Revenues from sale of assets can include:</p>\n<ul>\n<li>physical assets, such as property, infrastructure, and equipment;</li>\n<li>intangibles, such as intellectual property rights, designs, and brand names.</li>\n</ul>\n<p><em>Operating costs</em></p>\n<p>An organization can calculate operating costs as cash payments made outside the organization for materials, product components, facilities, and services purchased.</p>\n<p>Services purchased can include payments to self-employed persons, temporary placement agencies and other organizations providing services. Costs related to workers who are not employees working in an operational role are included as part of services purchased, rather than under employee wages<br /> and benefits.</p>\n<p>Operating costs can include:</p>\n<ul>\n<li class=\"column\">\n<p>property rental;</p>\n</li>\n<li class=\"column\">\n<p>license fees;</p>\n</li>\n<li class=\"column\">\n<p>facilitation payments (since these have a clear commercial objective);</p>\n</li>\n<li class=\"column\">\n<p>royalties;</p>\n</li>\n<li class=\"column\">\n<p>payments for contract workers;</p>\n</li>\n<li class=\"column\">\n<p>training costs, if outside trainers are used;</p>\n</li>\n<li class=\"column\">\n<p>personal protective clothing.</p>\n</li>\n</ul>\n<p>The use of facilitation payments is also addressed in GRI 205: Anti-corruption.</p>\n<p><em>Employee wages and benefits</em></p>\n<p>An organization can calculate employee wages and benefits as total payroll (including employee salaries and amounts paid to government institutions on behalf of employees) plus total benefits (excluding training, costs of protective equipment or other cost items directly related to the employee&rsquo;s job function).</p>\n<p>Amounts paid to government institutions on behalf of employees can include employee taxes, levies, and unemployment funds.</p>\n<p>Total benefits can include:</p>\n<div class=\"page\" title=\"Page 7\">\n<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<ul>\n<li>\n<p>regular contributions, such as to pensions, insurance, company vehicles, and private health;</p>\n</li>\n<li>\n<p>other employee support, such as housing, interest-free loans, public transport assistance, educational grants, and redundancy payments.</p>\n</li>\n</ul>\n<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><em>Payments to providers of capital</em></p>\n<p>An organization can calculate payments to providers of capital as dividends to all shareholders, plus interest payments made to providers of loans.</p>\n<p>Interest payments made to providers of loans can include:</p>\n<ul>\n<li>\n<p>interest on all forms of debt and borrowings (not only long-term debt);</p>\n</li>\n<li>\n<p>arrears of dividends due to preferred shareholders.</p>\n</li>\n</ul>\n<p><em>Payments to government</em></p>\n<p>An organization can calculate payments to governments as all of the organization&rsquo;s taxes plus related penalties paid at the international, national, and local levels. Organization taxes can include corporate, income, and property.</p>\n<p>Payments to government exclude deferred taxes, because they may not be paid.</p>\n<p>If operating in more than one country, the organization can report taxes paid by country, including the definition of segmentation used.</p>\n<p><em>Community investments</em></p>\n<p>Total community investments refers to actual expenditures in the reporting period, not commitments. An organization can calculate community investments as voluntary donations plus investment of funds in the broader community where the target beneficiaries are external to the organization. Voluntary donations and investment of funds in the broader community where the target beneficiaries are external to the organization can include:</p>\n<ul>\n<li>contributions to charities, NGOs and research institutes (unrelated to the organization&rsquo;s commercial research and development);</li>\n<li>funds to support community infrastructure, such as recreational facilities;</li>\n<li>direct costs of social programs, including arts and educational events.</li>\n</ul>\n<p>If reporting infrastructure investments, an organization can include costs of goods and labor,<br /> in addition to capital costs, as well as operating costs for support of ongoing facilities or programs. An example of support for ongoing facilities or programs can include the organization funding the daily operations of a public facility.</p>\n<p>Community investments exclude legal and commercial activities or where the purpose of the investment is exclusively commercial (donations to political parties can be included, but are also addressed separately in more detail in GRI 415: Public Policy).</p>\n<div class=\"page\" title=\"Page 7\">\n<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Community investments also exclude any infrastructure investment that is driven primarily by core business needs, or to facilitate the business operations of an organization. Infrastructure investments driven primarily by core business needs can include, for example, building a road to a mine or a factory. The calculation of investment can include infrastructure built outside the main business activities of the organization, such as a school or hospital for workers and their families.</p>\n<p>See references 5, 6, 7 and 9 in the References section.</p>\n</div>\n</div>\n</div>\n&nbsp;</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 1,
                "reportingRequirements" : [ {
                  "description" : "Direct economic value generated and distributed (EVG&D) on an accruals basis, including the basic components for the organization’s global operations as listed below. If data are presented on a cash basis, report the justification for this decision in addition to reporting the following basic components:",
                  "subRequirements" : [ {
                    "description" : "Direct economic value generated: revenues;"
                  }, {
                    "description" : "Economic value distributed: operating costs, employee wages and benefits, payments to providers of capital, payments to government by country, and community investments;"
                  }, {
                    "description" : "Economic value retained: ‘direct economic value generated’ less ‘economic value distributed’."
                  } ]
                }, {
                  "description" : "Where significant, report EVG&D separately at country, regional, or market levels, and the criteria used for defining significance."
                } ],
                "series" : "200",
                "subNumber" : 1,
                "title" : "Direct economic value generated and distributed"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 201-2, if the reporting organization does not have a system in place to calculate the financial implications or costs, or to make revenue projections, it shall report its plans and timeline to develop the necessary systems."
                } ],
                "disclosureNumber" : "201-2",
                "guidance" : "<p>&nbsp;</p>\n<div class=\"page\" title=\"Page 9\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 201-2</strong></p>\n<p>Risk and opportunities due to climate change can be classified as:</p>\n<ul>\n<li>\n<p>physical</p>\n</li>\n<li>\n<p>regulatory</p>\n</li>\n<li>\n<p>other</p>\n</li>\n</ul>\n</div>\n<div class=\"column\">\n<p>Physical risks and opportunities can include:</p>\n<p>&bull; the impact of more frequent and intense storms;</p>\n<p>&bull; changes in sea level, ambient temperature, and water availability;</p>\n<p>&bull; impacts on workers &ndash; such as health effects, including heat-related illness or disease, and the need to relocate operations.</p>\n<div class=\"page\" title=\"Page 10\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Other risks and opportunities can include the availability of new technologies, products, or services to address challenges related to climate change, as well as changes in customer behavior.</p>\n<p>Methods used to manage the risk or opportunity can include:</p>\n<ul>\n<li>\n<p>carbon capture and storage;</p>\n</li>\n<li>\n<p>fuel switching;</p>\n</li>\n<li>\n<p>use of renewable and lower carbon footprint energy;</p>\n</li>\n<li>\n<p>improving energy efficiency;</p>\n</li>\n<li>\n<p>flaring, venting, and fugitive emission reduction;</p>\n</li>\n<li>\n<p>renewable energy certificates;</p>\n</li>\n<li>\n<p>use of carbon offsets.</p>\n</li>\n</ul>\n<div class=\"page\" title=\"Page 10\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Climate change presents risks and opportunities to organizations, their investors, and their stakeholders.</p>\n<p>As governments move to regulate activities that contribute to climate change, organizations that are directly or indirectly responsible for emissions face regulatory risks and opportunities. Risks can include increased costs or other factors impacting competitiveness. However, limits on greenhouse gas (GHG) emissions can also create opportunities for organizations as new technologies and markets are created. This is especially the case for organizations that can use or produce energy and energy-efficient products more effectively.</p>\n<p>See references 2, 3 and 4 in the References section.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 1,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 201-2, the reporting organization should report the following additional characteristics for the identified risks and opportunities:",
                  "subRecommendations" : [ {
                    "description" : "A description of the risk or opportunity driver, such as a particular piece of legislation, or a physical driver, such as water scarcity;"
                  }, {
                    "description" : "The projected time frame in which the risk or opportunity is expected to have substantive financial implications;"
                  }, {
                    "description" : "Direct and indirect impacts (whether the impact directly affects the organization, or indirectly affects the organization via its value chain);"
                  }, {
                    "description" : "The potential impacts generally, including increased or decreased:",
                    "subRecommendations" : [ {
                      "description" : "capital and operational costs;"
                    }, {
                      "description" : "demand for products and services;"
                    }, {
                      "description" : "capital availability and investment opportunities;"
                    } ]
                  }, {
                    "description" : "Likelihood (the probability of the impact on the organization);"
                  }, {
                    "description" : "Magnitude of impact (if occurring, the extent to which the impact affects the organization financially)."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Risks and opportunities posed by climate change that have the potential to generate substantive changes in operations, revenue, or expenditure, including:",
                  "subRequirements" : [ {
                    "description" : "a description of the risk or opportunity and its classification as either physical, regulatory, or other;"
                  }, {
                    "description" : "a description of the impact associated with the risk or opportunity;"
                  }, {
                    "description" : "the financial implications of the risk or opportunity before action is taken;"
                  }, {
                    "description" : "the methods used to manage the risk or opportunity;"
                  }, {
                    "description" : "the costs of actions taken to manage the risk or opportunity."
                  } ]
                } ],
                "series" : "200",
                "subNumber" : 2,
                "title" : "Financial implications and other risks and opportunities due to climate change"
              },
              {
                "disclosureNumber" : "201-3",
                "guidance" : "<div class=\"page\" title=\"Page 11\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 201-3</strong></p>\n<p>The structure of retirement plans offered to employees can be based on:</p>\n<ul>\n<li>\n<p><span style=\"text-decoration: underline;\">defined benefit plans;</span></p>\n</li>\n<li>\n<p><span style=\"text-decoration: underline;\">defined contribution plans;</span></p>\n</li>\n<li>\n<p>other types of retirement benefits.</p>\n</li>\n</ul>\n<p>Different jurisdictions, such as countries, have varying interpretations and guidance regarding calculations used to determine plan coverage.</p>\n<p>Note that benefit pension plans are part of the International Accounting Standards Board (IASB) IAS 19 Employee Benefits, however IAS 19 covers additional topics.</p>\n<p>See reference 7 in the References section.</p>\n<div class=\"page\" title=\"Page 11\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>When an organization provides a retirement plan for its employees, these benefits can become a commitment that members of the schemes plan on for their long- term economic well-being.</p>\n<p>Defined benefit plans have potential implications for employers in terms of the obligations that need to be met. Other types of plans, such as defined contribution plans, do not guarantee access to a retirement plan or the quality of the benefits. Thus, the type of plan chosen has implications for both employees and employers. Conversely, a properly funded pension plan can help to attract and maintain employees and support long-term financial and strategic planning on the part of the employer.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 1,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 201-3, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "calculate the information in accordance with the regulations and methods for relevant jurisdictions, and report aggregated totals;"
                  }, {
                    "description" : "use the same consolidation techniques as those applied in preparing the financial accounts of the organization."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "If the plan’s liabilities are met by the organization’s general resources, the estimated value of those liabilities."
                }, {
                  "description" : "If a separate fund exists to pay the plan’s pension liabilities:",
                  "subRequirements" : [ {
                    "description" : "the extent to which the scheme’s liabilities are estimated to be covered by the assets that have been set aside to meet them;"
                  }, {
                    "description" : "the basis on which that estimate has been arrived at;"
                  }, {
                    "description" : "when that estimate was made."
                  } ]
                }, {
                  "description" : "If a fund set up to pay the plan’s pension liabilities is not fully covered, explain the strategy, if any, adopted by the employer to work towards full coverage, and the timescale, if any, by which the employer hopes to achieve full coverage."
                }, {
                  "description" : "Percentage of salary contributed by employee or employer."
                }, {
                  "description" : "Level of participation in retirement plans, such as participation in mandatory or voluntary schemes, regional, or country-based schemes, or those with financial impact."
                } ],
                "series" : "200",
                "subNumber" : 3,
                "title" : "Defined benefit plan obligations and other retirement plans"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 201-4, the reporting organization shall identify the monetary value of financial assistance received from government through consistent application of generally accepted accounting principles.\n"
                } ],
                "disclosureNumber" : "201-4",
                "guidance" : "<div class=\"page\" title=\"Page 12\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>This disclosure provides a measure of governments&rsquo; contributions to an organization.</p>\n<p>The significant financial assistance received from a government, in comparison with taxes paid, can be useful for developing a balanced picture of the transactions between the organization and government.</p>\n<p>See reference 8 in the References section.</p>\n</div>\n</div>\n</div>",
                "number" : 1,
                "reportingRequirements" : [ {
                  "description" : "Total monetary value of financial assistance received by the organization from any government during the reporting period, including:",
                  "subRequirements" : [ {
                    "description" : "tax relief and tax credits;"
                  }, {
                    "description" : "subsidies;"
                  }, {
                    "description" : "investment grants, research and development grants, and other relevant types of grant;"
                  }, {
                    "description" : "awards;"
                  }, {
                    "description" : "royalty holidays;"
                  }, {
                    "description" : "financial assistance from Export Credit Agencies (ECAs);"
                  }, {
                    "description" : "financial incentives;"
                  }, {
                    "description" : "other financial benefits received or receivable from any government for any operation."
                  } ]
                }, {
                  "description" : "The information in 201-4-a by country."
                }, {
                  "description" : "Whether, and the extent to which, any government is present in the shareholding structure."
                } ],
                "series" : "200",
                "subNumber" : 4,
                "title" : "Financial assistance received from government"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 202-2, the reporting organization shall calculate this percentage using data on full-time employees."
                } ],
                "disclosureNumber" : "202-2",
                "guidance" : "<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 202-2</strong></p>\n<p>Senior management hired from the local community includes those individuals either born or who have the legal right to reside indefinitely (such as naturalized citizens or permanent visa holders) in the same geographic market as the operation. The geographical definition of &lsquo;local&rsquo; can include the community surrounding operations, a region within a country, or a country.</p>\n<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Including members from the local community in an organization&rsquo;s senior management demonstrates the organization&rsquo;s positive market presence. Including local community members in the management team can enhance human capital. It can also increase the economic benefit to the local community, and improve an organization&rsquo;s ability to understand local needs.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 2,
                "reportingRequirements" : [ {
                  "description" : "Percentage of senior management at significant locations of operation that are hired from the local community."
                }, {
                  "description" : "The definition used for ‘senior management’."
                }, {
                  "description" : "The organization’s geographical definition of ‘local’."
                }, {
                  "description" : "The definition used for ‘significant locations of operation’."
                } ],
                "series" : "200",
                "subNumber" : 2,
                "title" : "Proportion of senior management hired from the local community"
              },
              {
                "disclosureNumber" : "202-1",
                "guidance" : "<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>This disclosure applies to those organizations in which a substantial portion of their employees, and workers (excluding employees) performing the organization&rsquo;s activities, are compensated in a manner or scale that is closely linked to laws or regulations on minimum wage.</p>\n<p>Providing wages above the minimum wage can help contribute to the economic well-being of workers performing the organization&rsquo;s activities. The impacts of wage levels are immediate, and they directly affect individuals, organizations, countries and economies. The distribution of wages is crucial for eliminating inequalities, such as wage gap differences between women and men, or nationals and migrants.</p>\n<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Also, entry level wages paid compared to local minimum wages show the competitiveness of an organization&rsquo;s wages and provide information relevant for assessing the effect of wages on the local labor market. Comparing this information by gender can also be a measure of an organization&rsquo;s approach to equal opportunity in the workplace.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 202-1-b, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "use the description of the organization’s activities from Disclosure 102-2 in GRI 102: General Disclosures;"
                  }, {
                    "description" : "if applicable, convert the entry level wage to the same units used in the minimum wage (e.g., hourly or monthly basis);"
                  }, {
                    "description" : "when a significant proportion of other workers (excluding employees) performing the organization’s activities are compensated based on wages subject to minimum wage rules, report the relevant ratio of the entry level wage by gender at significant locations of operation to the minimum wage."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "When a significant proportion of employees are compensated based on wages subject to minimum wage rules, report the relevant ratio of the entry level wage by gender at significant locations of operation to the minimum wage."
                }, {
                  "description" : "When a significant proportion of other workers (excluding employees) performing the organization’s activities are compensated based on wages subject to minimum wage rules, describe the actions taken to determine whether these workers are paid above the minimum wage."
                }, {
                  "description" : "Whether a local minimum wage is absent or variable at significant locations of operation, by gender. In circumstances in which different minimums can be used as a reference, report which minimum wage is being used."
                }, {
                  "description" : "The definition used for ‘significant locations of operation’."
                } ],
                "series" : "200",
                "subNumber" : 1,
                "title" : "Ratios of standard entry level wage by gender compared to local minimum wage"
              },
              {
                "disclosureNumber" : "203-1",
                "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>This disclosure concerns the impact that an organization&rsquo;s infrastructure investments and services supported have on its stakeholders and the economy.</p>\n<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>The impacts of infrastructure investment can extend beyond the scope of an organization&rsquo;s own operations and over a longer timescale. Such investments can include transport links, utilities, community social facilities, health and welfare centers, and sports centers. Along with investment in its own operations, this is one measure of the organization&rsquo;s capital contribution to the economy.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 3,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 203-1, the reporting organization should disclose:",
                  "subRecommendations" : [ {
                    "description" : "the size, cost and duration of each significant infrastructure investment or service supported;"
                  }, {
                    "description" : "the extent to which different communities or local economies are impacted by the organization’s infrastructure investments and services supported."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Extent of development of significant infrastructure investments and services supported."
                }, {
                  "description" : "Current or expected impacts on communities and local economies, including positive and negative impacts where relevant."
                }, {
                  "description" : "Whether these investments and services are commercial, in-kind, or pro bono engagements."
                } ],
                "series" : "200",
                "subNumber" : 1,
                "title" : "Infrastructure investments and services supported"
              },
              {
                "disclosureNumber" : "203-2",
                "guidance" : "<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Guidance for Disclosure 203-2</p>\n<p>This disclosure concerns the spectrum of indirect economic impacts that an organization can have on its stakeholders and the economy.</p>\n<p>Examples of significant indirect economic impacts, both positive and negative, can include:</p>\n<ul>\n<li>\n<p>changes in the productivity of organizations, sectors, or the whole economy (such as through greater adoption of information technology);</p>\n</li>\n<li>\n<p>economic development in areas of high poverty (such as changes in the total number of dependents supported through the income of a single job);</p>\n</li>\n<li>\n<p>economic impacts of improving or deteriorating social or environmental conditions (such as changing job market in an area converted from small farms to large plantations, or the economic impacts<br /> of pollution);</p>\n</li>\n<li>\n<p>availability of products and services for those<br /> on low incomes (such as preferential pricing of pharmaceuticals, which contributes to a healthier population that can participate more fully in the economy; or pricing structures that exceed the economic capacity of those on low incomes);</p>\n</li>\n<li>\n<p>enhanced skills and knowledge in a professional community or in a geographic location (such as when shifts in an organization&rsquo;s needs attract additional skilled workers to an area, who, in turn, drive a local need for new learning institutions);</p>\n</li>\n<li>\n<p>number of jobs supported in the supply or distribution chain (such as the employment impacts on suppliers as a result of an organization&rsquo;s growth or contraction);</p>\n</li>\n<li>\n<p>stimulating, enabling, or limiting foreign direct investment (such as when an organization changes the infrastructure or services it provides in a developing country, which then leads to changes in foreign direct investment in the region);</p>\n</li>\n<li>\n<p>economic impacts from a change in operation or activity location (such as the impact of outsourcing jobs to an overseas location);</p>\n</li>\n<li>\n<p>economic impacts from the use of products and services (such as economic growth resulting from the use of a particular product or service).</p>\n</li>\n</ul>\n</div>\n</div>\n</div>",
                "number" : 3,
                "reportingRequirements" : [ {
                  "description" : "Examples of significant identified indirect economic impacts of the organization, including positive and negative impacts."
                }, {
                  "description" : "Significance of the indirect economic impacts in the context of external benchmarks and stakeholder priorities, such as national and international standards, protocols, and policy agendas."
                } ],
                "series" : "200",
                "subNumber" : 2,
                "title" : "Significant indirect economic impacts"
              },
              {
                "disclosureNumber" : "204-1",
                "guidance" : "<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 204-1</strong></p>\n<p>Local purchases can be made either from a budget managed at the location of operation or at an organization&rsquo;s headquarters.</p>\n<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>By supporting local suppliers, an organization can indirectly attract additional investment to the local economy. Local sourcing can be a strategy to help ensure supply, support a stable local economy, and maintain community relations.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 4,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 204-1, the reporting organization should calculate the percentages based on invoices or commitments made during the reporting period, i.e., using accruals accounting."
                } ],
                "reportingRequirements" : [ {
                  "description" : "Percentage of the procurement budget used for significant locations of operation that is spent on suppliers local to that operation (such as percentage of products and services purchased locally)."
                }, {
                  "description" : "The organization’s geographical definition of ‘local’."
                }, {
                  "description" : "The definition used for ‘significant locations of operation’."
                } ],
                "series" : "200",
                "subNumber" : 1,
                "title" : "Proportion of spending on local suppliers"
              },
              {
                "additionalRequirements" : [ {
                  "description" : ""
                } ],
                "disclosureNumber" : "205-1",
                "guidance" : "<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 205-1</strong></p>\n<p>This disclosure can include a risk assessment focused on corruption or the inclusion of corruption as a risk factor in overall risk assessments.</p>\n<p>The term &lsquo;operation&rsquo; refers to a single location used by the organization for the production, storage and/or distribution of its goods and services, or for administrative purposes. Within a single operation, there can be multiple production lines, warehouses, or other activities. For example, a single factory can be used for multiple products or a single retail outlet can contain several different retail operations that are owned or managed by the organization.</p>\n<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>This disclosure measures the extent of the risk assessment&rsquo;s implementation across an organization. Risk assessments can help to assess the potential for incidents of corruption within and related to the organization, and help the organization to design policies and procedures to combat corruption.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 5,
                "reportingRequirements" : [ {
                  "description" : "Total number and percentage of operations assessed for risks related to corruption."
                }, {
                  "description" : "Significant risks related to corruption identified through the risk assessment."
                } ],
                "series" : "200",
                "subNumber" : 1,
                "title" : "Total number and percentage of operations assessed for risks related to corruption."
              },
              {
                "disclosureNumber" : "205-2",
                "guidance" : "<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Communication and training build the internal and external awareness and the necessary capacity to combat corruption.</p>\n</div>\n</div>\n</div>",
                "number" : 5,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 205-2, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "draw from the information used for Disclosure 405-1 in GRI 405: Diversity and Equal Opportunity to identify:",
                    "subRecommendations" : [ {
                      "description" : "the governance bodies that exist within the organization, such as the board of directors, management committee, or similar body for non-corporate organizations;"
                    }, {
                      "description" : "the total number of individuals and/or employees who comprise these governance bodies;"
                    }, {
                      "description" : "the total number of employees in each employee category, excluding governance body members;"
                    } ]
                  }, {
                    "description" : "estimate the total number of business partners."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Total number and percentage of governance body members that the organization’s anti-corruption policies and procedures have been communicated to, broken down by region."
                }, {
                  "description" : "Total number and percentage of employees that the organization’s anti-corruption policies and procedures have been communicated to, broken down by employee category and region."
                }, {
                  "description" : "Total number and percentage of business partners that the organization’s anti-corruption policies and procedures have been communicated to, broken down by type of business partner and region. Describe if the organization’s anti-corruption policies and procedures have been communicated to any other persons or organizations."
                }, {
                  "description" : "Total number and percentage of governance body members that have received training on anti-corruption, broken down by region."
                }, {
                  "description" : "Total number and percentage of employees that have received training on anti-corruption, broken down by employee category and region."
                } ],
                "series" : "200",
                "subNumber" : 2,
                "title" : "Communication and training about anti-corruption policies and procedures"
              },
              {
                "disclosureNumber" : "205-3",
                "guidance" : "<div class=\"page\" title=\"Page 9\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 205-3</strong></p>\n<p>For stakeholders, there is an interest in both the occurrence of incidents and an organization&rsquo;s response to the incidents. Public legal cases regarding corruption can include current public investigations, prosecutions, or closed cases.</p>\n</div>\n</div>\n</div>",
                "number" : 5,
                "reportingRequirements" : [ {
                  "description" : "Total number and nature of confirmed incidents of corruption."
                }, {
                  "description" : "Total number of confirmed incidents in which employees were dismissed or disciplined for corruption."
                }, {
                  "description" : "Total number of confirmed incidents when contracts with business partners were terminated or not renewed due to violations related to corruption."
                }, {
                  "description" : "Public legal cases regarding corruption brought against the organization or its employees during the reporting period and the outcomes of such cases."
                } ],
                "series" : "200",
                "subNumber" : 3,
                "title" : "Confirmed incidents of corruption and actions taken"
              },
              {
                "disclosureNumber" : "206-1",
                "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>This disclosure pertains to legal actions initiated under national or international laws designed primarily for the purpose of regulating anti-competitive behavior, anti-trust, or monopoly practices.</p>\n<p>Anti-competitive behavior, anti-trust, and monopoly practices can affect consumer choice, pricing, and other factors that are essential to efficient markets. Legislation introduced in many countries seeks to control or prevent monopolies, with the underlying assumption that competition between enterprises also promotes economic efficiency and sustainable growth.</p>\n<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Legal action indicates a situation in which the market actions or status of an organization have reached a sufficient scale to merit concern by a third party. Legal decisions arising from these situations can carry the risk of significant disruption of market activities for the organization as well as punitive measures.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 6,
                "reportingRequirements" : [ {
                  "description" : "Number of legal actions pending or completed during the reporting period regarding anti-competitive behavior and violations of anti-trust and monopoly legislation in which the organization has been identified as a participant."
                }, {
                  "description" : "Main outcomes of completed legal actions, including any decisions or judgments."
                } ],
                "series" : "200",
                "subNumber" : 1,
                "title" : "Legal actions for anti-competitive behavior, anti-trust, and monopoly practices"
              },
        ];
    },

    createEnvironmentDisclosure() {
        return [
            {
                "disclosureNumber" : "301-1",
                "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 301-1</strong></p>\n</div>\n</div>\n</div>\n<p>The reported usage data are to reflect the material in its original state, and not to be presented with further data manipulation, such as reporting it as &lsquo;dry weight&rsquo;.</p>\n</div>\n</div>\n</div>",
                "number" : 1,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 301-1, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "include the following material types in the calculation of total materials used:",
                    "subRecommendations" : [ {
                      "description" : "raw materials, i.e., natural resources used for conversion to products or services, such as ores, minerals, and wood;"
                    }, {
                      "description" : "associated process materials, i.e., materials that are needed for the manufacturing process but are not part of the final product, such as lubricants for manufacturing machinery;"
                    }, {
                      "description" : "semi-manufactured goods or parts, including all forms of materials and components other than raw materials that are part of the final product;"
                    }, {
                      "description" : "materials for packaging purposes, including paper, cardboard and plastics;"
                    } ]
                  }, {
                    "description" : "report, for each material type, whether it was purchased from external suppliers or sourced internally (such as by captive production and extraction activities);"
                  }, {
                    "description" : "report whether these data are estimated or sourced from direct measurements;"
                  }, {
                    "description" : "if estimation is required, report the methods used."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Total weight or volume of materials that are used to produce and package the organization’s primary products and services during the reporting period, by:",
                  "subRequirements" : [ {
                    "description" : "non-renewable materials used;"
                  }, {
                    "description" : "ii. renewable materials used."
                  } ]
                } ],
                "series" : "300",
                "subNumber" : 1,
                "title" : "Materials used by weight or volume"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 301-2, the reporting organization shall:",
                  "subRequirements" : [ {
                    "description" : "use the total weight or volume of materials used as specified in Disclosure 301-1;"
                  }, {
                    "description" : "calculate the percentage of recycled input materials used by applying the following formula: Percentage of recycled input materials used = ( Total recycled input materials used / Total input materials used ) * 100"
                  } ]
                } ],
                "disclosureNumber" : "301-2",
                "guidance" : "<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 301-2</strong></p>\n<p>If material weight and volume measurements are stated as different units, the organization can convert measurements to standardized units.</p>\n</div>\n</div>\n</div>",
                "number" : 1,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 301-2, the reporting organization should, if estimation is required, report the methods used."
                } ],
                "reportingRequirements" : [ {
                  "description" : "Percentage of recycled input materials used to manufacture the organization’s primary products and services."
                } ],
                "series" : "300",
                "subNumber" : 2,
                "title" : "Recycled input materials used"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 301-3, the reporting organization shall:",
                  "subRequirements" : [ {
                    "description" : "exclude rejects and recalls of products;"
                  }, {
                    "description" : "calculate the percentage of reclaimed products and their packaging materials for each product category using the following formula: Percentage of reclaimed products and their packaging materials = ( Products and their packaging materials reclaimed within the reporting period / Products sold within the reporting period ) * 100"
                  } ]
                } ],
                "disclosureNumber" : "301-3",
                "guidance" : "<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 301-3</strong></p>\n<p>The reporting organization can also report recycling or reuse of packaging separately.</p>\n</div>\n</div>\n</div>",
                "number" : 1,
                "reportingRequirements" : [ {
                  "description" : "Percentage of reclaimed products and their packaging materials for each product category."
                }, {
                  "description" : "How the data for this disclosure have been collected."
                } ],
                "series" : "300",
                "subNumber" : 3,
                "title" : "Reclaimed products and their packaging materials"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 302-1, the reporting organization shall:",
                  "subRequirements" : [ {
                    "description" : "avoid the double-counting of fuel consumption, when reporting self-generated energy consumption. If the organization generates electricity from a non-renewable or renewable fuel source and then consumes the generated electricity, the energy consumption shall\nbe counted once under fuel consumption;"
                  }, {
                    "description" : "report fuel consumption separately for non-renewable and renewable fuel sources;"
                  }, {
                    "description" : "only report energy consumed by entities owned or controlled by the organization;"
                  }, {
                    "description" : "calculate the total energy consumption within the organization in joules or multiples using the following formula: Total energy consumption within the organization = Non-renewable fuel consumed + Non-renewable fuel consumed + Electricity, heating, cooling, and steam purchased for consumption + Self-generated electricity, heating, cooling, and steam, which are not consumed (see clause 2.1.1) - Electricity, heating, cooling, and steam sold"
                  } ]
                } ],
                "disclosureNumber" : "302-1",
                "guidance" : "<div class=\"column\">\n<p><strong>Background</strong></p>\n<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>For some organizations, electricity is the only significant form of energy they consume. For others, energy sources such as steam or water provided from a district heating plant or chilled water plant can also be important.</p>\n<p>Energy can be purchased from sources external to the organization or produced by the organization itself (self-generated).</p>\n<p>Non-renewable fuel sources can include fuel for combustion in boilers, furnaces, heaters, turbines, flares, incinerators, generators and vehicles that<br />are owned or controlled by the organization. Non- renewable fuel sources cover fuels purchased by the organization. They also include fuel generated by the organization&rsquo;s activities &ndash; such as mined coal, or gas from oil and gas extraction.</p>\n</div>\n</div>\n</div>\n</div>\n<div class=\"column\">\n<p>Renewable fuel sources can include biofuels, when purchased for direct use, and biomass in sources owned or controlled by the organization.</p>\n<p>Consuming non-renewable fuels is usually the main contributor to direct (Scope 1) GHG emissions, which are reported in Disclosure 305-1 of GRI 305: Emissions. Consuming purchased electricity, heating, cooling, and steam contributes to the organization&rsquo;s energy indirect (Scope 2) GHG emissions, which are reported in Disclosure 305-2 of GRI 305: Emissions.</p>\n</div>",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 302-1, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "apply conversion factors consistently for the data disclosed;"
                  }, {
                    "description" : "use local conversion factors to convert fuel to joules, or multiples, when possible;"
                  }, {
                    "description" : "use the generic conversion factors, when local conversion factors are unavailable;"
                  }, {
                    "description" : "if subject to different standards and methodologies, describe the approach to selecting them;"
                  }, {
                    "description" : "select a consistent topic Boundary for energy consumption. When possible, the Boundary should be consistent with that used in Disclosures 305-1 and 305-2 of GRI 305: Emissions;"
                  }, {
                    "description" : "where it aids transparency or comparability over time, provide a breakdown of energy consumption data by:",
                    "subRecommendations" : [ {
                      "description" : "business unit or facility;"
                    }, {
                      "description" : "country;"
                    }, {
                      "description" : "type of source (see de nitions for the listing of non-renewable sources and renewable sources);"
                    }, {
                      "description" : "type of activity."
                    } ]
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Total fuel consumption within the organization from non-renewable sources, in joules or multiples, and including fuel types used."
                }, {
                  "description" : "Total fuel consumption within the organization from renewable sources, in joules or multiples, and including fuel types used."
                }, {
                  "description" : "In joules, watt-hours or multiples, the total:",
                  "subRequirements" : [ {
                    "description" : "electricity consumption"
                  }, {
                    "description" : "heating consumption"
                  }, {
                    "description" : "cooling consumption"
                  }, {
                    "description" : "steam consumption"
                  } ]
                }, {
                  "description" : "In joules, watt-hours or multiples, the total:",
                  "subRequirements" : [ {
                    "description" : "electricity sold"
                  }, {
                    "description" : "heating sold"
                  }, {
                    "description" : "cooling sold"
                  }, {
                    "description" : "steam sold"
                  } ]
                }, {
                  "description" : "Total energy consumption within the organization, in joules or multiples."
                }, {
                  "description" : "Standards, methodologies, assumptions, and/or calculation tools used."
                }, {
                  "description" : "Source of the conversion factors used."
                } ],
                "series" : "300",
                "subNumber" : 1,
                "title" : "Energy consumption within the organization"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 302-2, the reporting organization shall exclude energy consumption reported in Disclosure 302-1."
                } ],
                "disclosureNumber" : "302-2",
                "guidance" : "<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 302-2</strong></p>\n<p>The reporting organization can identify energy consumption outside of the organization by assessing whether an activity&rsquo;s energy consumption:</p>\n<ul>\n<li>\n<p>contributes significantly to the organization&rsquo;s total anticipated energy consumption outside of the organization;</p>\n</li>\n<li>\n<p>offers potential for reductions the organization can undertake or influence;</p>\n</li>\n<li>\n<p>contributes to climate change-related risks, such as financial, regulatory, supply chain, product and customer, litigation, and reputational risks;</p>\n</li>\n<li>\n<p>is deemed material by stakeholders, such as customers, suppliers, investors, or civil society;</p>\n</li>\n<li>\n<p>results from outsourced activities previously performed in-house, or that are typically performed in-house by other organizations in the same sector;</p>\n</li>\n<li>\n<p>has been identified as significant for the organization&rsquo;s sector;</p>\n</li>\n<li>\n<p>meets any additional criteria for determining relevance, developed by the organization<br /> or by organizations in its sector.</p>\n</li>\n</ul>\n<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>The organization can use the following upstream and downstream categories and activities from the &lsquo;GHG Protocol Corporate Value Chain (Scope 3) Accounting and Reporting Standard&rsquo; for identifying relevant energy consumption outside of the organization (see reference 2 in the References section):</p>\n<p><em><strong>Upstream categories</strong></em></p>\n<p>1. Purchased goods and services</p>\n<p>2. Capital goods</p>\n<p>3. Fuel- and energy-related activities (not included in Disclosure 302-1)</p>\n<p>4. Upstream transportation and distribution 5. Waste generated in operations<br /> 6. Business travel<br /> 7. Employee commuting</p>\n<p>8. Upstream leased assets Other upstream</p>\n<p><em><strong>Downstream categories</strong></em></p>\n<p>9. Downstream transportation and distribution 10. Processing of sold products<br /> 11. Use of sold products<br /> 12. End-of-life treatment of sold products</p>\n<p>13. Downstreamleasedassets 14. Franchises<br /> 15. Investments</p>\n</div>\n</div>\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Other downstream</p>\n<div class=\"page\" title=\"Page 9\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>For each of these categories and activities, the organization can calculate or estimate the amount of energy consumed.</p>\n<p>The organization can report energy consumption separately for non-renewable sources and renewable sources.</p>\n<p><strong>Background</strong></p>\n<p>Energy consumption can occur outside an organization, i.e., throughout the organization&rsquo;s upstream and downstream activities associated with its operations.</p>\n<div class=\"page\" title=\"Page 9\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>This can include consumers&rsquo; use of products the organization sells, and the end-of-life treatment of products.</p>\n<p>Quantifying energy consumption outside of the organization can provide a basis for calculating some of the relevant other indirect (Scope 3) GHG emissions in Disclosure 305-3 of GRI 305: Emissions.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 302-2, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "if subject to different standards and methodologies, describe the approach to selecting them;"
                  }, {
                    "description" : "list energy consumption outside of the organization, with a breakdown by upstream and downstream categories and activities."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Energy consumption outside of the organization, in joules or multiples."
                }, {
                  "description" : "Standards, methodologies, assumptions, and/or calculation tools used."
                }, {
                  "description" : "Source of the conversion factors used."
                } ],
                "series" : "300",
                "subNumber" : 2,
                "title" : "Energy consumption outside of the organization"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 302-3, the reporting organization shall:",
                  "subRequirements" : [ {
                    "description" : "calculate the ratio by dividing the absolute energy consumption (the numerator) by the organization-specific metric (the denominator);\n"
                  }, {
                    "description" : "if reporting an intensity ratio both for the energy consumed within the organization and outside of it, report these intensity ratios separately."
                  } ]
                } ],
                "disclosureNumber" : "302-3",
                "guidance" : "<div class=\"page\" title=\"Page 10\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 302-3</strong></p>\n<p>Intensity ratios can be provided for, among others:</p>\n<ul>\n<li>\n<p>products (such as energy consumed per unit produced);</p>\n</li>\n<li>\n<p>services (such as energy consumed per function or per service);</p>\n</li>\n<li>\n<p>sales (such as energy consumed per monetary unit of sales).</p>\n</li>\n</ul>\n<p>Organization-specific metrics (denominators) can include:</p>\n<ul>\n<li>\n<p>units of product;</p>\n</li>\n<li>\n<p>production volume (such as metric tons, liters, or MWh);</p>\n</li>\n<li>\n<p>size (such as m2 floor space);</p>\n</li>\n<li>\n<p>number of full-time employees;</p>\n</li>\n<li>\n<p>monetary units (such as revenue or sales).</p>\n</li>\n</ul>\n<div class=\"page\" title=\"Page 10\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Energy intensity ratios define energy consumption in the context of an organization-specific metric.</p>\n<p>These ratios express the energy required per unit of activity, output, or any other organization-specific metric. Intensity ratios are often called normalized environmental impact data.</p>\n<p>In combination with the organization&rsquo;s total energy consumption, reported in Disclosures 302-1 and 302-2, energy intensity helps to contextualize the organization&rsquo;s efficiency, including in relation to other organizations.</p>\n<p>See references 1 and 3 in the References section.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 302-3, the reporting organization should, where it aids transparency or comparability over time, provide a breakdown of the energy intensity ratio by:",
                  "subRecommendations" : [ {
                    "description" : "business unit or facility;"
                  }, {
                    "description" : "country;"
                  }, {
                    "description" : "type of source (see definitions for the listing of non-renewable sources and renewable sources);"
                  }, {
                    "description" : "type of activity."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Energy intensity ratio for the organization."
                }, {
                  "description" : "Organization-specific metric (the denominator) chosen to calculate the ratio."
                }, {
                  "description" : "Types of energy included in the intensity ratio; whether fuel, electricity, heating, cooling, steam, or all."
                }, {
                  "description" : "Whether the ratio uses energy consumption within the organization, outside of it, or both."
                } ],
                "series" : "300",
                "subNumber" : 3,
                "title" : "Energy intensity"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 302-4, the reporting organization shall:",
                  "subRequirements" : [ {
                    "description" : "exclude reductions resulting from reduced production capacity or outsourcing;"
                  }, {
                    "description" : "describe whether energy reduction is estimated, modeled, or sourced from direct measurements. If estimation or modeling is used, the organization shall disclose the methods used."
                  } ]
                } ],
                "disclosureNumber" : "302-4",
                "guidance" : "<div class=\"page\" title=\"Page 11\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 302-4</strong></p>\n<p>The reporting organization can prioritize disclosing reduction initiatives that were implemented in the reporting period, and that have the potential to contribute significantly to reductions. Reduction initiatives and their targets can be described in the management approach for this topic.</p>\n<p>Reduction initiatives can include:</p>\n<ul>\n<li>\n<p>process redesign;</p>\n</li>\n<li>\n<p>conversion and retrofitting of equipment;</p>\n</li>\n<li>\n<p>changes in behavior;</p>\n</li>\n<li>\n<p>operational changes.</p>\n</li>\n</ul>\n<div class=\"page\" title=\"Page 11\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>The organization can report reductions in energy consumption by combining energy types, or separately for fuel, electricity, heating, cooling, and steam.</p>\n<p>The organization can also provide a breakdown of reductions in energy consumption by individual initiatives or groups of initiatives.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 302-4, the reporting organization should, if subject to different standards and methodologies, describe the approach to selecting them."
                } ],
                "reportingRequirements" : [ {
                  "description" : "Amount of reductions in energy consumption achieved as a direct result of conservation and efficiency initiatives, in joules or multiples."
                }, {
                  "description" : "Types of energy included in the reductions; whether fuel, electricity, heating, cooling, steam, or all."
                }, {
                  "description" : "Basis for calculating reductions in energy consumption, such as base year or baseline, including the rationale for choosing it."
                }, {
                  "description" : "Standards, methodologies, assumptions, and/or calculation tools used."
                } ],
                "series" : "300",
                "subNumber" : 4,
                "title" : "Reduction of energy consumption"
              },
              {
                "disclosureNumber" : "302-5",
                "guidance" : "",
                "number" : 2,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 302-5, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "if subject to different standards and methodologies, describe the approach to selecting them;"
                  }, {
                    "description" : "refer to industry use standards to obtain this information, where available (such as fuel consumption of cars for 100 km at 90 km/h)."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Reductions in energy requirements of sold products and services achieved during the reporting period, in joules or multiples."
                }, {
                  "description" : "Basis for calculating reductions in energy consumption, such as base year or baseline, including the rationale for choosing it."
                }, null, {
                  "description" : "Standards, methodologies, assumptions, and/or calculation tools used."
                } ],
                "series" : "300",
                "subNumber" : 5,
                "title" : "Reductions in energy requirements of products and services"
              },
              {
                "disclosureNumber" : "303-1",
                "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 303-1</strong></p>\n<p>This disclosure can include water withdrawn either directly by the organization or through intermediaries, such as water utilities.</p>\n<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Reporting the total volume of water withdrawn by source contributes to an understanding of the overall scale of potential impacts and risks associated with an organization&rsquo;s water use. The total volume withdrawn provides an indication of the organization&rsquo;s relative size and importance as a user of water, and provides a baseline figure for other calculations relating to efficiency and use.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 3,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 303-1, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "include the abstraction of cooling water;"
                  }, {
                    "description" : "report whether these calculations are estimated, modelled, or sourced from direct measurements;"
                  }, {
                    "description" : "if estimation or modelling has been used, report the estimation or modelling methods. ￼303-1 ￼￼￼"
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Total volume of water withdrawn, with a breakdown by the following sources:",
                  "subRequirements" : [ {
                    "description" : "Surface water, including water from wetlands, rivers, lakes, and oceans;"
                  }, {
                    "description" : "Ground water;"
                  }, {
                    "description" : "Rainwater collected directly and stored by the organization;"
                  }, {
                    "description" : "Waste water from another organization;"
                  }, {
                    "description" : "Municipal water supplies or other public or private water utilities."
                  } ]
                }, {
                  "description" : "Standards, methodologies, and assumptions used."
                } ],
                "series" : "300",
                "subNumber" : 1,
                "title" : "Water withdrawal by source"
              },
              {
                "disclosureNumber" : "303-2",
                "guidance" : "<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>See references 1 and 3 in the References section.</p>\n</div>\n</div>\n</div>",
                "number" : 3,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 303-2, the reporting organization should report the original water body or source, if the water is provided by municipal water supplies or other public or private water utilities."
                } ],
                "reportingRequirements" : [ {
                  "description" : "Total number of water sources significantly affected by withdrawal by type:",
                  "subRequirements" : [ {
                    "description" : "Size of the water source;"
                  }, {
                    "description" : "Whether the source is designated as a nationally or internationally protected area;"
                  }, {
                    "description" : "Biodiversity value (such as species diversity and endemism, and total number of protected species);"
                  }, {
                    "description" : "Value or importance of the water source to local communities and indigenous peoples."
                  } ]
                }, {
                  "description" : "Standards, methodologies, and assumptions used."
                } ],
                "series" : "300",
                "subNumber" : 2,
                "title" : "Water sources significantly affected by withdrawal of water"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 303-3, the reporting organization shall include grey water, i.e., collected rainwater and wastewater generated by household processes, such as washing dishes, laundry, and bathing."
                } ],
                "disclosureNumber" : "303-3",
                "guidance" : "<p><strong>Guidance for Disclosure 303-3</strong></p>\n<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>This disclosure measures both water treated prior to reuse and water not treated prior to reuse.</p>\n<p><strong>Guidance for clause 2.5.2</strong></p>\n<p>For example, if an organization has a production cycle that requires 20 m3 of water per cycle, the organization withdraws 20 m3 of water for one production process cycle and reuses it for an additional three cycles, then the total volume of water recycled and reused for that process is 60 m3.</p>\n</div>\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>The rate of water reuse and recycling is a measure of efficiency and demonstrates the success of an organization in reducing total water withdrawals and discharges. Increased reuse and recycling can reduce water consumption, treatment, and disposal costs. Reducing water consumption over time through reuse and recycling also contributes to local, national, or regional goals for managing water supplies.</p>\n</div>\n</div>\n</div>",
                "number" : 3,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 303-3, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "report if water or flow meters do not exist and estimation by modeling is required;"
                  }, {
                    "description" : "calculate the volume of recycled/reused water based on the volume of water demand satisfied by recycled/reused water, rather than by further withdrawals."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Total volume of water recycled and reused by the organization."
                }, {
                  "description" : "Total volume of water recycled and reused as a percentage of the total water withdrawal as specified in Disclosure 303-1."
                }, {
                  "description" : "Standards, methodologies, and assumptions used."
                } ],
                "series" : "300",
                "subNumber" : 3,
                "title" : "Water recycled and reused"
              },
              {
                "disclosureNumber" : "304-1",
                "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Monitoring which activities are taking place in both protected areas and areas of high biodiversity value outside protected areas makes it possible for the organization to reduce the risks of impacts. It also makes it possible for the organization to manage impacts on biodiversity or to avoid mismanagement.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 4,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 304-1, the reporting organization should include information about sites for which future operations have been formally announced."
                } ],
                "reportingRequirements" : [ {
                  "description" : "For each operational site owned, leased, managed in, or adjacent to, protected areas and areas of high biodiversity value outside protected areas, the following information:",
                  "subRequirements" : [ {
                    "description" : "Geographic location;"
                  }, {
                    "description" : "Subsurface and underground land that may be owned, leased, or managed by the organization;"
                  }, {
                    "description" : "Position in relation to the protected area (in the area, adjacent to, or containing portions of the protected area) or the high biodiversity value area outside protected areas;"
                  }, {
                    "description" : "Type of operation (office, manufacturing or production, or extractive);"
                  }, {
                    "description" : "Size of operational site in km2 (or another unit, if appropriate);"
                  }, {
                    "description" : "Biodiversity value characterized by the attribute of the protected area or area of high biodiversity value outside the protected area (terrestrial, freshwater, or maritime ecosystem);"
                  }, {
                    "description" : "Biodiversity value characterized by listing of protected status (such as IUCN Protected Area Management Categories, Ramsar Convention, national legislation)."
                  } ]
                } ],
                "series" : "300",
                "subNumber" : 1,
                "title" : "Operational sites owned, leased, managed in, or adjacent to, protected areas and areas of high biodiversity value outside protected areas"
              },
              {
                "disclosureNumber" : "304-2",
                "guidance" : "<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 304-2</strong></p>\n<p>Indirect impacts on biodiversity can include impacts in the supply chain.</p>\n<p>Areas of impact are not limited to areas that are formally protected and include consideration of impacts on buffer zones, as well as formally designated areas of special importance or sensitivity.</p>\n<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>This disclosure provides the background for understanding (and developing) an organization&rsquo;s strategy to mitigate significant direct and indirect impacts on biodiversity. By presenting structured and qualitative information, the disclosure enables comparison of the relative size, scale, and nature of impacts over time and across organizations.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 4,
                "reportingRequirements" : [ {
                  "description" : "Nature of significant direct and indirect impacts on biodiversity with reference to one or more of the following:",
                  "subRequirements" : [ {
                    "description" : "Construction or use of manufacturing plants, mines, and transport infrastructure;"
                  }, {
                    "description" : "Pollution (introduction of substances that do not naturally occur in the habitat from point and non-point sources);"
                  }, {
                    "description" : "Introduction of invasive species, pests, and pathogens;"
                  }, {
                    "description" : "Reduction of species;"
                  }, {
                    "description" : "Habitat conversion;"
                  }, {
                    "description" : "Changes in ecological processes outside the natural range of variation (such as salinity or changes in groundwater level)."
                  } ]
                }, {
                  "description" : "Significant direct and indirect positive and negative impacts with reference to the following:",
                  "subRequirements" : [ {
                    "description" : "Species affected;"
                  }, {
                    "description" : "Extent of areas impacted;"
                  }, {
                    "description" : "Duration of impacts;"
                  }, {
                    "description" : "Reversibility or irreversibility of the impacts."
                  } ]
                } ],
                "series" : "300",
                "subNumber" : 2,
                "title" : "Significant impacts of activities, products, and services on biodiversity"
              },
              {
                "disclosureNumber" : "304-3",
                "guidance" : "<div class=\"page\" title=\"Page 9\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 304-3</strong></p>\n<p>This disclosure addresses the extent of an organization&rsquo;s prevention and remediation activities with respect to its impacts on biodiversity. This disclosure refers to areas where remediation has been completed or where the area is actively protected. Areas where operations are still active can be counted if they conform to the definitions of &lsquo;<span style=\"text-decoration: underline;\">area restored</span>&rsquo; or &lsquo;<span style=\"text-decoration: underline;\">area protected</span>&rsquo;.</p>\n</div>\n</div>\n</div>",
                "number" : 4,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 304-3, the reporting organization should align the information presented in this disclosure with regulatory or license requirements for the protection or restoration of habitats, if applicable."
                } ],
                "reportingRequirements" : [ {
                  "description" : "Size and location of all habitat areas protected or restored, and whether the success of the restoration measure was or is approved by independent external professionals."
                }, {
                  "description" : "Whether partnerships exist with third parties to protect or restore habitat areas distinct from where the organization has overseen and implemented restoration or protection measures."
                }, {
                  "description" : "Status of each area based on its condition at the close of the reporting period."
                }, {
                  "description" : "Standards, methodologies, and assumptions used."
                } ],
                "series" : "300",
                "subNumber" : 3,
                "title" : "Habitats protected or restored"
              },
              {
                "disclosureNumber" : "304-4",
                "guidance" : "<div class=\"page\" title=\"Page 10\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>This disclosure helps an organization to identify where its activities pose a threat to endangered plant and animal species. By identifying these threats, the organization can initiate appropriate steps to avoid harm and to prevent the extinction of species.<br /> The International Union for Conservation of Nature (IUCN) &lsquo;Red List of Threatened Species&rsquo; (an inventory of the global conservation status of plant and animal species) and national conservation lists serve as authorities on the sensitivity of habitat in areas affected by operations, and on the relative importance of these habitats from a management perspective.</p>\n</div>\n<div class=\"column\">\n<p>See reference 8 in the References section.</p>\n</div>\n</div>\n</div>",
                "number" : 4,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 304-4, the reporting organization should compare the information in the IUCN Red List and national conservation lists with the species outlined in planning documentation and monitoring records to ensure consistency."
                } ],
                "reportingRequirements" : [ {
                  "description" : "Total number of IUCN Red List species and national conservation list species with habitats in areas affected by the operations of the organization, by level of extinction risk:",
                  "subRequirements" : [ {
                    "description" : "Critically endangered"
                  }, {
                    "description" : "Endangered"
                  }, {
                    "description" : "Vulnerable"
                  }, {
                    "description" : "Near threatened"
                  }, {
                    "description" : "Least concern"
                  } ]
                } ],
                "series" : "300",
                "subNumber" : 4,
                "title" : "IUCN Red List species and national conservation list species with habitats in areas affected by operations"
              },
              {
                "disclosureNumber" : "305-1",
                "guidance" : "<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 305-1</strong></p>\n<p>Direct (Scope 1) GHG emissions include, but are not limited to, the CO2 emissions from the fuel consumption as reported in Disclosure 302-1 of GRI 302: Energy.</p>\n<p>Direct (Scope 1) GHG emissions can come from the following sources owned or controlled by an organization:</p>\n<ul>\n<li>\n<p>Generation of electricity, heating, cooling and steam: these emissions result from combustion of fuels in stationary sources, such as boilers, furnaces, and turbines &ndash; and from other combustion processes such as flaring;</p>\n</li>\n<li>\n<p>Physical or chemical processing: most of these emissions result from the manufacturing or processing of chemicals and materials, such as cement, steel, aluminum, ammonia, and waste processing;</p>\n</li>\n<li>\n<p>Transportation of materials, products, waste, workers, and passengers: these emissions result from the combustion of fuels in mobile combustion sources owned or controlled by the organization, such as trucks, trains, ships, airplanes, buses,<br /> and cars;</p>\n</li>\n<li>\n<p>Fugitive emissions: these are emissions that are not physically controlled but result from intentional or unintentional releases of GHGs. These can include equipment leaks from joints, seals, packing, and gaskets; methane emissions (e.g., from coal mines) and venting; HFC emissions from refrigeration and air conditioning equipment; and methane leakages (e.g., from gas transport).</p>\n</li>\n</ul>\nMethodologies used to calculate the direct (Scope I) GHG emissions can include:<br />\n<ul>\n<li>\n<p>direct measurement of energy source consumed (coal, gas) or losses (refills) of cooling systems and conversion to GHG (CO2 equivalents);</p>\n</li>\n<li>\n<p>mass balance calculations;</p>\n</li>\n<li>\n<p>calculations based on site-specific data, such as for fuel composition analysis;</p>\n</li>\n<li>\n<p>calculations based on published criteria, such as emission factors and GWP rates;</p>\n</li>\n<li>\n<p>direct measurements of GHG emissions, such as continuous online analyzers;</p>\n</li>\n<li>\n<p>estimations.</p>\n</li>\n</ul>\n<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>If estimations are used due to a lack of default figures, the reporting organization can indicate the basis and assumptions on which figures were estimated.</p>\n<p>For recalculations of prior year emissions, the organization can follow the approach in the &lsquo;GHG Protocol Corporate Standard&rsquo;.</p>\n<p>The chosen emission factors can originate from mandatory reporting requirements, voluntary reporting frameworks, or industry groups.</p>\n<p>Estimates of GWP rates change over time as scientific research develops. GWP rates from the Second Assessment Report of the Intergovernmental Panel<br /> on Climate Change (IPCC) are used as the basis for international negotiations under the &lsquo;Kyoto Protocol&rsquo;. Thus, such rates can be used for disclosing GHG emissions where it does not conflict with national or regional reporting requirements. The organization can also use the latest GWP rates from the most recent IPCC assessment report.</p>\n<p>The organization can combine Disclosure 305-1 with Disclosures 305-2 (energy indirect/Scope 2 GHG emissions) and 305-3 (other indirect/Scope 3 GHG emissions) to disclose total GHG emissions.</p>\n<p>Further details and guidance are available in the &lsquo;GHG Protocol Corporate Standard&rsquo;. See also references 1, 2, 12, 13, 14 and 19 in the References section.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 5,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 305-1, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "apply emission factors and GWP rates consistently for the data disclosed;"
                  }, {
                    "description" : "use the GWP rates from the IPCC assessment reports based on a 100-year timeframe;"
                  }, {
                    "description" : "select a consistent approach for consolidating direct (Scope 1) and energy indirect (Scope 2) GHG emissions; choosing from the equity share, financial control, or operational control methods outlined in the ‘GHG Protocol Corporate Standard’;"
                  }, {
                    "description" : "if subject to different standards and methodologies, describe the approach to selecting them;"
                  }, {
                    "description" : "where it aids transparency or comparability over time, provide a breakdown of the direct (Scope 1) GHG emissions by:",
                    "subRecommendations" : [ {
                      "description" : "business unit or facility;"
                    }, {
                      "description" : "country;"
                    }, {
                      "description" : "type of source (stationary combustion, process, fugitive);"
                    }, {
                      "description" : "type of activity."
                    } ]
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Gross direct (Scope 1) GHG emissions in metric tons of CO2 equivalent."
                }, {
                  "description" : "Gases included in the calculation; whether CO2, CH4, N2O, HFCs, PFCs, SF6, NF3, or all."
                }, {
                  "description" : "Biogenic CO2 emissions in metric tons of CO2 equivalent."
                }, {
                  "description" : "Base year for the calculation, if applicable, including:",
                  "subRequirements" : [ {
                    "description" : "the rationale for choosing it;"
                  }, {
                    "description" : "emissions in the base year;"
                  }, {
                    "description" : "the context for any significant changes in emissions that triggered recalculations of base year emissions."
                  } ]
                }, {
                  "description" : "Source of the emission factors and the global warming potential (GWP) rates used, or a reference to the GWP source."
                }, {
                  "description" : "Consolidation approach for emissions; whether equity share, financial control, or operational control."
                }, {
                  "description" : "Standards, methodologies, assumptions, and/or calculation tools used."
                } ],
                "series" : "300",
                "subNumber" : 1,
                "title" : "Direct (Scope 1) GHG emissions"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 305-2, the reporting organization shall:\n",
                  "subRequirements" : [ {
                    "description" : "exclude any GHG trades from the calculation of gross energy indirect (Scope 2) GHG emissions;"
                  }, {
                    "description" : "exclude other indirect (Scope 3) GHG emissions that are disclosed as specified in Disclosure 305-3;"
                  }, {
                    "description" : "account and report energy indirect (Scope 2) GHG emissions based on the location- based method, if it has operations in markets without product or supplier-specific data;"
                  }, {
                    "description" : "account and report energy indirect (Scope 2) GHG emissions based on both the location-based and market-based methods, if it has any operations in markets providing product or supplier-specific data in the form of contractual instruments."
                  } ]
                } ],
                "disclosureNumber" : "305-2",
                "guidance" : "<div class=\"page\" title=\"Page 10\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 305-2</strong></p>\n<p>Energy indirect (Scope 2) GHG emissions include, but are not limited to, the CO2 emissions from the generation of purchased or acquired electricity, heating, cooling, and steam consumed by an organization &ndash; disclosed as specified in Disclosure 302-1 of GRI 302: Energy. For many organizations, the energy indirect (Scope 2) GHG emissions that result from the generation of purchased electricity can be much greater than their direct (Scope 1) GHG emissions.</p>\n<p>The &lsquo;GHG Protocol Scope 2 Guidance&rsquo; requires organizations to provide two distinct Scope 2 values: a location-based and a market-based value. A location- based method reflects the average GHG emissions intensity of grids on which energy consumption occurs, using mostly grid-average emission factor data. A market-based method reflects emissions from electricity that an organization has purposefully chosen (or its lack of choice). It derives emission factors from contractual instruments, which include any type of contract between two parties for the sale and purchase of energy bundled with attributes about the energy generation, or for unbundled attribute claims.</p>\n<p>The market-based method calculation also includes the use of a residual mix, if the organization does not have specified emissions-intensity from its contractual instruments. This helps prevent double counting between consumers&rsquo; market-based method figures. If a residual mix is unavailable, the organization can disclose this and use grid-average emission factors as a proxy (which can mean that the location-based and market- based are the same number until information on the residual mix is available).</p>\n<p>The reporting organization can apply the Quality Criteria in the &lsquo;GHG Protocol Scope 2 Guidance&rsquo; so that contractual instruments convey GHG emission rate claims and to prevent double counting. See reference 18 in the References section.</p>\n<p>For recalculations of prior year emissions, the organization can follow the approach in the &lsquo;GHG Protocol Corporate Standard&rsquo;.</p>\n<p>The chosen emission factors can originate from mandatory reporting requirements, voluntary reporting frameworks, or industry groups.</p>\n</div>\n<div class=\"column\">\n<p>Estimates of GWP rates change over time as scientific research develops. GWP rates from the Second Assessment Report of the IPCC are used as the basis for international negotiations under the &lsquo;Kyoto Protocol&rsquo;. Thus, such rates can be used for disclosing GHG emissions where it does not conflict with national or regional reporting requirements. The organization can also use the latest GWP rates from the most recent IPCC assessment report.</p>\n<p>The organization can combine Disclosure 305-2 with Disclosures 305-1 (direct/Scope 1 GHG emissions) and 305-3 (other indirect/Scope 3 GHG emissions) to disclose total GHG emissions.</p>\n<p>Further details and guidance are available in the &lsquo;GHG Protocol Corporate Standard&rsquo;. Details on the location- based and market-based methods are available in the &lsquo;GHG Protocol Scope 2 Guidance&rsquo;. See also references 1, 2, 12, 13, 14 and 18 in the References section.</p>\n</div>\n</div>\n</div>",
                "number" : 5,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 305-2, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "apply emission factors and GWP rates consistently for the data disclosed;"
                  }, {
                    "description" : "use the GWP rates from the IPCC assessment reports based on a 100-year timeframe;"
                  }, {
                    "description" : "select a consistent approach for consolidating direct (Scope 1) and energy indirect (Scope 2) GHG emissions, choosing from the equity share, financial control, or operational control methods outlined in the ‘GHG Protocol Corporate Standard’;"
                  }, {
                    "description" : "if subject to different standards and methodologies, describe the approach to selecting them;"
                  }, {
                    "description" : "where it aids transparency or comparability over time, provide a breakdown of the energy indirect (Scope 2) GHG emissions by:",
                    "subRecommendations" : [ {
                      "description" : "business unit or facility;"
                    }, {
                      "description" : "country;"
                    }, {
                      "description" : "type of source (stationary combustion, process, fugitive);"
                    }, {
                      "description" : "type of activity."
                    } ]
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Gross location-based energy indirect (Scope 2) GHG emissions in metric tons of CO2 equivalent."
                }, {
                  "description" : "If applicable, gross market-based energy indirect (Scope 2) GHG emissions in metric tons of CO2 equivalent."
                }, {
                  "description" : "If available, the gases included in the calculation; whether CO2, CH4, N2O, HFCs, PFCs, SF6, NF3, or all."
                }, {
                  "description" : "Base year for the calculation, if applicable, including:",
                  "subRequirements" : [ {
                    "description" : "the rationale for choosing it;"
                  }, {
                    "description" : "emissions in the base year;"
                  }, {
                    "description" : "the context for any significant changes in emissions that triggered recalculations of base year emissions."
                  } ]
                }, {
                  "description" : "Source of the emission factors and the global warming potential (GWP) rates used, or a reference to the GWP source."
                }, {
                  "description" : "Consolidation approach for emissions; whether equity share, financial control, or operational control."
                }, {
                  "description" : "Standards, methodologies, assumptions, and/or calculation tools used."
                } ],
                "series" : "300",
                "subNumber" : 2,
                "title" : "Energy indirect (Scope 2) GHG emissions"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 305-3, the reporting organization shall:",
                  "subRequirements" : [ {
                    "description" : "exclude any GHG trades from the calculation of gross other indirect (Scope 3) GHG emissions;"
                  }, {
                    "description" : "exclude energy indirect (Scope 2) GHG emissions from this disclosure. Energy indirect (Scope 2) GHG emissions are disclosed as specified in Disclosure 305-2;"
                  }, {
                    "description" : "report biogenic emissions of CO2 from the combustion or biodegradation of biomass that occur in its value chain separately from the gross other indirect (Scope 3) GHG emissions. Exclude biogenic emissions of other types of GHG (such as CH4 and N2O), and biogenic emissions of CO2 that occur in the life cycle of biomass other than from combustion or biodegradation (such as GHG emissions from processing or transporting biomass)."
                  } ]
                } ],
                "disclosureNumber" : "305-3",
                "guidance" : "<p>&nbsp;</p>\n<div class=\"page\" title=\"Page 12\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 305-3</strong></p>\n<p>Other indirect (Scope 3) GHG emissions are a consequence of an organization&rsquo;s activities, but<br /> occur from sources not owned or controlled by the organization. Other indirect (Scope 3) GHG emissions include both upstream and downstream emissions. Some examples of Scope 3 activities include extracting and producing purchased materials; transporting purchased fuels in vehicles not owned or controlled by the organization; and the end use of products and services.</p>\n<p>Other indirect emissions can also come from the decomposing of the organization&rsquo;s waste. Process- related emissions during the manufacture of purchased goods and fugitive emissions in facilities not owned by the organization can also produce indirect emissions.</p>\n<p>For some organizations, GHG emissions that result from energy consumption outside of the organization can be much greater than their direct (Scope 1) or energy indirect (Scope 2) GHG emissions.</p>\n<p>The reporting organization can identify other indirect (Scope 3) GHG emissions by assessing which of its activities&rsquo; emissions:</p>\n<ul>\n<li>\n<p>contribute significantly to the organization&rsquo;s total anticipated other indirect (Scope 3) GHG emissions;</p>\n</li>\n<li>\n<p>offer potential for reductions the organization can undertake or influence;</p>\n</li>\n<li>\n<p>contribute to climate change-related risks, such as financial, regulatory, supply chain, product and customer, litigation, and reputational risks;</p>\n</li>\n<li>\n<p>are deemed material by stakeholders, such as customers, suppliers, investors, or civil society;</p>\n</li>\n<li>\n<p>result from outsourced activities previously performed in-house, or that are typically performed in-house by other organizations in the same sector;</p>\n</li>\n<li>\n<p>have been identified as significant for the organization&rsquo;s sector;</p>\n</li>\n<li>\n<p>meet any additional criteria for determining relevance, developed by the organization or by organizations in its sector.</p>\n</li>\n</ul>\n<p>The organization can use the following upstream and downstream categories and activities from the &lsquo;GHG Protocol Corporate Value Chain Standard&rsquo; (see reference 15 in the References section):</p>\n<div class=\"page\" title=\"Page 12\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><em><strong>Upstream categories</strong></em></p>\n<p>1. Purchased goods and services</p>\n<p>2. Capital goods</p>\n<p>3. Fuel- and energy-related activities (not included in Scope 1 or Scope 2)</p>\n<p>4. Upstream transportation and distribution</p>\n<p>5. Waste generated in operations<br /> 6. Business travel<br /> 7. Employee commuting</p>\n<p>8. Upstream leased assets Other upstream</p>\n<p><em><strong>Downstream categories</strong></em></p>\n<p>9. Downstream transportation and distribution 10. Processing of sold products<br /> 11. Use of sold products<br /> 12. End-of-life treatment of sold products</p>\n<p>13. Downstreamleasedassets 14. Franchises<br /> 15. Investments</p>\n<p>Other downstream</p>\n<p>For each of these categories and activities, the organization can provide a figure in CO2 equivalent or explain why certain data are not included.</p>\n<p>For recalculations of prior year emissions, the organization can follow the approach in the &lsquo;GHG Protocol Corporate Value Chain Standard&rsquo;.</p>\n<p>The chosen emission factors can originate from mandatory reporting requirements, voluntary reporting frameworks, or industry groups.</p>\n<p>Estimates of GWP rates change over time as scientific research develops. GWP rates from the Second Assessment Report of the IPCC are used as the basis for international negotiations under the &lsquo;Kyoto Protocol&rsquo;. Thus, such rates can be used for disclosing GHG emissions where it does not conflict with national or regional reporting requirements. The organization can also use the latest GWP rates from the most recent IPCC assessment report.</p>\n<p>The organization can combine Disclosure 305-3 with Disclosures 305-1 (direct/Scope 1 GHG emissions) and 305-2 (energy indirect/Scope 2 GHG emissions) to disclose total GHG emissions.</p>\n<p>See references 1, 2, 12, 13, 15, 17 and 19 in the References section.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 5,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 305-3, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "apply emission factors and GWP rates consistently for the data disclosed;"
                  }, {
                    "description" : "use the GWP rates from the IPCC assessment reports based on a 100-year timeframe;"
                  }, {
                    "description" : "if subject to different standards and methodologies, describe the approach to selecting them;"
                  }, {
                    "description" : "list other indirect (Scope 3) GHG emissions, with a breakdown by upstream and downstream categories and activities;"
                  }, {
                    "description" : "where it aids transparency or comparability over time, provide a breakdown of the other indirect (Scope 3) GHG emissions by:",
                    "subRecommendations" : [ {
                      "description" : "business unit or facility;"
                    }, {
                      "description" : "country;"
                    }, {
                      "description" : "type of source (stationary combustion, process, fugitive);"
                    }, {
                      "description" : "type of activity."
                    } ]
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Gross other indirect (Scope 3) GHG emissions in metric tons of CO2 equivalent."
                }, {
                  "description" : "If available, the gases included in the calculation; whether CO2, CH4, N2O, HFCs, PFCs, SF6, NF3, or all."
                }, {
                  "description" : "Biogenic CO2 emissions in metric tons of CO2 equivalent."
                }, {
                  "description" : "Other indirect (Scope 3) GHG emissions categories and activities included in the calculation."
                }, {
                  "description" : "Base year for the calculation, if applicable, including:",
                  "subRequirements" : [ {
                    "description" : "the rationale for choosing it;"
                  }, {
                    "description" : "emissions in the base year;"
                  }, {
                    "description" : "the context for any significant changes in emissions that triggered recalculations of base year emissions."
                  } ]
                }, {
                  "description" : "Source of the emission factors and the global warming potential (GWP) rates used, or a reference to the GWP source."
                }, {
                  "description" : "Standards, methodologies, assumptions, and/or calculation tools used."
                } ],
                "series" : "300",
                "subNumber" : 3,
                "title" : "Other indirect (Scope 3) GHG emissions"
              },
              {
                "disclosureNumber" : "305-4",
                "guidance" : "<div class=\"page\" title=\"Page 13\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 305-4</strong></p>\n<p>Intensity ratios can be provided for, among others:</p>\n<ul>\n<li>\n<p>products (such as metric tons of CO2 emissions per unit produced);</p>\n</li>\n<li>\n<p>services (such as metric tons of CO2 emissions per function or per service);</p>\n</li>\n<li>\n<p>sales (such as metric tons of CO2 emissions per sales).</p>\n</li>\n</ul>\n<p>Organization-specific metrics (denominators) can include:</p>\n<ul>\n<li>\n<p>units of product;</p>\n</li>\n<li>\n<p>production volume (such as metric tons, liters, or MWh);</p>\n</li>\n<li>\n<p>size (such as m2 floor space);</p>\n</li>\n<li>\n<p>number of full-time employees;</p>\n</li>\n<li>\n<p>monetary units (such as revenue or sales).</p>\n</li>\n</ul>\n<div class=\"page\" title=\"Page 13\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>The reporting organization can report an intensity ratio for direct (Scope 1) and energy indirect (Scope 2) GHG emissions combined, using the figures reported in Disclosures 305-1 and 305-2.</p>\n<p><strong>Background</strong></p>\n<p>Intensity ratios define GHG emissions in the context of an organization-specific metric. Many organizations track environmental performance with intensity ratios, which are often called normalized environmental impact data.</p>\n<p>GHG emissions intensity expresses the amount of GHG emissions per unit of activity, output, or any other organization-specific metric. In combination with an organization&rsquo;s absolute GHG emissions, reported in Disclosures 305-1, 305-2, and 305-3, GHG emissions intensity helps to contextualize the organization&rsquo;s efficiency, including in relation to other organizations.</p>\n<p>See references 13, 14, and 19 in the References section.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 5,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 305-4, the reporting organization should, where it aids transparency or comparability over time, provide a breakdown of the GHG emissions intensity ratio by:",
                  "subRecommendations" : [ {
                    "description" : "business unit or facility;"
                  }, {
                    "description" : "country;"
                  }, {
                    "description" : "type of source;"
                  }, {
                    "description" : "type of activity."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "GHG emissions intensity ratio for the organization."
                }, {
                  "description" : "Organization-specific metric (the denominator) chosen to calculate the ratio."
                }, {
                  "description" : "Types of GHG emissions included in the intensity ratio; whether direct (Scope 1), energy indirect (Scope 2), and/or other indirect (Scope 3)."
                }, {
                  "description" : "Gases included in the calculation; whether CO2, CH4, N2O, HFCs, PFCs, SF6, NF3, or all."
                } ],
                "series" : "300",
                "subNumber" : 4,
                "title" : "GHG emissions intensity"
              },
              {
                "disclosureNumber" : "305-5",
                "guidance" : "<div class=\"page\" title=\"Page 14\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 305-5</strong></p>\n<p>The reporting organization can prioritize disclosing reduction initiatives that were implemented in the reporting period, and that have the potential to contribute significantly to reductions. Reduction initiatives and their targets can be described in the management approach for this topic.</p>\n<p>Reduction initiatives can include:</p>\n<ul>\n<li>\n<p>process redesign;</p>\n</li>\n<li>\n<p>conversion and retrofitting of equipment;</p>\n</li>\n<li>\n<p>fuel switching;</p>\n</li>\n<li>\n<p>changes in behavior;</p>\n</li>\n<li>\n<p>offsets.</p>\n</li>\n</ul>\n<p>The organization can report reductions disaggregated by initiatives or groups of initiatives.</p>\n<p>This disclosure can be used in combination with Disclosures 305-1, 305-2, and 305-3 of this Standard to monitor the reduction of GHG emissions with reference to the organization&rsquo;s targets, or to regulations and trading systems at international or national level.</p>\n</div>\n<div class=\"column\">\n<p>See references 12, 13, 14, 15, 16, and 19 in the References section.</p>\n<p><strong>Guidance for clause 2.9.2</strong></p>\n<p>The inventory method compares reductions to a base year. The project method compares reductions to a baseline. Further details on these methods are available in references 15 and 16 in the References section.</p>\n<p><strong>Guidance for clause 2.9.3</strong></p>\n<p>Primary effects are the elements or activities designed to reduce GHG emissions, such as carbon storage. Secondary effects are smaller, unintended consequences of a reduction initiative, including changes to production or manufacture, which result in changes to GHG emissions elsewhere. See reference 14 in the References section.</p>\n</div>\n</div>\n</div>",
                "number" : 5,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 305-5, the reporting organization should, if subject to different standards and methodologies, describe the approach to selecting them."
                } ],
                "reportingRequirements" : [ {
                  "description" : "GHG emissions reduced as a direct result of reduction initiatives, in metric tons of CO2 equivalent."
                }, {
                  "description" : "Gases included in the calculation; whether CO2, CH4, N2O, HFCs, PFCs, SF6, NF3, or all."
                }, {
                  "description" : "Base year or baseline, including the rationale for choosing it."
                }, {
                  "description" : "Scopes in which reductions took place; whether direct (Scope 1), energy indirect (Scope 2), and/or other indirect (Scope 3)."
                }, {
                  "description" : "Standards, methodologies, assumptions, and/or calculation tools used."
                } ],
                "series" : "300",
                "subNumber" : 5,
                "title" : "Reduction of GHG emissions"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 305-6, the reporting organization shall:",
                  "subRequirements" : [ {
                    "description" : "calculate the production of ODS as the amount of ODS produced, minus the amount destroyed by approved technologies, and minus the amount entirely used as feedstock in the manufacture of other chemicals; Production of ODS = ODS produced – ODS destroyed by approved technologies – ODS entirely used as feedstock in the manufacture of other chemicals"
                  }, {
                    "description" : "2.11.2 exclude ODS recycled and reused."
                  } ]
                } ],
                "disclosureNumber" : "305-6",
                "guidance" : "<div class=\"page\" title=\"Page 16\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 305-6</strong></p>\n<p>The reporting organization can report separate or combined data for the substances included in the calculation.</p>\n<p><strong>Background</strong></p>\n<p>Measuring ODS production, imports, and exports helps to indicate how an organization complies with legislation. This is particularly relevant if the organization produces or uses ODS in its processes, products and services and is subject to phase-out commitments. Results on ODS phase-out help to indicate the organization&rsquo;s position in any markets affected by regulation on ODS.</p>\n</div>\n<div class=\"column\">\n<p>This disclosure covers the substances included in Annexes A, B, C, and E of the &lsquo;Montreal Protocol&rsquo; as well as any other ODS produced, imported, or exported by an organization.</p>\n<p>See references 1, 2, 8 and 9 in the References section.</p>\n</div>\n</div>\n</div>",
                "number" : 5,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 305-6, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "if subject to different standards and methodologies, describe the approach to selecting them;"
                  }, {
                    "description" : "where it aids transparency or comparability over time, provide a breakdown of the ODS data by:",
                    "subRecommendations" : [ {
                      "description" : "business unit or facility;"
                    }, {
                      "description" : "country;"
                    }, {
                      "description" : "type of source;"
                    }, {
                      "description" : "type of activity;"
                    } ]
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Production, imports, and exports of ODS in metric tons of CFC-11 (trichlorofluoromethane) equivalent."
                }, {
                  "description" : "Substances included in the calculation."
                }, {
                  "description" : "Source of the emission factors used."
                }, {
                  "description" : "Standards, methodologies, assumptions, and/or calculation tools used."
                } ],
                "series" : "300",
                "subNumber" : 6,
                "title" : "Emissions of ozone-depleting substances (ODS)"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 305-7, the reporting organization shall select one of the following approaches for calculating significant air emissions:",
                  "subRequirements" : [ {
                    "description" : "Direct measurement of emissions (such as online analyzers);"
                  }, {
                    "description" : "Calculation based on site-specific data;"
                  }, {
                    "description" : "Calculation based on published emission factors;"
                  }, {
                    "description" : "Estimation. If estimations are used due to a lack of default figures, the organization shall indicate the basis on which figures were estimated.\n"
                  } ]
                } ],
                "disclosureNumber" : "305-7",
                "guidance" : "<div class=\"page\" title=\"Page 17\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>See references 3, 4, 5, 6 and 10 in the References section.</p>\n</div>\n</div>\n</div>",
                "number" : 5,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 305-7, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "if subject to different standards and methodologies, describe the approach to selecting them;"
                  }, {
                    "description" : "where it aids transparency or comparability over time, provide a breakdown of the air emissions data by:",
                    "subRecommendations" : [ {
                      "description" : "business unit or facility;"
                    }, {
                      "description" : "country;"
                    }, {
                      "description" : "type of source;"
                    }, {
                      "description" : "type of activity;"
                    } ]
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Significant air emissions, in kilograms or multiples, for each of the following:",
                  "subRequirements" : [ {
                    "description" : "NOX"
                  }, {
                    "description" : "SOX"
                  }, {
                    "description" : "Persistent organic pollutants (POP)"
                  }, {
                    "description" : "Volatile organic compounds (VOC)"
                  }, {
                    "description" : "Hazardous air pollutants (HAP)"
                  }, {
                    "description" : "Particulate matter (PM)"
                  }, {
                    "description" : "Other standard categories of air emissions identified in relevant regulations"
                  } ]
                }, {
                  "description" : "Source of the emission factors used."
                }, {
                  "description" : "Standards, methodologies, assumptions, and/or calculation tools used."
                } ],
                "series" : "300",
                "subNumber" : 7,
                "title" : "Nitrogen oxides (NOX), sulfur oxides (SOX), and other significant air emissions"
              },
              {
                "disclosureNumber" : "306-1",
                "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 306-1</strong></p>\n<p>In the context of this Standard, &lsquo;water discharge&rsquo; includes water effluents discharged over the course of the reporting period. These effluents can be discharged to subsurface waters, surface waters, sewers that lead to rivers, oceans, lakes, wetlands, treatment facilities, and ground water, either:</p>\n<ul>\n<li>\n<p>through a defined discharge point (point source discharge);</p>\n</li>\n<li>\n<p>over land in a dispersed or undefined manner (non-point source discharge);</p>\n</li>\n<li>\n<p>as wastewater removed from the organization via truck.</p>\n</li>\n</ul>\n<p>Discharge of collected rainwater and domestic sewage is not considered to be water discharge.</p>\n<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for clause 2.2</strong></p>\n<p>The specific choice of water quality parameters can vary depending on the organization&rsquo;s products, services, and operations.</p>\n<p>Water quality metrics can vary depending on national or regional regulations.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 6,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 306-1, the reporting organization should:",
                  "subRecommendations" : [ {
                    "description" : "if it discharges effluents or process water, report water quality in terms of total volumes of effluent using standard effluent parameters, such as Biological Oxygen Demand (BOD) or Total Suspended Solids (TSS);"
                  }, {
                    "description" : "select parameters that are consistent with those used in the organization’s sector."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Total volume of planned and unplanned water discharges by:",
                  "subRequirements" : [ {
                    "description" : "destination;"
                  }, {
                    "description" : "quality of the water, including treatment method;"
                  }, {
                    "description" : "whether the water was reused by another organization."
                  } ]
                }, {
                  "description" : "Standards, methodologies, and assumptions used."
                } ],
                "series" : "300",
                "subNumber" : 1,
                "title" : "Water discharge by quality and destination"
              },
              {
                "disclosureNumber" : "306-2",
                "guidance" : "<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n</div>\n</div>\n</div>\n<p>Information about waste disposal methods reveals the extent to which an organization has managed the balance between disposal options and uneven environmental impacts. For example, land filling and recycling create very different types of environmental impacts and residual effects. Most waste minimization strategies emphasize prioritizing options for reuse, recycling, and then recovery over other disposal options to minimize ecological impacts.</p>\n</div>\n</div>\n</div>",
                "number" : 6,
                "reportingRequirements" : [ {
                  "description" : "Total weight of hazardous waste, with a breakdown by the following disposal methods where applicable:",
                  "subRequirements" : [ {
                    "description" : "Reuse"
                  }, {
                    "description" : "Recycling"
                  }, {
                    "description" : "Composting"
                  }, {
                    "description" : "Recovery, including energy recovery"
                  }, {
                    "description" : "Incineration (mass burn)"
                  }, {
                    "description" : "Deep well injection"
                  }, {
                    "description" : "Landfill"
                  }, {
                    "description" : "On-site storage"
                  }, {
                    "description" : "Other (to be specified by the organization)"
                  } ]
                }, {
                  "description" : "Total weight of non-hazardous waste, with a breakdown by the following disposal methods where applicable:",
                  "subRequirements" : [ {
                    "description" : "Reuse"
                  }, {
                    "description" : "Recycling"
                  }, {
                    "description" : "Composting"
                  }, {
                    "description" : "Recovery, including energy recovery"
                  }, {
                    "description" : "Incineration (mass burn)"
                  }, {
                    "description" : "Deep well injection"
                  }, {
                    "description" : "Landfill"
                  }, {
                    "description" : "On-site storage"
                  }, {
                    "description" : "Other (to be specified by the organization)"
                  } ]
                }, {
                  "description" : "How the waste disposal method has been determined:",
                  "subRequirements" : [ {
                    "description" : "Disposed of directly by the organization, or otherwise directly confirmed"
                  }, {
                    "description" : "Information provided by the waste disposal contractor"
                  }, {
                    "description" : "Organizational defaults of the waste disposal contractor"
                  } ]
                } ],
                "series" : "300",
                "subNumber" : 2,
                "title" : "Waste by type and disposal method"
              },
              {
                "disclosureNumber" : "306-3",
                "guidance" : "",
                "number" : 6,
                "reportingRequirements" : [ {
                  "description" : "Total number and total volume of recorded significant spills."
                }, {
                  "description" : "The following additional information for each spill that was reported in the organization’s financial statements:",
                  "subRequirements" : [ {
                    "description" : "Location of spill;"
                  }, {
                    "description" : "Volume of spill;"
                  }, {
                    "description" : "Material of spill, categorized by: oil spills (soil or water surfaces), fuel spills (soil or water surfaces), spills of wastes (soil or water surfaces), spills of chemicals (mostly soil or water surfaces), and other (to be specified by the organization)."
                  } ]
                }, {
                  "description" : "Impacts of significant spills."
                } ],
                "series" : "300",
                "subNumber" : 3,
                "title" : "Significant spills"
              },
              {
                "additionalRequirements" : [ {
                  "description" : "When compiling the information specified in Disclosure 306-4, the reporting organization shall:",
                  "subRequirements" : [ {
                    "description" : "convert volumes to an estimate of weight;\n"
                  }, {
                    "description" : "in response to Disclosure 306-4-c, provide a brief explanation of the methodology used for making these conversions."
                  } ]
                } ],
                "disclosureNumber" : "306-4",
                "guidance" : "<div class=\"page\" title=\"Page 10\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 306-4</strong></p>\n<p>This disclosure covers waste deemed hazardous under the terms of the Basel Convention Annex I, II, III, and VIII (see reference 1 in the References section). It covers hazardous waste transported by or on behalf of the reporting organization within the reporting period by destination, including transport across operational boundaries and within operations.</p>\n<p>The organization can calculate the total weight of transported hazardous waste using the following equation:</p>\n<div class=\"page\" title=\"Page 10\">\n<div class=\"section\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Total weight of hazardous waste transported by destination = Weight of hazardous waste transported to the organization by destination from external sources/ suppliers not owned by the organization + Weight of hazardous waste transported from the organization by destination to external sources/ suppliers not owned by the organization + Weight of hazardous waste transported nationally and internationally by destination between locations owned, leased, or managed by the organization&nbsp;</p>\n<div class=\"page\" title=\"Page 10\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>Imported hazardous waste can be calculated as the total weight of hazardous waste transported across international borders and which enters the boundaries of the organization, by destination, excluding waste transported between different locations of the organization.</p>\n<p>Exported hazardous waste can be calculated as<br /> the proportion of the total amount of transported hazardous waste by destination that is transported from the organization to locations abroad, including all waste that leaves the boundaries of the organization to cross international borders and excluding transportation between different locations of the organization.</p>\n<p>For treated waste, the organization can identify:</p>\n<ul>\n<li>\n<p>the portion of the total amount of transported and exported waste that the organization has treated, by destination;</p>\n</li>\n<li>\n<p>the portion of the total amount of waste, by destination, that is treated by external sources/ suppliers, that has been transported, exported, or imported by the organization.</p>\n</li>\n</ul>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 6,
                "reportingRequirements" : [ {
                  "description" : "Total weight for each of the following:",
                  "subRequirements" : [ {
                    "description" : "Hazardous waste transported"
                  }, {
                    "description" : "Hazardous waste imported"
                  }, {
                    "description" : "Hazardous waste exported"
                  }, {
                    "description" : "Hazardous waste treated"
                  } ]
                }, {
                  "description" : "Percentage of hazardous waste shipped internationally."
                }, {
                  "description" : "Standards, methodologies, and assumptions used."
                } ],
                "series" : "300",
                "subNumber" : 4,
                "title" : "Transport of hazardous waste"
              },
              {
                "disclosureNumber" : "306-5",
                "guidance" : "<div class=\"page\" title=\"Page 11\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>This disclosure is a qualitative counterpart to the quantitative disclosures of water discharge, and helps to describe the impact of these discharges. Discharges and runoff affecting aquatic habitats can have a significant impact on the availability of water resources.</p>\n<p>See references 4 and 5 in the References section.</p>\n</div>\n</div>\n</div>",
                "number" : 6,
                "reportingRequirements" : [ {
                  "description" : "Water bodies and related habitats that are significantly affected by water discharges and/or runoff, including information on:",
                  "subRequirements" : [ {
                    "description" : "the size of the water body and related habitat;"
                  }, {
                    "description" : "whether the water body and related habitat is designated as a nationally or internationally protected area;"
                  }, {
                    "description" : "the biodiversity value, such as total number of protected species."
                  } ]
                } ],
                "series" : "300",
                "subNumber" : 5,
                "title" : "Water bodies affected by water discharges and/or runoff"
              },
              {
                "disclosureNumber" : "307-1",
                "guidance" : "<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 307-1</strong></p>\n<p>In certain jurisdictions, voluntary environmental agreements with regulating authorities can be referred to as &lsquo;covenants&rsquo;.</p>\n<div class=\"page\" title=\"Page 6\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>Non-compliance within an organization can indicate the ability of management to ensure that operations conform to certain performance parameters. In some circumstances, non-compliance can lead to clean-up obligations or other costly environmental liabilities. The strength of an organization&rsquo;s compliance record can also affect its ability to expand operations or gain permits.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 7,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 307-1, the reporting organization should include administrative and judicial sanctions for failure to comply with environmental laws and/or regulations, including:",
                  "subRecommendations" : [ {
                    "description" : "international declarations, conventions, and treaties;"
                  }, {
                    "description" : "national, sub-national, regional, and local regulations;"
                  }, {
                    "description" : "voluntary environmental agreements with regulating authorities that are considered binding and developed as a substitute for implementing new regulations;"
                  }, {
                    "description" : "cases brought against the organization through the use of international dispute mechanisms or national dispute mechanisms supervised by government authorities;"
                  }, {
                    "description" : "cases of non-compliance related to spills as reported with GRI 306: Effluents and Waste."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Significant fines and non-monetary sanctions for non-compliance with environmental laws and/or regulations in terms of:",
                  "subRequirements" : [ {
                    "description" : "total monetary value of significant fines;"
                  }, {
                    "description" : "total number of non-monetary sanctions;"
                  }, {
                    "description" : "cases brought through dispute resolution mechanisms."
                  } ]
                }, {
                  "description" : "If the organization has not identified any non-compliance with environmental laws and/or regulations, a brief statement of this fact is sufficient."
                } ],
                "series" : "300",
                "subNumber" : 1,
                "title" : "Non-compliance with environmental laws and regulations"
              },
              {
                "disclosureNumber" : "308-1",
                "guidance" : "<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 308-1</strong></p>\n<p>Environmental criteria can include the topics in the 300 series (Environmental topics).</p>\n<p><strong>Background</strong></p>\n<p>This disclosure informs stakeholders about the percentage of suppliers selected or contracted subject to due diligence processes for environmental impacts.</p>\n<div class=\"page\" title=\"Page 7\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p>An organization is expected to initiate due diligence as early as possible in the development of a new relationship with a supplier.</p>\n<p>Impacts may be prevented or mitigated at the stage of structuring contracts or other agreements, as well as via ongoing collaboration with suppliers.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>",
                "number" : 8,
                "reportingRequirements" : [ {
                  "description" : "Percentage of new suppliers that were screened using environmental criteria."
                } ],
                "series" : "300",
                "subNumber" : 1,
                "title" : "New suppliers that were screened using environmental criteria"
              },
              {
                "disclosureNumber" : "308-2",
                "guidance" : "<div class=\"page\" title=\"Page 8\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><strong>Guidance for Disclosure 308-2</strong></p>\n<p>Negative impacts include those that are either caused or contributed to by an organization, or that are directly linked to its activities, products, or services by its relationship with a supplier.</p>\n<p>Assessments for environmental impacts can include the topics in the 300 series (Environmental topics).</p>\n<p>Assessments can be made against agreed performance expectations that are set and communicated to the suppliers prior to the assessment.</p>\n<p>Assessments can be informed by audits, contractual reviews, two-way engagement, and complaint and <span style=\"text-decoration: underline;\">grievance mechanisms</span>.</p>\n<p>Improvements can include changing an organization&rsquo;s procurement practices, the adjustment of performance expectations, capacity building, training, and changes<br /> to processes.</p>\n</div>\n<div class=\"column\">\n<p><strong>Background</strong></p>\n<p>This disclosure informs stakeholders about an organization&rsquo;s awareness of significant actual and potential negative environmental impacts in the supply chain.</p>\n</div>\n</div>\n</div>",
                "number" : 8,
                "recommendations" : [ {
                  "description" : "When compiling the information specified in Disclosure 308-2, the reporting organization should, where it provides appropriate context on significant impacts, provide a breakdown of the information by:",
                  "subRecommendations" : [ {
                    "description" : "the location of the supplier;"
                  }, {
                    "description" : "the significant actual and potential negative environmental impact."
                  } ]
                } ],
                "reportingRequirements" : [ {
                  "description" : "Number of suppliers assessed for environmental impacts."
                }, {
                  "description" : "Number of suppliers identified as having significant actual and potential negative environmental impacts."
                }, {
                  "description" : "Significant actual and potential negative environmental impacts identified in the supply chain."
                }, {
                  "description" : "Percentage of suppliers identified as having significant actual and potential negative environmental impacts with which improvements were agreed upon as a result of assessment."
                }, {
                  "description" : "Percentage of suppliers identified as having significant actual and potential negative environmental impacts with which relationships were terminated as a result of assessment, and why."
                } ],
                "series" : "300",
                "subNumber" : 2,
                "title" : "Negative environmental impacts in the supply chain and actions taken"
              },
        ];
    },
};