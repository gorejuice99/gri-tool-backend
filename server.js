const express = require('express'); // call express
const app = express(); // define our app using express
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const deferred = require('q').defer();
const morgan = require('morgan');

mongoose.connect('mongodb://127.0.0.1:27017/griDataGatheringTool');
mongoose.connection.on('connected', function() {
    console.log('Connected to ' + 'mongodb://127.0.0.1:27017/griDataGatheringTool');
});

mongoose.connection.on('error', function(err) {
    console.log('Mongoose default connection error: ' + err);
});

app.use(function(req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4203');
  
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-type, Authorization');
  
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
  
    // Pass to next layer of middleware
    next();
});

app.use(bodyParser.urlencoded({ limit: '80mb', extended: true }));
app.use(bodyParser.json({ limit: '80mb' }));

app.use(morgan('dev'));

const port = process.env.PORT || 3000;




/** 
 * requiring routes
*/
const userApi = require('./api/routes/userRoutes');
const organizationApi = require('./api/routes/organizationRoutes');
const disclosureApi = require('./api/routes/disclosureRoutes');
/**
 * requireing model
 */
require('./api/models/userModel');
require('./api/models/organizationModel');
require('./api/models/disclosureModel');
require('./api/models/userDisclosureModel');

/**
 * use Routes
 */
userApi(app);
organizationApi(app);
disclosureApi(app);

const router = express.Router();
router.get('/', function(req, res) {
  res.json({ message: 'hooray! welcome to our api!' });
});

app.use('/api', router);
app.listen(port);

deferred.resolve(app);
module.exports = deferred.promise;

console.log('GRI Backend Tool running on port ' + port);

